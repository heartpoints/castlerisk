import { FC, PropsWithChildren } from "react";

export const GridHeading: FC<PropsWithChildren> = ({ children }) => (
  <span style={{ fontWeight: "bold", textDecoration: "underline" }}>
    {children}
  </span>
);
