import { GridHeading } from "./GridHeading";

export const DiceRollGridHeadings = () => (
  <>
    <GridHeading>Player</GridHeading>
    <GridHeading>highest</GridHeading>
    <GridHeading>2nd highest</GridHeading>
    <GridHeading>3rd highest</GridHeading>
  </>
);
