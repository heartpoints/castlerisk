import { FC, PropsWithChildren } from "react";

export const DiceGrid: FC<PropsWithChildren> = ({ children }) => (
  <div
    style={{
      display: "grid",
      gridTemplateRows: "auto",
      gridTemplateColumns: "1fr 1fr 1fr 1fr",
      gap: "1em",
    }}
  >
    {children}
  </div>
);
