import { HasAttackDiceCount } from "./HasAttackDiceCount";

export type HasPossibleAttackDiceCount = Partial<HasAttackDiceCount>
