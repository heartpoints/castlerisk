
import { z } from "zod";
import { PlayerDiceRoll } from "./PlayerDiceRoll";

export const DiceRoll = z.object({ attacker: PlayerDiceRoll, defender: PlayerDiceRoll, numGenerals: z.number(), numMarshalls: z.number(), isAdmiral: z.boolean() });
export type DiceRoll = z.infer<typeof DiceRoll>;
