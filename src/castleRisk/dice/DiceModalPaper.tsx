import { boxShadow } from "castleRisk/config/boxShadow";
import { FC, PropsWithChildren } from "react";


export const DiceModalPaper: FC<PropsWithChildren> = ({ children }) => (
  <div
    style={{
      backgroundColor: "rgba(255,255,255,0.9)",
      borderRadius: "1em",
      boxShadow,
      padding: "1em",
      marginBottom: "5em",
    }}
  >
    {children}
  </div>
);
