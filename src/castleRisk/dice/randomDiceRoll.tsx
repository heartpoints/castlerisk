import { randomIntegerBetweenZeroAnd } from "../../utils/list/randomIntegerBetweenZeroAnd";

export const randomDiceRoll = () => randomIntegerBetweenZeroAnd(6) + 1;
