import { HasDefenseDiceCount } from "./HasDefenseDiceCount";

export type HasPossibleDefenseDiceCount = Partial<HasDefenseDiceCount>
