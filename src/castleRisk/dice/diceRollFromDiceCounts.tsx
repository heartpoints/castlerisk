import { randomDiceRolls } from "./randomDiceRolls";

export const diceRollFromDiceCounts = (attackerDiceCount: number, defenseDiceCount: number) => ({
  attackerDice: randomDiceRolls(attackerDiceCount),
  defenderDice: randomDiceRolls(defenseDiceCount),
});
