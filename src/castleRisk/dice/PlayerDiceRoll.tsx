
import { z } from "zod";

export const PlayerDiceRoll = z.object({ playerIndex: z.number(), dice: z.array(z.number()) });
export type PlayerDiceRoll = z.infer<typeof PlayerDiceRoll>;
