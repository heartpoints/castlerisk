import { doNothing } from "@utils/axioms/doNothing";
import { randomNumberBetweenZeroAnd } from "@utils/list/randomNumberBetweenZeroAnd";
import { FC, useLayoutEffect, useRef } from "react";
import ReactDice, { ReactDiceRef } from "react-dice-complete";

export const AttackDie: FC<{
  sideToDisplay?: number;
  selected?: boolean;
  onClick?: VoidFunction;
  disabled?: boolean;
  rollTo?: number;
  onRollComplete?: VoidFunction;
  shouldAnimate?: boolean;
}> = ({ sideToDisplay, selected, onClick, disabled, rollTo, onRollComplete = doNothing, shouldAnimate }) => {
  const reactDice = useRef<ReactDiceRef>(null);
  useLayoutEffect(() => {
    shouldAnimate && rollTo && reactDice.current?.rollAll([rollTo]);
  })
  return (
    <div
      onClick={disabled ? doNothing : onClick}
      style={{
        cursor: disabled ? "not-allowed" : "pointer",
        opacity: disabled ? 0.2 : 1,
      }}
    >
      <ReactDice
        disableIndividual={true}
        defaultRoll={rollTo || sideToDisplay }
        numDice={1}
        rollTime={randomNumberBetweenZeroAnd(2) + 1}
        ref={reactDice}
        rollDone={onRollComplete}
        dieCornerRadius={5}
        dotColor="black"
        faceColor="white"
        outline={true}
        outlineColor={selected ? "blue" : "rgba(0,0,0,0.1)"}
        dieSize={50}
        margin={0} />
    </div>
  );
};
