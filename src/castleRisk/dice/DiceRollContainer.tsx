import { FC, PropsWithChildren } from "react";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";

export const DiceRollContainer: FC<PropsWithChildren<{ isWinner: boolean; }>> = ({
  children, isWinner,
}) => (
  <span
    style={{
      borderColor: isWinner ? "green" : "rgba(0,0,0,0)",
      borderWidth: "3px",
      borderRadius: "1em",
      borderStyle: "dotted",
      padding: "1em",
    }}
  >
    {children}
    {isWinner && (
      <CheckCircleIcon
        style={{
          paddingTop: "5px",
          color: "green",
          width: "1.5em",
          height: "auto",
        }} />
    )}
  </span>
);
