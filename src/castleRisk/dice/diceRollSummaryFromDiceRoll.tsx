import { sort } from "../../utils/arrays/sort";
import { attacker } from "../phases/attack/general/attacker";
import { defender } from "../phases/attack/general/defender";
import { DiceRoll } from "./DiceRoll";
import { DiceRollSummary } from "./DiceRollSummary";

export const diceRollSummaryFromDiceRoll = ({
  attacker: { dice: attackerDice }, defender: { dice: defenderDice }, numGenerals, numMarshalls
}: DiceRoll): DiceRollSummary => {
  const attackerDiceHighestFirst = sort(attackerDice).reverse();
  const defenderDiceHighestFirst = sort(defenderDice).reverse();

  const highest = {
    attacker: attackerDiceHighestFirst[0],
    defender: defenderDiceHighestFirst[0],
    winner: attackerDiceHighestFirst[0] + numGenerals > defenderDiceHighestFirst[0] + numMarshalls
      ? attacker
      : defender,
  };

  const attackerSecondHighest = attackerDiceHighestFirst[1];
  const defenderSecondHighest = defenderDiceHighestFirst[1];

  const secondHighest = {
    attacker: attackerSecondHighest,
    defender: defenderSecondHighest,
    winner: attackerSecondHighest && defenderSecondHighest
      ? attackerDiceHighestFirst[1] > defenderDiceHighestFirst[1]
        ? attacker
        : defender
      : undefined,
  };

  const thirdHighest = attackerDiceHighestFirst[2];

  const armiesLost = {
    attacker: (highest.winner === defender ? 1 : 0) +
      (secondHighest.winner === defender ? 1 : 0),
    defender: (highest.winner === attacker ? 1 : 0) +
      (secondHighest.winner === attacker ? 1 : 0),
  };

  return {
    highest,
    secondHighest,
    thirdHighest,
    armiesLost,
  };
};
