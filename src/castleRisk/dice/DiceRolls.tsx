import { z } from "zod";
import { DiceRoll } from "./DiceRoll";

export const DiceRolls = z.array(DiceRoll)
export type DiceRolls = z.infer<typeof DiceRolls>
