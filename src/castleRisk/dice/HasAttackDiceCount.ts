import { z } from "zod";

export const HasAttackDiceCount = z.object({ attackDiceCount: z.number() });
export type HasAttackDiceCount = z.infer<typeof HasAttackDiceCount>;
