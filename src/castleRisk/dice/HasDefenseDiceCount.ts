import { z } from "zod";

export const HasDefenseDiceCount = z.object({ defenseDiceCount: z.number() });
export type HasDefenseDiceCount = z.infer<typeof HasDefenseDiceCount>;
