import { Alert, Button } from "@mui/material";
import { last } from "@utils/arrays/last";
import { applyAttackOutcome } from "castleRisk/phases/attack/general/applyAttackOutcome";
import { defendingPlayer } from "castleRisk/phases/attack/general/defendingPlayer";
import { RollingProps } from "castleRisk/phases/attack/rolling/RollingProps";
import { currentPlayerFromState } from "castleRisk/players/currentPlayerFromState";
import { FC, useEffect } from "react";
import { DiceGrid } from "./DiceGrid";
import { DiceModal } from "./DiceModal";
import { DiceModalPaper } from "./DiceModalPaper";
import { diceRollSummaryFromDiceRoll } from "./diceRollSummaryFromDiceRoll";
import { SingleDiceRollSummary } from "./SingleDiceRollSummary";
import { DiceRollGridHeadings } from "./DiceRollGridHeadings";

export const Dice: FC<RollingProps> = ({ setState, ...state }) => {
  useEffect(() => {
    const audio = new Audio("/sounds/diceRoll.wav");
    audio.play();
  });
  const currentDiceRoll = last(state.diceRolls);
  const diceRollSummary = diceRollSummaryFromDiceRoll(currentDiceRoll);
  return (
    <DiceModal>
      <h2 style={{ color: "white" }}>
        <strong>{state.attackOriginTerritory.name}</strong> attacks{" "}
        <strong>{state.attackTargetTerritory.name}</strong>!
      </h2>
      <DiceModalPaper>
        <DiceGrid>
          <DiceRollGridHeadings />
          <SingleDiceRollSummary shouldAnimate={true} {...{ currentDiceRoll, ...state }} />
        </DiceGrid>
        <div style={{ marginTop: "1em", marginBottom: "1em" }}>
          <Alert severity="info">
            {currentPlayerFromState(state).name} loses{" "}
            {diceRollSummary.armiesLost.attacker}{" "}
            {diceRollSummary.armiesLost.attacker !== 1 ? "armies" : "army"}
            {currentDiceRoll.numGenerals > 0 &&
            diceRollSummary.highest.winner == "defender"
              ? ` and ${currentDiceRoll.numGenerals} ${
                  currentDiceRoll.numGenerals !== 1 ? "Generals" : "General"
                }. `
              : ". "}
            {defendingPlayer(state).name} loses{" "}
            {diceRollSummary.armiesLost.defender}{" "}
            {diceRollSummary.armiesLost.defender !== 1 ? "armies" : "army"}.
          </Alert>
        </div>
        <div style={{ textAlign: "right" }}>
          <Button
            variant="contained"
            onClick={() => {
              setState(applyAttackOutcome(state));
            }}
          >
            OK
          </Button>
        </div>
      </DiceModalPaper>
    </DiceModal>
  );
};
