import { arrayOfLength } from "@utils/arrays/arrayOfLength";
import { AttackReadyState } from "castleRisk/phases/attack/general/AttackReadyState";
import { defendingPlayer } from "castleRisk/phases/attack/general/defendingPlayer";
import { currentPlayerFromState } from "castleRisk/players/currentPlayerFromState";
import { FC } from "react";
import { AttackDie } from "./AttackDie";
import { DefenseDie } from "./DefenseDie";
import { DiceRoll } from "./DiceRoll";
import { DiceRollContainer } from "./DiceRollContainer";
import { diceRollSummaryFromDiceRoll } from "./diceRollSummaryFromDiceRoll";
import { GeneralAvatar } from "castleRisk/cards/general/GeneralAvatar";

export const SingleDiceRollSummary: FC<
  { currentDiceRoll: DiceRoll; shouldAnimate: boolean } & AttackReadyState
> = ({ currentDiceRoll, shouldAnimate, ...state }) => {
  const diceRollSummary = diceRollSummaryFromDiceRoll(currentDiceRoll);
  return (
    <>
      <span>
        {currentPlayerFromState(state).name}
        <br />({state.attackOriginTerritory.name})
      </span>

      <DiceRollContainer
        isWinner={diceRollSummary.highest.winner == "attacker"}
      >
        <AttackDie
          rollTo={diceRollSummary.highest.attacker}
          {...{ shouldAnimate }}
        />
        {arrayOfLength(currentDiceRoll.numGenerals).map((_, i) => (
          <GeneralAvatar key={i} />
        ))}
      </DiceRollContainer>
      <DiceRollContainer
        isWinner={diceRollSummary.secondHighest.winner == "attacker"}
      >
        {diceRollSummary.secondHighest.attacker && (
          <AttackDie
            rollTo={diceRollSummary.secondHighest.attacker}
            {...{ shouldAnimate }}
          />
        )}
      </DiceRollContainer>
      <DiceRollContainer isWinner={false}>
        {diceRollSummary.thirdHighest && (
          <AttackDie
            rollTo={diceRollSummary.thirdHighest}
            {...{ shouldAnimate }}
          />
        )}
      </DiceRollContainer>
      <span>
        {defendingPlayer(state).name}
        <br />({state.attackTargetTerritory.name})
      </span>
      <DiceRollContainer
        isWinner={diceRollSummary.highest.winner == "defender"}
      >
        <DefenseDie
          rollTo={diceRollSummary.highest.defender}
          {...{ shouldAnimate }}
        />
      </DiceRollContainer>
      <DiceRollContainer
        isWinner={diceRollSummary.secondHighest.winner == "defender"}
      >
        {diceRollSummary.secondHighest.defender && (
          <DefenseDie
            rollTo={diceRollSummary.secondHighest.defender}
            {...{ shouldAnimate }}
          />
        )}
      </DiceRollContainer>
    </>
  );
};
