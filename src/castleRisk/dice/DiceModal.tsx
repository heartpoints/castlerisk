import { FC, PropsWithChildren } from "react";

export const DiceModal: FC<PropsWithChildren> = ({ children }) => (
  <div
    style={{
      position: "absolute",
      height: "100%",
      backgroundColor: "rgba(0,0,0,0.5)",
      zIndex: 4,
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      width: "100%",
      flexDirection: "column",
    }}
  >
    {children}
  </div>
);
