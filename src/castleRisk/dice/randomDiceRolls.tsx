import { arrayOfLength } from "../../utils/arrays/arrayOfLength";
import { randomDiceRoll } from "./randomDiceRoll";

export const randomDiceRolls = (numDice: number) => arrayOfLength(numDice).map(randomDiceRoll);
