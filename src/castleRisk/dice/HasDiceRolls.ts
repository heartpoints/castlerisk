import { DiceRolls } from "./DiceRolls";
import { z } from "zod";

export const HasDiceRolls = z.object({ diceRolls: DiceRolls });
export type HasDiceRolls = z.infer<typeof HasDiceRolls>;
