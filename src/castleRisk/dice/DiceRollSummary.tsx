import { AttackerAndDefenderNumbers } from "../phases/attack/general/AttackerAndDefenderNumbers";
import { HasWinner } from "../phases/attack/general/HasWinner";

export type DiceRollSummary = {
  highest: AttackerAndDefenderNumbers & HasWinner;
  secondHighest: Partial<AttackerAndDefenderNumbers & HasWinner>;
  thirdHighest: number | undefined;
  armiesLost: AttackerAndDefenderNumbers;
};
