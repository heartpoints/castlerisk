import { CastleRiskState } from '../phases/common/CastleRiskState'

export const onSaveClicked = (state: CastleRiskState) => () => {
    const prettyJson = JSON.stringify(state, null, 3)
    navigator.clipboard.writeText(prettyJson).then(() => alert('copied state to clipboard!'))
}
