import { gray } from '../../utils/colors/gray';
import { lightenColor } from '../../utils/colors/lightenColor';

export const castlePieceBackgroundColor = lightenColor(0.6)(gray);
