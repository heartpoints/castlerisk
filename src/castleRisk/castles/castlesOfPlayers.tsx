import { Players } from '../players/Players';

export const castlesOfPlayers = (players: Players) => players.flatMap(p => p.castles);
