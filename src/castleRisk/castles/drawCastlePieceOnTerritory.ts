import { defaultFontPixels } from "../../utils/canvas/defaultFontPixels";
import { centroidOfPolygon } from "../../utils/geometry/centroidOfPolygon";
import { pointPlus } from "../../utils/geometry/pointPlus";
import { Empire } from "../empires/Empire";
import { Territory } from "../territories/Territory";
import { castleTokenRadius } from "./castleTokenRadius";
import { drawCastlePiece } from "./drawCastlePiece";

export const drawCastlePieceOnTerritory =
  (empire: Empire) => (territory: Territory) =>
    drawCastlePiece(empire)(
      pointPlus({ x: 0, y: -((castleTokenRadius + defaultFontPixels + 5) / 2) })(
        centroidOfPolygon(territory.boundary)
      )
    );
