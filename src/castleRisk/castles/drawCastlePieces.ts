import { sequentially } from '../../utils/composition/sequentially';
import { Castles } from './Castles';
import { territoryByName } from '../territories/territoryByName';
import { drawCastlePieceOnTerritory } from './drawCastlePieceOnTerritory';
import { empireNamed } from '../empires/empireNamed';
import { territoryNameExists } from '../territories/territoryNameExists';

export const drawCastlePieces = (castles: Castles) => sequentially(
    castles
        .filter(territoryNameExists)
        .map(
            c => 
            //todo: remove the ! and make it typesafe
            drawCastlePieceOnTerritory(empireNamed(c.empireName))(territoryByName(c.territoryName)!)
        )
);
