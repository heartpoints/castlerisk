import { Castles } from "./Castles";

import { z } from "zod";

export const HasCastles = z.object({ castles: Castles });
export type HasCastles = z.infer<typeof HasCastles>;
