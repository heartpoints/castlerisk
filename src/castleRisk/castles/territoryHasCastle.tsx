import { castlesOfPlayers } from "./castlesOfPlayers";
import { Players } from "../players/Players";
import { Territory } from "../territories/Territory";
import { territoryNameEquals } from "../territories/territoryNameEquals";

export const territoryHasCastle = 
    (players: Players) => 
    (t: Territory) => 
    castlesOfPlayers(players).some(territoryNameEquals(t.name))
