import { Castle } from "./Castle";

import { z } from "zod";

export const Castles = z.array(Castle);
export type Castles = z.infer<typeof Castles>;
