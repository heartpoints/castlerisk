import { sequentially } from '../../utils/composition/sequentially'
import { beginPath } from '../../utils/canvas/beginPath'
import { fill } from '../../utils/canvas/fill'
import { setFillStyle } from '../../utils/canvas/setFillStyle'
import { setStrokeStyle } from '../../utils/canvas/setStrokeStyle'
import { stroke } from '../../utils/canvas/stroke'
import { white } from '../../utils/colors/white'
import { Empire } from "../empires/Empire"
import { circle } from '../../utils/canvas/circle'
import { drawText } from '../../utils/canvas/drawText'
import { setShadow } from '../../utils/canvas/setShadow'
import { pointPlus } from '../../utils/geometry/pointPlus'
import { castleLetterOffset } from './castleLetterOffset'
import { castlePieceBackgroundColor } from './castlePieceBackgroundColor'
import { Point } from '../../utils/geometry/Point'
import { castleTokenRadius } from './castleTokenRadius'

export const drawCastlePiece = 
    (empire: Empire) => 
    (center: Point) =>
    sequentially([
        setShadow(true),
        setStrokeStyle(white),
        setFillStyle(castlePieceBackgroundColor),
        beginPath,
        circle(center)(castleTokenRadius),
        fill,
        stroke,
        setShadow(false),
        drawText(false)(empire.color)(pointPlus(center)(castleLetterOffset))(empire.name.charAt(0).toUpperCase()),
    ])
