import { HasEmpireName } from "../empires/HasEmpireName";
import { HasTerritoryName } from "../territories/HasTerritoryName";

import { z } from "zod";

export const Castle = z.intersection(HasEmpireName, HasTerritoryName.partial());
export type Castle = z.infer<typeof Castle>;
