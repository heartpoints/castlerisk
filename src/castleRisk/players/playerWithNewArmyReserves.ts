import { range } from '../..//utils/list/range';
import { Integer } from '../..//utils/math/Integer';
import { newArmyReservePiece } from '../pieces/newArmyReservePiece';
import { Player } from '../players/Player';

export const playerWithNewArmyReserves = 
    (numPieces: Integer) => 
    (player: Player): Player => ({
        ...player,
        pieces: range(numPieces).map(() => newArmyReservePiece(player))
    });
