import { Castles } from '../castles/Castles';
import { RGBAColor } from '../../utils/colors/RGBAColor';
import { Player } from './Player';

export const initialPlayer = (name:string, color: RGBAColor, castles: Castles): Player => ({
    id: crypto.randomUUID(),
    name,
    color,
    castles,
    pieces: [],
    hiddenArmyTerritoryNames: [],
    cards: [],
});
