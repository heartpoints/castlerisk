import { nextPlayerIndex } from "./nextPlayerIndex";
import { Players } from "./Players";
import { CastleRiskPlayState } from "../phases/common/CastleRiskPlayState";

export const nonCurrentPlayersInTurnOrder = (state: CastleRiskPlayState): Players => [
  ...state.players.slice(nextPlayerIndex(state)),
  ...state.players.slice(0, state.currentPlayerIndex),
];
