import { z } from "zod";

export const ID = z.string().nonempty();
export type ID = z.infer<typeof ID>;
