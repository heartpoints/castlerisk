import { paddingBetweenPlayerBoxes } from './paddingBetweenPlayerBoxes';
import { playerBoxSize } from './playerBoxSize';

export const playerBoxHeight = (numPlayers: number) => (playerBoxSize.y - paddingBetweenPlayerBoxes * numPlayers) / numPlayers;
