import { NumPlayers } from './NumPlayers';
import { numberOfInitialArmies } from './numberOfInitialArmies';
import { totalNumberOfPiecesPerColor } from "./totalNumberOfPiecesPerColor";

export const numberOfReserveArmiesAfterSetup = 
    (numPlayers: NumPlayers) => 
    totalNumberOfPiecesPerColor - numberOfInitialArmies(numPlayers);
