import { HasName } from "../../utils/axioms/HasName";
import { HasColor } from "../../utils/colors/HasColor";
import { HasPlayerCards } from "../cards/HasPlayerCards";
import { HasCastles } from "../castles/HasCastles";
import { HasPieces } from "../pieces/HasPieces";
import { HasHiddenArmies } from "./HasHiddenArmy";

import { z } from "zod";
import { HasID } from "./HasID";

export const Player = z.object({
  ...HasID.shape,
  ...HasName.shape,
  ...HasColor.shape,
  ...HasCastles.shape,
  ...HasPieces.shape,
  ...HasHiddenArmies.shape,
  ...HasPlayerCards.shape,
});
export type Player = z.infer<typeof Player>;
