import { Player } from './Player'

export const playerNameRequirementsMet = 
    (possibleNewPlayer: Player) => 
    possibleNewPlayer.name.trim().length > 1
