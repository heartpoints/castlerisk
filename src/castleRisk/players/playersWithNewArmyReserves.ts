import { throwError } from '../../utils/debugging/throwError';
import { Players } from './Players';
import { isValidNumPlayers } from './isValidNumPlayers';
import { numberOfInitialArmies } from './numberOfInitialArmies';
import { playerWithNewArmyReserves } from './playerWithNewArmyReserves';

export const playersWithNewArmyReserves = 
    (players: Players): Players => 
    isValidNumPlayers(players.length)
        ? players.map(playerWithNewArmyReserves(numberOfInitialArmies(players.length)))
        : throwError(new Error(`Invalid number of players`));
