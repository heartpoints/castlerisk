import { RGBAColor } from "../../utils/colors/RGBAColor";
import { PlayerCards } from "../cards/PlayerCards";
import { Player } from "./Player";
import { Players } from "./Players";

export const newPlayer = (color: RGBAColor) => (playersSoFar: Players):Player => ({
    id: crypto.randomUUID(),
    color,
    name: `Player ${playersSoFar.length + 1}`,
    castles: [],
    pieces: [],
    hiddenArmyTerritoryNames: [],
    cards: [] as PlayerCards,
});
