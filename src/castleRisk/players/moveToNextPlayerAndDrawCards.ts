import { CastleRiskPlayState } from "../phases/common/CastleRiskPlayState";
import { pickCardsAtStartOfTurn } from "../cards/pickCardsAtStartOfTurn";
import { moveToNextPlayer } from "./moveToNextPlayer";

export const moveToNextPlayerAndDrawCards = <S extends CastleRiskPlayState>(
  state: S
): S => pickCardsAtStartOfTurn(moveToNextPlayer(state));
