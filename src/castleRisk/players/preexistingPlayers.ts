import { austrianEmpire } from "../map/empires/austrian/austrianEmpire";
import { britishEmpire } from "../map/empires/british/britishEmpire";
import { frenchEmpire } from "../map/empires/french/frenchEmpire";
import { pieceColors } from "../pieces/pieceColors";
import { Players } from "./Players";
import { initialPlayer } from "./initialPlayer";
import { tommy } from "./tommy";

export const preexistingPlayers: Players = [
    tommy,
    initialPlayer("Tdizzlewizzle", pieceColors[2], [{ empireName: frenchEmpire.name }]),
    initialPlayer("Mike", pieceColors[3], [{ empireName: austrianEmpire.name }]),
    initialPlayer("Peepycakes", pieceColors[4], [{ empireName: britishEmpire.name }]),
];
