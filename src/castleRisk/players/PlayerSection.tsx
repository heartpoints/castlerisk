import { Player } from './Player';
import { PlayerBox } from './PlayerBox';
import { Players } from './Players';
import { playerEquals } from './playerEquals';

export type PlayerSectionProps = { players: Players, player: Player }
export const PlayerSection = 
    ({players, player}:PlayerSectionProps) =>
    <>
        {players.map(currentPlayer => PlayerBox(currentPlayer)(playerEquals(player)(currentPlayer)))}
    </>
