import { validNumPlayers } from "./validNumPlayers"

export type NumPlayers = typeof validNumPlayers[number]
