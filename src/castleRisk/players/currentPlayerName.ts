import { HasCurrentPlayerIndex } from "../phases/common/HasCurrentPlayerIndex";
import { HasPlayers } from "../phases/common/HasPlayers";
import { currentPlayerFromState } from "./currentPlayerFromState";

export const currentPlayerName = (state: HasPlayers & HasCurrentPlayerIndex) => currentPlayerFromState(state).name;
