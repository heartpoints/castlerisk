
import { z } from "zod";

//TODO: constrain string to territory names specifically
export const HasHiddenArmies = z.object({ hiddenArmyTerritoryNames: z.array(z.string()) });
export type HasHiddenArmies = z.infer<typeof HasHiddenArmies>;
