import { Player } from './Player'
import { Players } from './Players'

export const playerNameNotDuplicated = 
    (players: Players) => 
    (possibleNewPlayer: Player) => 
    !players
        .map(p => p.name.toLowerCase())
        .includes(possibleNewPlayer.name.toLowerCase())
