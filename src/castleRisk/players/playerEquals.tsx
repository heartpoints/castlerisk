import { EqualityCheck } from "../../utils/axioms/EqualityCheck";
import { Player } from "./Player";

export const playerEquals: EqualityCheck<Player> =
  (p1: Player) => (p2: Player) =>
    p1.id === p2.id;
