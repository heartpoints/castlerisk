import { colorStyle } from "../../utils/colors/colorStyle";
import { lightenColor } from "../../utils/colors/lightenColor";
import { Player } from "./Player";
import { highlightedPlayerBoxStyle } from "./highlightedPlayerBoxStyle";

//TODO: Bug if two players have same name!
export const PlayerBox = (p: Player) => (isCurrent: boolean) => {
  const color = isCurrent ? lightenColor(0.5)(p.color) : p.color;
  const divStyle = {
    ...(isCurrent ? highlightedPlayerBoxStyle : {}),
    ...colorStyle(color),
    padding: "1em",
  };
  return (
    <div style={divStyle} key={p.name}>
      <p>
        <strong>
          {isCurrent && ">>> "}
          {p.name}
        </strong>
      </p>
    </div>
  );
};
