import { Castle } from "../castles/Castle";
import { russianEmpire } from "../map/empires/russian/russianEmpire";
import { pieceColors } from "../pieces/pieceColors";
import { initialPlayer } from "./initialPlayer";

//TODO: use references / names of empires instead of references for serializability
const tommyCastle:Castle = { empireName: russianEmpire.name }
export const tommy = initialPlayer("Tommy", pieceColors[0], [tommyCastle]);
