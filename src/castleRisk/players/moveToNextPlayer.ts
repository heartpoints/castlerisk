import { produce } from "immer";
import { CastleRiskPlayState } from "../phases/common/CastleRiskPlayState";
import { nextPlayerIndex } from "./nextPlayerIndex";
import { currentPlayerFromState } from "./currentPlayerFromState";
import { isType } from "@utils/zod/isType";
import { Diplomat } from "castleRisk/cards/diplomat/Diplomat";
import { new_ } from "@utils/zod/new_";
import { PlayerCard } from "castleRisk/cards/PlayerCard";
import { not } from "@utils/predicates/not";

export const moveToNextPlayer = <S extends CastleRiskPlayState>(
  state: S
): S => {
  const nextState = {
    ...state,
    currentPlayerIndex: nextPlayerIndex(state),
    hasAttacked: false,
  };
  return produce(nextState, (draft) => {
    const isActiveDiplomat = (c: PlayerCard) =>
      isType(Diplomat)(c) && c.isActive;
    const currentPlayerCards = currentPlayerFromState(draft).cards;
    const activeDiplomats = currentPlayerCards.filter(isActiveDiplomat);
    currentPlayerFromState(draft).cards = currentPlayerCards.filter(
      not(isActiveDiplomat)
    );
    draft.discardPile.push(
      ...activeDiplomats.map((d) => new_(Diplomat)({ isActive: false }))
    );
  });
};
