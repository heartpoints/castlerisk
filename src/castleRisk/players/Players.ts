import { Player } from "./Player";

import { z } from "zod";

export const Players = z.array(Player);
export type Players = z.infer<typeof Players>;
