import { PlacedInHand } from "../pieces/PlacedInHand";
import { currentPlayerFromState } from "./currentPlayerFromState";
import { PlaceCastlesState } from "../phases/placeCastles/PlaceCastlesState";
import { isType } from "@utils/zod/isType";

export const playerHandIsEmpty = (state: PlaceCastlesState) =>
  !currentPlayerFromState(state).pieces.some(isType(PlacedInHand));
