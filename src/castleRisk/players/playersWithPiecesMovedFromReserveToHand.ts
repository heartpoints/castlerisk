import { Players } from './Players'
import { moveUpToNPiecesFromReservesToHand } from '../pieces/moveUpToNPiecesFromReservesToHand'

export const playersWithPiecesMovedFromReserveToHand = 
    (players:Players) =>
    (playerIndex:number) =>
    (numPiecesMax:number) => 
    players.map(
        (player, index) =>
        index === playerIndex
            ? { ...player, pieces: moveUpToNPiecesFromReservesToHand(numPiecesMax)(player.pieces) }
            : player
    )