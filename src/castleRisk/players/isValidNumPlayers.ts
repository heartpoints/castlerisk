import { NumPlayers } from './NumPlayers';
import { isInteger } from '../../utils/math/isInteger';
import { validNumPlayers } from './validNumPlayers';

export const isValidNumPlayers = (num: number): num is NumPlayers => isInteger(num) && (validNumPlayers as readonly number[]).includes(num);
