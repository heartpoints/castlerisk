import { z } from "zod";
import { ID } from "./ID";

export const HasID = z.object({ id: ID });
export type HasID = z.infer<typeof HasID>;
