import { CastleRiskPlayState } from "../phases/common/CastleRiskPlayState";
import { currentPlayerFromState } from "./currentPlayerFromState";
import { drawText } from "../../utils/canvas/drawText";
import { white } from "../../utils/colors/white";
import { pointPlus } from "../../utils/geometry/pointPlus";
import { playerNameByMouseOffset } from "./playerNameByMouseOffset";
import { canvasWidth } from "../config/canvasWidth";
import { canvasHeight } from "../config/canvasHeight";
import { doNothing } from "../../utils/axioms/doNothing";

export const drawPlayerNameByMouse = (state: CastleRiskPlayState) =>
  state.mousePoint.x > 0 &&
  state.mousePoint.x < canvasWidth &&
  state.mousePoint.y > 0 &&
  state.mousePoint.y < canvasHeight
    ? drawText(true)(white)(
        pointPlus(state.mousePoint)(playerNameByMouseOffset)
      )(currentPlayerFromState(state).name)
    : doNothing;
