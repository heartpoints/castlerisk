import { isNotLastIndex } from '../../utils/arrays/isNotLastIndex'
import { plus } from '../../utils/math/plus'
import { maybe } from '../../utils/maybe/maybe'
import { CastleRiskPlayState } from '../phases/common/CastleRiskPlayState'

export const nextPlayerIndex = (c: CastleRiskPlayState) => maybe(c.currentPlayerIndex).if(isNotLastIndex(c.players)).mapOrDefault(plus(1), 0)
