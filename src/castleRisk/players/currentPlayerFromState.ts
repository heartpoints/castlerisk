import { HasCurrentPlayerIndex } from "../phases/common/HasCurrentPlayerIndex";
import { HasPlayers } from "../phases/common/HasPlayers";

export const currentPlayerFromState = (c: HasPlayers & HasCurrentPlayerIndex) => c.players[c.currentPlayerIndex];
