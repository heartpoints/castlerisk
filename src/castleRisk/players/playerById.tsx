import { HasPlayers } from "castleRisk/phases/common/HasPlayers";
import { ID } from "castleRisk/players/ID";

export const playerById = (id: ID, state: HasPlayers) =>
  state.players.find((p) => p.id == id)!;
