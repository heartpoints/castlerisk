import { black } from '../../utils/colors/black';
import { blue } from '../../utils/colors/blue';
import { green } from '../../utils/colors/green';
import { red } from '../../utils/colors/red';
import { yellow } from '../../utils/colors/yellow';
import { castleRiskPurple } from '../config/castleRiskPurple';

export const pieceColors = [
    red,
    green,
    yellow,
    black,
    castleRiskPurple,
    blue
];
