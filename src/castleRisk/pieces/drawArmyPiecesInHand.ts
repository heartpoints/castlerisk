import { pipe } from "fp-ts/lib/function"
import { numPiecesInHand } from "./numPiecesInHand"
import { CastleRiskPlayState } from "../phases/common/CastleRiskPlayState"
import { currentPlayerFromState } from "../players/currentPlayerFromState"
import { drawArmyPiecesHorizontally } from "./drawArmyPiecesHorizontally"

export const drawArmyPiecesInHand = 
    (state:CastleRiskPlayState) => 
    drawArmyPiecesHorizontally
        (pipe(state, currentPlayerFromState, numPiecesInHand))
        (state.mousePoint)
        (currentPlayerFromState(state).color)
