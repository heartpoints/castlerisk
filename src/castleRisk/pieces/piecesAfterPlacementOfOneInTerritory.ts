import { new_ } from "@utils/zod/new_";
import { list } from "../../utils/list/list";
import { Territory } from "../territories/Territory";
import { Pieces } from "./Pieces";
import { PlacedInHand } from "./PlacedInHand";
import { PlacedInTerritory } from "./PlacedInTerritory";
import { isType } from "@utils/zod/isType";

export const piecesAfterPlacementOfOneInTerritory =
  (pieces: Pieces) => (territory: Territory) =>
    list(...pieces).replaceFirstT(isType(PlacedInHand), (p) =>
      new_(PlacedInTerritory)({ ...p, territoryName: territory.name })
    ).asArray;
