import { isType } from "../../utils/zod/isType"
import { Player } from "../players/Player"
import { PlacedInTerritory } from "./PlacedInTerritory"

export const placedPiecesForPlayer = (player: Player) => player.pieces.filter(isType(PlacedInTerritory))
