import { PlacedInHand } from "./PlacedInHand";
import { PlacedInReserves } from "./PlacedInReserves";
import { PlacedInTerritory } from "./PlacedInTerritory";

import { z } from "zod";
import { PlacedOnBoat } from "./PlacedOnBoat";

export const Piece = z.discriminatedUnion("URI", [
  PlacedInTerritory,
  PlacedInHand,
  PlacedInReserves,
  PlacedOnBoat,
]);
export type Piece = z.infer<typeof Piece>;
