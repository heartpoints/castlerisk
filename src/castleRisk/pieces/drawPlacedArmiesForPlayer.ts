import { sequentially } from '../../utils/composition/sequentially'
import { Player } from '../players/Player'
import { Players } from '../players/Players'
import { drawPieceCount } from './drawPieceCount'
import { drawPieceInTerritory } from './drawPieceInTerritory'
import { territoryPieceCountPairsForPlayer } from './territoryPieceCountPairs'

export const drawPlacedArmiesForPlayer = 
    (players:Players) =>
    (player: Player) =>
    sequentially(
        territoryPieceCountPairsForPlayer(players)(player).map(
            ([territory, pieceCount]) => 
            sequentially([
                drawPieceInTerritory
                    (player.color)
                    (territory),
                drawPieceCount(territory)(player)(pieceCount)
            ])
        )
    )