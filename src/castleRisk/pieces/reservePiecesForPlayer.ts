import { isType } from '../../utils/zod/isType';
import { Player } from '../players/Player';
import { PlacedInReserves } from "./PlacedInReserves";

export const reservePiecesForPlayer = 
    (player: Player) => 
    player.pieces.filter(isType(PlacedInReserves))
