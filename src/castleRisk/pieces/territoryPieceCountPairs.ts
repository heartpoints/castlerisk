import { Pairs } from '../../utils/arrays/Pairs'
import { Player } from '../players/Player'
import { Players } from '../players/Players'
import { Territory } from '../territories/Territory'
import { territoriesForPlayer } from '../territories/territoriesForPlayer'
import { numPiecesInTerritory } from './numPiecesInTerritory'

export const territoryPieceCountPairsForPlayer = 
    (players:Players) =>
    (player: Player):Pairs<Territory, number> => 
    territoriesForPlayer(player).map(
        t => [t, numPiecesInTerritory(players)(t)]
    )