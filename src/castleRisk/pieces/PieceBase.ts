import { HasColor } from "../../utils/colors/HasColor";

import { z } from "zod";

//TODO: We neednt explicitly add color to every piece; its a function of the player's color
export const PieceBase = HasColor
export type PieceBase = z.infer<typeof PieceBase>;
