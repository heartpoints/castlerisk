import { Pieces } from "./Pieces";

import { z } from "zod";

export const HasPieces = z.object({ pieces: Pieces });
export type HasPieces = z.infer<typeof HasPieces>;
