import { PlacedInHand } from "./PlacedInHand";
import { CastleRiskPlayState } from "../phases/common/CastleRiskPlayState";
import { isType } from "@utils/zod/isType";

export const isPieceInSomePlayersHand = (state: CastleRiskPlayState) =>
  state.players.flatMap((p) => p.pieces).some(isType(PlacedInHand));
