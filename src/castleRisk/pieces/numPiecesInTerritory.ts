import { Players } from "../players/Players";
import { Territory } from "../territories/Territory";
import { piecesInTerritory } from "./piecesInTerritory";

export const numPiecesInTerritory =
  (players: Players) => (territory: Territory) =>
    piecesInTerritory(players)(territory).length;
