import { HasURI } from "@utils/zod/HasURI";
import { z } from "zod";
import { PieceBase } from "./PieceBase";


export const PlacedOnBoat = z.object({
  ...HasURI("PlacedOnBoat").shape,
  ...PieceBase.shape,
});
export type PlacedOnBoat = z.infer<typeof PlacedOnBoat>;
