import { z } from "zod";
import { HasURI } from "../../utils/zod/HasURI";
import { PieceBase } from "./PieceBase";

export const PlacedInReserves = z.object({
  ...HasURI("PlacedInReserves").shape,
  ...PieceBase.shape,
});
export type PlacedInReserves = z.infer<typeof PlacedInReserves>;
