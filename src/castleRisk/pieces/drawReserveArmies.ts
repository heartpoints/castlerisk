import { sequentially } from "../../utils/composition/sequentially";
import { clearRect } from "../../utils/canvas/clearRect";
import { translatePointY } from "../../utils/geometry/translatePointY";
import { paddingBetweenPlayerBoxes } from "../players/paddingBetweenPlayerBoxes";
import { playerBoxOrigin } from "../players/playerBoxOrigin";
import { playerBoxSize } from "../players/playerBoxSize";
import { Players } from "../players/Players";
import { drawPlayerReserveArmies } from "./drawPlayerReserveArmies";
import { Player } from "../players/Player";
import { playerEquals } from "../players/playerEquals";
import { playerBoxHeight } from "../players/playerBoxHeight";

export const drawReserveArmies =
  (players: Players) => (currentPlayer: Player) => {
    const playerOriginPairs = players.map((player, i) => ({
      player,
      origin: translatePointY(
        (playerBoxHeight(players.length) + paddingBetweenPlayerBoxes) * i
      )(playerBoxOrigin),
    }));

    return sequentially([
      clearRect(playerBoxOrigin)(playerBoxSize),
      ...playerOriginPairs.map(({ player, origin }) =>
        drawPlayerReserveArmies(player)(playerEquals(currentPlayer)(player))(
          origin
        )
      ),
    ]);
  };
