import { Players } from "../players/Players";
import { Territory } from "../territories/Territory";
import { allPlacedPiecesForPlayers } from "./allPlacedPiecesForPlayers";

export const piecesInTerritory = (players: Players) => (territory: Territory) => allPlacedPiecesForPlayers(players).filter(
    (p) => p.territoryName === territory.name
);
