import { translatePoint } from '../../utils/geometry/translatePoint';
import { Point } from '../../utils/geometry/Point';
import { piecesOffset } from './piecesOffset';

export const xLocationByIndex = (origin: Point) => i => translatePoint(piecesOffset(i))(origin);
