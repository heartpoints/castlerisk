import { isType } from "@utils/zod/isType";
import { Territory } from "../territories/Territory";
import { Piece } from "./Piece";
import { PlacedInTerritory } from "./PlacedInTerritory";

export const isPlacedTerritory =
  (territory: Territory) =>
  (piece: Piece): piece is PlacedInTerritory =>
    isType(PlacedInTerritory)(piece) && piece.territoryName === territory.name;
