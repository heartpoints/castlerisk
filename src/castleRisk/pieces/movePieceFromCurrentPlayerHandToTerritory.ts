import { piecesAfterPlacementOfOneInTerritory } from './piecesAfterPlacementOfOneInTerritory'
import { Territory } from '../territories/Territory'
import { CastleRiskPlayState } from '../phases/common/CastleRiskPlayState'

export const movePieceFromCurrentPlayerHandToTerritory = 
    (territory:Territory) =>
    <T extends CastleRiskPlayState>(state:T) => 
    ({
        ...state,
        players: state.players.map(
            (player, playerIndex) =>
            playerIndex === state.currentPlayerIndex
                ? ({
                    ...player,
                    pieces: piecesAfterPlacementOfOneInTerritory(player.pieces)(territory)
                })
                : player
        )
    })

