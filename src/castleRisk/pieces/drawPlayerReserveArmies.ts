import { drawRectangle } from "../../utils/canvas/drawRectangle";
import { drawText } from "../../utils/canvas/drawText";
import { black } from "../../utils/colors/black";
import { lightenColor } from "../../utils/colors/lightenColor";
import { white } from "../../utils/colors/white";
import { sequentially } from "../../utils/composition/sequentially";
import { Point } from "../../utils/geometry/Point";
import { pointPlus } from "../../utils/geometry/pointPlus";
import { translatePoint } from "../../utils/geometry/translatePoint";
import { Castle } from "../castles/Castle";
import { castleTokenRadius } from "../castles/castleTokenRadius";
import { drawCastlePiece } from "../castles/drawCastlePiece";
import { empireNamed } from "../empires/empireNamed";
import { Player } from "../players/Player";
import { playerBoxSize } from "../players/playerBoxSize";
import { drawArmyPiecesHorizontally } from "./drawArmyPiecesHorizontally";
import { reservePiecesForPlayer } from "./reservePiecesForPlayer";

export const drawPlayerReserveArmies =
  (player: Player) => (isCurrent: boolean) => (origin: Point) => {
    const { color } = player;
    const reservePieces = reservePiecesForPlayer(player);
    const margin = { x: 10, y: 10 };
    const rowHeight = 30;
    const rowOffset = { x: 0, y: rowHeight };
    const pieceRowStartingPoint = pointPlus(origin)(rowOffset);
    const castleTokenOffset = pointPlus(rowOffset)({ x: 20, y: 10 });
    const castleTokenRowStartingPoint = pointPlus(pieceRowStartingPoint)(
      castleTokenOffset
    );
    const playerBGColor = lightenColor(0.5)(color);
    const bgRect =
      drawRectangle(origin)(playerBoxSize)(playerBGColor)(playerBGColor);
    const textColor = isCurrent ? white : black;
    const playerDisplayName = `${isCurrent ? ">>> " : ""}${player.name}`;
    const drawPlayerName = drawText(isCurrent)(textColor)(
      translatePoint(margin)(origin)
    )(playerDisplayName);

    const drawPlayerCastleToken = ({ empireName }: Castle, i: number) =>
      drawCastlePiece(empireNamed(empireName))(
        pointPlus(castleTokenRowStartingPoint)({
          x: i * (castleTokenRadius + 3) * 2,
          y: 0,
        })
      );

    const drawPlayerCastleTokens = player.castles.map(drawPlayerCastleToken);

    return sequentially([
      bgRect,
      drawPlayerName,
      drawArmyPiecesHorizontally(reservePieces.length)(pieceRowStartingPoint)(
        color
      ),
      ...drawPlayerCastleTokens,
    ]);
  };
