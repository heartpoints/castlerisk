import { identity } from '../../utils/axioms/identity';
import { centroidOfPolygon } from '../../utils/geometry/centroidOfPolygon';
import { RGBAColor } from '../../utils/colors/RGBAColor';
import { Territory } from '../territories/Territory';
import { drawSingleArmyPiece } from './drawSingleArmyPiece';
import { pointPlus } from '../../utils/geometry/pointPlus';
import { defaultFontPixels } from '../../utils/canvas/defaultFontPixels';

export const drawPieceInTerritory = 
    (color: RGBAColor) => 
    (territory?: Territory) => 
    territory
        ? drawSingleArmyPiece(color)(pointPlus({x: 0, y: defaultFontPixels})(centroidOfPolygon(territory.boundary)))
        : identity
