import { Players } from '../players/Players';

export const allPiecesForPlayers = (players: Players) => players.flatMap(p => p.pieces);
