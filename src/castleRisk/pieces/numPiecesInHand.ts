import { PlacedInHand } from "./PlacedInHand";
import { Player } from "../players/Player";
import { isType } from "@utils/zod/isType";

export const numPiecesInHand = (player: Player) =>
  player.pieces.filter(isType(PlacedInHand)).length;
