import { range } from '../..//utils/list/range';
import { numericPlus } from '../..//utils/math/numericPlus';
import { RGBAColor } from '../../utils/colors/RGBAColor';
import { sequentially } from '../../utils/composition/sequentially';
import { Point } from '../../utils/geometry/Point';
import { floor } from '../../utils/math/floor';
import { draw10ArmyPiece } from './draw10ArmyPiece';
import { drawSingleArmyPiece } from './drawSingleArmyPiece';
import { xLocationByIndex } from './xLocationByIndex';

export const drawArmyPiecesHorizontally = 
    (numPieces:number) =>
    (origin: Point) => 
    (color:RGBAColor) =>
    {
        const num10Pieces = floor(numPieces / 10)
        const numSinglePieces = numPieces % 10

        const draw10s = range(num10Pieces)
            .map(xLocationByIndex(origin))
            .map(draw10ArmyPiece(color))
        
        const draw1s = range(numSinglePieces)
            .map(numericPlus(num10Pieces))
            .map(xLocationByIndex(origin))
            .map(drawSingleArmyPiece(color))

        return sequentially([
            ...draw10s, 
            ...draw1s
        ])
    }