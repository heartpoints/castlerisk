import { z } from "zod";
import { HasTerritoryName } from "../territories/HasTerritoryName";
import { PieceBase } from "./PieceBase";
import { HasURI } from "@utils/zod/HasURI";

export const PlacedInTerritory = z.object({
  ...HasURI("PlacedInTerritory").shape,
  ...PieceBase.shape,
  ...HasTerritoryName.shape,
});
export type PlacedInTerritory = z.infer<typeof PlacedInTerritory>;
