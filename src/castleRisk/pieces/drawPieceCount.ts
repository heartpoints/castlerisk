import { Player } from "../players/Player";
import { drawText } from "../../utils/canvas/drawText";
import { centroidOfPolygon } from "../../utils/geometry/centroidOfPolygon";
import { Territory } from "../territories/Territory";
import { pointPlus } from "../../utils/geometry/pointPlus";
import { doNothing } from "../../utils/axioms/doNothing";
import { defaultFontPixels } from "../../utils/canvas/defaultFontPixels";
import { black } from "../../utils/colors/black";

export const drawPieceCount =
  (territory: Territory) => (player: Player) => (pieceCount: number) =>
    pieceCount < 2
      ? doNothing
      : drawText(true)(black)(
          pointPlus(centroidOfPolygon(territory!.boundary))({
            x: 15,
            y: defaultFontPixels,
          })
        )(`x${pieceCount}`);
