import { pipe } from "fp-ts/lib/function"
import { length } from "../../utils/arrays/length"
import { withValue } from "../../utils/axioms/withValue"
import { minCurried } from "../../utils/math/minCurried"
import { Pieces } from "./Pieces"
import { PlacedInReserves } from "./PlacedInReserves"
import { PlacedInTerritory } from "./PlacedInTerritory"
import { moveNReservePiecesToHand } from "./moveNReservePiecesToHand"
import { isType } from "@utils/zod/isType"

export const moveUpToNPiecesFromReservesToHand = 
    (numPiecesMax:number)=> 
    (pieces:Pieces):Pieces =>
    withValue
        (pieces.filter(isType(PlacedInReserves)))
        (
            reservePieces =>
            pipe(
                reservePieces,
                length,
                minCurried(numPiecesMax),
                moveNReservePiecesToHand(reservePieces),
                nonPlacedPieces => [...nonPlacedPieces, ...pieces.filter(isType(PlacedInTerritory))]
            )
        )