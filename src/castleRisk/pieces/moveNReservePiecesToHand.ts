import { new_ } from "@utils/zod/new_";
import { PlacedInHand } from "./PlacedInHand";
import { PlacedInReserves } from "./PlacedInReserves";

export const moveNReservePiecesToHand =
  (reservePieces: PlacedInReserves[]) => (numPiecesToMove: number) =>
    reservePieces.map((piece, index) =>
      index < numPiecesToMove ? new_(PlacedInHand)({ ...piece }) : piece
    );
