import { PlacedInTerritory } from "./PlacedInTerritory";
import { Players } from "../players/Players";
import { allPiecesForPlayers } from "./allPiecesForPlayers";
import { isType } from "@utils/zod/isType";

export const allPlacedPiecesForPlayers = (players: Players) =>
  allPiecesForPlayers(players).filter(isType(PlacedInTerritory));
