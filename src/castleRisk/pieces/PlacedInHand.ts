import { PieceBase } from "./PieceBase";

import { HasURI } from "@utils/zod/HasURI";
import { z } from "zod";

export const PlacedInHand = z.object({
  ...HasURI("PlacedInHand").shape,
  ...PieceBase.shape,
});
export type PlacedInHand = z.infer<typeof PlacedInHand>;
