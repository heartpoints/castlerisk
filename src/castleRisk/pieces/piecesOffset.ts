import { armyPieceDiameter } from './armyPieceDiameter';
import { armyPieceRadius } from './armyPieceRadius';

export const piecesOffset = i => ({ x: i * armyPieceDiameter + armyPieceRadius, y: armyPieceRadius });
