import { Piece } from './Piece'
import { Player } from '../players/Player'
import { PlacedInReserves } from './PlacedInReserves'
import { new_ } from '@utils/zod/new_'

export const newArmyReservePiece = 
    ({color}: Player): Piece => 
    new_(PlacedInReserves)({color})