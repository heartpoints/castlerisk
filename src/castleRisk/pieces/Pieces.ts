import { Piece } from "./Piece";

import { z } from "zod";

export const Pieces = z.array(Piece);
export type Pieces = z.infer<typeof Pieces>;
