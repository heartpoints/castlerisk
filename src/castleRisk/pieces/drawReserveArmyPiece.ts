import { drawPolygon } from '../../utils/canvas/drawPolygon';
import { black } from '../../utils/colors/black';
import { translatePointX } from '../../utils/geometry/translatePointX';
import { translateShape } from '../../utils/geometry/translateShape';
import { Point } from '../../utils/geometry/Point';
import { RGBAColor } from '../../utils/colors/RGBAColor';
import { Shape } from '../../utils/geometry/Shape';

export const drawReserveArmyPiece = 
    (pieceShape:Shape) =>
    (color: RGBAColor) =>
    (origin: Point) => 
    {
        const piecePadding = 8;
        const drawArmyPiece = drawPolygon(black)(color);
        return drawArmyPiece
            (translateShape
                (translatePointX(piecePadding)(origin))
                (pieceShape)
            )
    };
