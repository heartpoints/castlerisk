import { isType } from "@utils/zod/isType";
import { replaceN } from "../../utils/arrays/replaceN";
import { new_ } from "../../utils/zod/new_";
import { Territory } from "../territories/Territory";
import { isPlacedTerritory } from "./isPlacedTerritory";
import { Pieces } from "./Pieces";
import { PlacedInReserves } from "./PlacedInReserves";
import { PlacedOnBoat } from "./PlacedOnBoat";

export const piecesAfterRemovingDefeated =
  (pieces: Pieces) =>
  (territory: Territory) =>
  (num: number) =>
  (isAdmiralAttacker: boolean) =>
    isAdmiralAttacker
      ? replaceN(
          pieces,
          isType(PlacedOnBoat),
          ({ color }) => new_(PlacedInReserves)({ color }),
          num
        )
      : replaceN(
          pieces,
          isPlacedTerritory(territory),
          ({ color }) => new_(PlacedInReserves)({ color }),
          num
        );
