import { getName } from "@utils/axioms/getName";
import { not } from "@utils/predicates/not";
import { britishEmpire } from "castleRisk/map/empires/british/britishEmpire";
import { frenchEmpire } from "castleRisk/map/empires/french/frenchEmpire";
import { Paris } from "castleRisk/map/empires/french/Paris";
import { Berlin } from "castleRisk/map/empires/german/Berlin";
import { Prussia } from "castleRisk/map/empires/german/Prussia";
import { Rhine } from "castleRisk/map/empires/german/Rhine";
import { cypress } from "castleRisk/map/empires/ottoman/cypress";
import { montenegro } from "castleRisk/map/empires/ottoman/montenegro";
import { turkey } from "castleRisk/map/empires/ottoman/turkey";
import { livonia } from "castleRisk/map/empires/russian/livonia";
import { stPetersberg } from "castleRisk/map/empires/russian/stPetersburg";
import { independentTerritories } from "castleRisk/map/independent/independentTerritories";
import { Switzerland } from "castleRisk/map/independent/Switzerland";
import { territoryEquals } from "castleRisk/territories/territoryEquals";
import { BodyOfWater } from "./BodyOfWater";
import { Vienna } from "castleRisk/map/empires/austrian/Vienna";
import { Trieste } from "castleRisk/map/empires/austrian/Trieste";

export const atlanticOcean: BodyOfWater = {
    labelCoordinate: { x: 150, y: 400 },
    name: "Atlantic Ocean",
    territoryNames: [
        ...independentTerritories.territories.filter(
            not(territoryEquals(Switzerland))
        ),
        ...frenchEmpire.territories.filter(not(territoryEquals(Paris))),
        ...britishEmpire.territories,
        Prussia,
        Berlin,
        Rhine,
        livonia,
        stPetersberg,
        montenegro,
        cypress,
        turkey,
        Vienna,
        Trieste,
    ].map(getName),
} as const;
