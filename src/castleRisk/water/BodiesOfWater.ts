import { atlanticOcean } from "./atlanticOcean";
import { blackSea } from "./blackSea";

export const bodiesOfWater = [atlanticOcean, blackSea] as const;
