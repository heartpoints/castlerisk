import { drawPolygon } from "../../utils/canvas/drawPolygon";
import { seaColor } from "./seaColor";
import { rectangle } from "../../utils/geometry/rectangle";
import { CanvasEffect } from "../../utils/canvas/CanvasEffect";

export const drawSea: CanvasEffect = drawPolygon(seaColor)(seaColor)(
  rectangle(1000)(1000)
);
