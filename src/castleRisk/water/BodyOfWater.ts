import { Name } from "@utils/axioms/Name";
import { Point } from "@utils/geometry/Point";
import { Territory } from "castleRisk/territories/Territory";
import { territoryByName } from "castleRisk/territories/territoryByName";
import { uniqueStrings } from "../../utils/hashing/uniqueStrings";
import { bodiesOfWater } from "./bodiesOfWater";

export type BodyOfWater = {
  labelCoordinate: Point;
  name: string;
  territoryNames: Name[];
};

const territoriesAccessibleByWater = (fromTerritory: Territory) =>
  uniqueStrings(
    bodiesOfWater
      .filter((bw) => bw.territoryNames.includes(fromTerritory.name))
      .flatMap((b) => b.territoryNames)
  ).map(territoryByName);
