import { RGBAColor } from "../../utils/colors/RGBAColor";

export const seaColor: RGBAColor = [178, 208, 216, 255];
