import { getName } from "@utils/axioms/getName";
import { bulgaria } from "castleRisk/map/empires/ottoman/bulgaria";
import { rumania } from "castleRisk/map/empires/ottoman/rumania";
import { turkey } from "castleRisk/map/empires/ottoman/turkey";
import { ukraine } from "castleRisk/map/empires/russian/ukraine";
import { BodyOfWater } from "./BodyOfWater";

export const blackSea: BodyOfWater = {
    labelCoordinate: { x: 1300, y: 700 },
    name: "Black Sea",
    territoryNames: [turkey, bulgaria, rumania, ukraine].map(getName),
};
