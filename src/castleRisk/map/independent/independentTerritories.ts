import { new_ } from "@utils/zod/new_";
import { darkenColor } from "../../../utils/colors/darkenColor";
import { green } from "../../../utils/colors/green";
import { denmark } from "./denmark";
import { finland } from "./finland";
import { Independents } from "./Independents";
import { Macedonia } from "./Macedonia";
import { Madrid } from "./Madrid";
import { Naples } from "./Naples";
import { norway } from "./norway";
import { Portugal } from "./Portugal";
import { Rome } from "./Rome";
import { Sweden } from "./Sweden";
import { Switzerland } from "./Switzerland";
import { Venice } from "./Venice";

export const independentTerritories = new_(Independents)({
  color: darkenColor(0.2)(green),
  name: "Independent",
  territories: [
    finland,
    norway,
    denmark,
    Sweden,
    Switzerland,
    Venice,
    Rome,
    Naples,
    Macedonia,
    Madrid,
    Portugal,
  ],
});
