export const finland = {
  name: "Finland",
  boundary: [
    {
      x: 689,
      y: 0,
    },
    {
      x: 695,
      y: 53,
    },
    {
      x: 725,
      y: 59,
    },
    {
      x: 772,
      y: 40,
    },
    {
      x: 785,
      y: 41,
    },
    {
      x: 790,
      y: 0,
    },
  ],
};
