import { TerritoryGroupBase } from "../../empires/TerritoryGroupBase";

import { HasURI } from "@utils/zod/HasURI";
import { z } from "zod";

export const Independents = z.object({
  ...HasURI("Independents").shape,
  ...TerritoryGroupBase.shape,
});
export type Independents = z.infer<typeof Independents>;
