import { independentTerritories } from "./independent/independentTerritories";
import { empires } from "./empires/empires";

export const territoryGroups = () => [
    ...empires(),
    independentTerritories
];
