import { territoryGroups } from "./territoryGroups"

export const territories = () => 
    territoryGroups().flatMap(e => e.territories)
