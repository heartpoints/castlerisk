import { austrianEmpire } from "./austrian/austrianEmpire";
import { britishEmpire } from "./british/britishEmpire";
import { frenchEmpire } from "./french/frenchEmpire";
import { germanEmpire } from "./german/germanEmpire";
import { ottomanEmpire } from "./ottoman/ottomanEmpire";
import { russianEmpire } from "./russian/russianEmpire";

export const empires = () => [
    russianEmpire,
    germanEmpire,
    frenchEmpire,
    ottomanEmpire,
    austrianEmpire,
    britishEmpire,
];
