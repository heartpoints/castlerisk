import { new_ } from "@utils/zod/new_";
import { darkenColor } from "../../../../utils/colors/darkenColor";
import { orange } from "../../../../utils/colors/orange";
import { Empire } from "../../../empires/Empire";
import { Bavaria } from "./Bavaria";
import { Berlin } from "./Berlin";
import { Prussia } from "./Prussia";
import { Rhine } from "./Rhine";
import { saxony } from "./saxony";

export const germanEmpire = new_(Empire)({
  color: darkenColor(0.15)(orange),
  name: "German Empire",
  territories: [
    saxony,
    Prussia,
    Bavaria,
    Berlin,
    Rhine,
  ],
});
