import { new_ } from "@utils/zod/new_";
import { desaturateColor } from "../../../../utils/colors/desaturateColor";
import { Empire } from "../../../empires/Empire";
import { Ireland } from "./Ireland";
import { London } from "./London";
import { Scotland } from "./Scotland";
import { Wales } from "./Wales";
import { Yorkshire } from "./Yorkshire";

export const britishEmpire = new_(Empire)({
  color: desaturateColor(0.4)([200, 0, 255, 255]),
  name: "British Empire",
  territories: [
    London,
    Wales,
    Yorkshire,
    Ireland,
    Scotland,
  ],
});
