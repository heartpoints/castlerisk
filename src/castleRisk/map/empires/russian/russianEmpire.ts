import { new_ } from "@utils/zod/new_";
import { red } from "../../../../utils/colors/red";
import { Empire } from "../../../empires/Empire";
import { livonia } from "./livonia";
import { moscow } from "./moscow";
import { poland } from "./poland";
import { smolensk } from "./smolensk";
import { stPetersberg } from "./stPetersburg";
import { ukraine } from "./ukraine";

export const russianEmpire = new_(Empire)({
  color: red,
  name: "Russian Empire",
  territories: [
    stPetersberg,
    poland,
    livonia,
    moscow,
    smolensk,
    ukraine,
  ],
});
