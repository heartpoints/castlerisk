export const Bohemia = {
  name: "Bohemia",
  boundary: [
    {
      x: 625,
      y: 309,
    },
    {
      x: 615,
      y: 278,
    },
    {
      x: 657,
      y: 279,
    },
    {
      x: 673,
      y: 298,
    },
    {
      x: 663,
      y: 309,
    },
  ],
};
