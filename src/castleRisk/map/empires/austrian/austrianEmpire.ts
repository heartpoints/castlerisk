import { new_ } from "@utils/zod/new_";
import { darkenColor } from "../../../../utils/colors/darkenColor";
import { yellow } from "../../../../utils/colors/yellow";
import { Empire } from "../../../empires/Empire";
import { Bohemia } from "./Bohemia";
import { galecia } from "./galecia";
import { hungary } from "./hungary";
import { Trieste } from "./Trieste";
import { Vienna } from "./Vienna";

export const austrianEmpire = new_(Empire)({
  color: darkenColor(0.2)(yellow),
  name: "Austrian Empire",
  territories: [
    galecia,
    hungary,
    Vienna,
    Bohemia,
    Trieste,
  ],
});
