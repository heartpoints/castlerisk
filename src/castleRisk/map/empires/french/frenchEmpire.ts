import { new_ } from "@utils/zod/new_";
import { darkenColor } from "../../../../utils/colors/darkenColor";
import { pink } from "../../../../utils/colors/pink";
import { Empire } from "../../../empires/Empire";
import { Brittany } from "./Brittany";
import { Burgundy } from "./Burgundy";
import { Gascony } from "./Gascony";
import { Marseille } from "./Marseille";
import { netherlands } from "./netherlands";
import { Paris } from "./Paris";

export const frenchEmpire = new_(Empire)({
  color: darkenColor(0.3)(pink),
  name: "French Empire",
  territories: [
    netherlands,
    Burgundy,
    Paris,
    Marseille,
    Gascony,
    Brittany,
  ],
});
