import { new_ } from "@utils/zod/new_";
import { darkenColor } from "../../../../utils/colors/darkenColor";
import { Empire } from "../../../empires/Empire";
import { bulgaria } from "./bulgaria";
import { cypress } from "./cypress";
import { montenegro } from "./montenegro";
import { rumania } from "./rumania";
import { Serbia } from "./Serbia";
import { turkey } from "./turkey";

export const ottomanEmpire = new_(Empire)({
  color: darkenColor(0.3)([130, 255, 210, 255]),
  name: "Ottoman Empire",
  territories: [
    rumania,
    bulgaria,
    Serbia,
    montenegro,
    turkey,
    cypress,
  ],
});
