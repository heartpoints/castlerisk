import { Empire } from "castleRisk/empires/Empire";
import { z } from "zod";
import { Independents } from "./independent/Independents";

export const TerritoryGroup = Independents.or(Empire);
export type TerritoryGroup = z.infer<typeof TerritoryGroup>;
