import { pipe } from 'fp-ts/lib/function';
import { desaturateColor } from '../../utils/colors/desaturateColor';
import { lightenColor } from '../../utils/colors/lightenColor';
import { purple } from '../../utils/colors/purple';

export const castleRiskPurple = pipe(purple, desaturateColor(0.5), lightenColor(0.4));
