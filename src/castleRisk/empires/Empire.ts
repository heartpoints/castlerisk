import { TerritoryGroupBase } from "./TerritoryGroupBase";

import { HasURI } from "@utils/zod/HasURI";
import { z } from "zod";

export const Empire = z.object({
  ...HasURI("Empire").shape,
  ...TerritoryGroupBase.shape,
});
export type Empire = z.infer<typeof Empire>;
