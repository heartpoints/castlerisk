import { segmentsFromShape } from "../../utils/geometry/segmentsFromShape";
import { TerritoryGroup } from "castleRisk/map/TerritoryGroup";

export const territoryGroupsIncludingDupes = (territoryGroup: TerritoryGroup) =>
  territoryGroup.territories.flatMap((territory) =>
    segmentsFromShape(territory.boundary)
  );
