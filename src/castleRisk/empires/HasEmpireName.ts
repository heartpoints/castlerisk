import { z } from "zod";

export const HasEmpireName = z.object({ empireName: z.string().min(1) });
export type HasEmpireName = z.infer<typeof HasEmpireName>;
