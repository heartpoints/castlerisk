import { listFromArray } from '../../utils/list/listFromArray';
import { territoryGroups } from '../map/territoryGroups';
import { Territory } from '../territories/Territory';
import { territoryEquals } from '../territories/territoryEquals';

export const empireContaining = 
    (territory: Territory) => 
    listFromArray(territoryGroups()).first(
        e => e.territories.some(territoryEquals(territory))
    ).value;
