import { listFromArray } from '../../utils/list/listFromArray'
import { empires } from '../map/empires/empires'

export const empireNamed = 
    (empireName:string) => 
    listFromArray(empires())
        .first(e => e.name === empireName)
        .value