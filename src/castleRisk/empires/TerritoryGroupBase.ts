import { HasName } from "../../utils/axioms/HasName";
import { HasTerritories } from "../territories/HasTerritories";
import { HasColor } from "../../utils/colors/HasColor";
import { z } from "zod";

export const TerritoryGroupBase = z.object({
  ...HasName.shape,
  ...HasColor.shape,
  ...HasTerritories.shape,
});
export type TerritoryGroupBase = z.infer<typeof TerritoryGroupBase>;
