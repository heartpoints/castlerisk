import { independentTerritories } from "../map/independent/independentTerritories";
import { Player } from "../players/Player";
import { Players } from "../players/Players";
import { territoryOwnedByPlayer } from "../territories/territoryOwnedByPlayer";

export const isOwnerOfAllIndependentTerritories =
  (players: Players) => (player: Player) =>
    independentTerritories.territories.every(
      territoryOwnedByPlayer(players)(player)
    );
