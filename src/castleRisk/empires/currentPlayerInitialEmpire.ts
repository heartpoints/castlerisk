import { currentPlayerFromState } from '../players/currentPlayerFromState'
import { CastleRiskPlayState } from '../phases/common/CastleRiskPlayState'
import { initialEmpireOfPlayer } from './initialEmpireOfPlayer'

export const currentPlayerInitialEmpire = 
    (state:CastleRiskPlayState) => 
    initialEmpireOfPlayer(currentPlayerFromState(state))