import { Player } from '../players/Player'
import { empireNamed } from './empireNamed'

export const initialEmpireOfPlayer = 
    (player: Player) => 
    empireNamed(player.castles[0].empireName)

