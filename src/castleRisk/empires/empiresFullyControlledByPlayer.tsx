import { isType } from "@utils/zod/isType";
import { HasPlayers } from "../phases/common/HasPlayers";
import { Player } from "../players/Player";
import { Empire } from "./Empire";
import { territoryGroupsControlledByPlayer } from "./territoryGroupsControlledByPlayer";

export const empiresFullyControlledByPlayer = (
  { players }: HasPlayers,
  player: Player
) =>
  territoryGroupsControlledByPlayer({ players }, player).filter(isType(Empire));
