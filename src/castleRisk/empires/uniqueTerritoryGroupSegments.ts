import { segmentsThatDoNotAppearMoreThanOnce } from "../../utils/geometry/segmentsThatDoNotAppearMoreThanOnce";
import { TerritoryGroup } from "../map/TerritoryGroup";
import { territoryGroupsIncludingDupes } from "./empireSegmentsInludingDupes";

export const uniqueTerritoryGroupSegments = (territoryGroup: TerritoryGroup) =>
  segmentsThatDoNotAppearMoreThanOnce(territoryGroupsIncludingDupes(territoryGroup));
