import { territoryGroups } from "../map/territoryGroups";
import { HasPlayers } from "../phases/common/HasPlayers";
import { Player } from "../players/Player";
import { territoryOwnedByPlayer } from "../territories/territoryOwnedByPlayer";


export const territoryGroupsControlledByPlayer = (
    { players }: HasPlayers,
    player: Player
) => territoryGroups().filter((e) => e.territories.every(territoryOwnedByPlayer(players)(player))
);
