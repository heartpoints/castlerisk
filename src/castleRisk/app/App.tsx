import { useState } from 'react'
import { initialState } from './initialState'
import { viewForState } from './viewForState'

export const App =
  () =>
  {
    const [state, setState] = useState(initialState())
    return viewForState(state)(setState)
  }
