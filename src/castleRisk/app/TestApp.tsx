import { useState } from "react";
import { initialCastleRiskStateValue } from "../phases/addPlayers/initialCastleRiskStateValue";
import { CastleRiskState } from "../phases/common/CastleRiskState";
import { viewForState } from "./viewForState";

export const TestApp = () => {
  const [state, setState] = useState(initialCastleRiskStateValue() as any as CastleRiskState);
  return viewForState(state)(setState);
};
