import { initialCastleRiskStateValue } from '../phases/addPlayers/initialCastleRiskStateValue';
import { initialAdminState } from '../phases/admin/initialAdminState';
import { initialStateForChooseAction } from '../phases/chooseAction/initialStateForChooseAction';
import { CastleRiskState } from '../phases/common/CastleRiskState';
import { pickInitialCards } from '../cards/pickInitialCards';
import { pickCardsAtStartOfTurn } from '../cards/pickCardsAtStartOfTurn';

//NOTE: below is for testing by starting out with a convenient initial state
export const initialState = (): CastleRiskState => {
  // const returnValue = initialAdminState()
  // const returnValue = initialCastleRiskStateValue()
  const returnValue = pickCardsAtStartOfTurn(pickInitialCards(initialStateForChooseAction))
  // console.log({returnValue})
  return returnValue;
};
