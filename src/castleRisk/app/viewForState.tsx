import { BoardAdmiralShip } from 'castleRisk/phases/attack/boardAdmiralShip/BoardAdmiralShip'
import { BoardAdmiralShipState } from 'castleRisk/phases/attack/boardAdmiralShip/BoardAdmiralShipState'
import { Rolling } from 'castleRisk/phases/attack/rolling/Rolling'
import { RollingState } from 'castleRisk/phases/attack/rolling/RollingState'
import { PlaceReinforcements } from 'castleRisk/phases/placeReinforcments/PlaceReinforcements'
import { PlaceReinforcementsState } from 'castleRisk/phases/placeReinforcments/PlaceReinforcementsState'
import { PlayDiplomat } from 'castleRisk/phases/playDiplomat/PlayDiplomat'
import { PlayDiplomatState } from 'castleRisk/phases/playDiplomat/PlayDiplomatState'
import { Consumer } from '../../utils/axioms/Consumer'
import { isType } from '../../utils/zod/isType'
import { AddPlayers } from '../phases/addPlayers/AddPlayers'
import { AddPlayersState } from '../phases/addPlayers/AddPlayersState'
import { AdminBorders } from '../phases/admin/AdminBorders'
import { AdminState } from '../phases/admin/AdminState'
import { AttackerWon } from '../phases/attack/attackerWon/AttackerWon'
import { AttackerWonState } from '../phases/attack/attackerWon/AttackerWonState'
import { BattleUndecidedState } from '../phases/attack/battleUndecided/BattleUndecidedState'
import { ChooseAttackOrigin } from '../phases/attack/chooseAttackOrigin/ChooseAttackOrigin'
import { ChooseAttackOriginState } from '../phases/attack/chooseAttackOrigin/ChooseAttackOriginState'
import { ChooseAttackTarget } from '../phases/attack/chooseAttackTarget/ChooseAttackTarget'
import { ChooseAttackTargetState } from '../phases/attack/chooseAttackTarget/ChooseAttackTargetState'
import { ChooseDiceCount } from '../phases/attack/chooseDiceCount/ChooseDiceCount'
import { ChooseDiceCountState } from '../phases/attack/chooseDiceCount/ChooseDiceCountState'
import { DefenderWon } from '../phases/attack/defenderWon/DefenderWon'
import { DefenderWonState } from '../phases/attack/defenderWon/DefenderWonState'
import { ChooseAction } from '../phases/chooseAction/ChooseAction'
import { ChooseActionState } from '../phases/chooseAction/ChooseActionState'
import { ClaimTerritories } from '../phases/claimTerritories/ClaimTerritories'
import { ClaimTerritoriesState } from '../phases/claimTerritories/ClaimTerritoriesState'
import { CastleRiskState } from '../phases/common/CastleRiskState'
import { FillDefeatedTerritories } from '../phases/fillDefeatedTerritories/FillDefeatedTerritories'
import { FillDefeatedTerritoriesState } from '../phases/fillDefeatedTerritories/FillDefeatedTerritoriesState'
import { FortifyTerritories } from '../phases/fortifyTerritories/FortifyTerritories'
import { FortifyTerritoriesState } from '../phases/fortifyTerritories/FortifyTerritoriesState'
import { PlaceHiddenArmies } from '../phases/hiddenArmies/place/PlaceHiddenArmies'
import { PlaceHiddenArmiesState } from '../phases/hiddenArmies/place/PlaceHiddenArmiesState'
import { RevealHiddenArmies } from '../phases/hiddenArmies/reveal/RevealHiddenArmies'
import { RevealHiddenArmiesState } from '../phases/hiddenArmies/reveal/RevealHiddenArmiesState'
import { PlaceCastles } from '../phases/placeCastles/PlaceCastles'
import { PlaceCastlesState } from '../phases/placeCastles/PlaceCastlesState'
import { PlaceSpoils } from '../phases/placeSpoils/PlaceSpoils'
import { PlaceSpoilsState } from '../phases/placeSpoils/PlaceSpoilsState'
import { PlayerDefeated } from '../phases/playerDefeated/PlayerDefeated'
import { PlayerDefeatedState } from '../phases/playerDefeated/PlayerDefeatedState'

export const viewForState = 
  (state:CastleRiskState) =>
  (setState:Consumer<CastleRiskState>) =>
  isType(AdminState)(state) ? <AdminBorders {...{setState, ...state}} />
    : isType(AddPlayersState)(state) ? <AddPlayers {...{setState, ...state}} />
    : isType(PlaceCastlesState)(state) ? <PlaceCastles {...{setState, ...state}} />
    : isType(ClaimTerritoriesState)(state) ? <ClaimTerritories {...{setState, ...state}} />
    : isType(PlaceHiddenArmiesState)(state) ? <PlaceHiddenArmies {...{setState, ...state}} />
    : isType(RevealHiddenArmiesState)(state) ? <RevealHiddenArmies {...{setState, ...state}} />
    : isType(FortifyTerritoriesState)(state) ? <FortifyTerritories {...{setState, ...state}} />
    : isType(ChooseActionState)(state) ? <ChooseAction {...{setState, ...state}} />
    : isType(PlaceSpoilsState)(state) ? <PlaceSpoils {...{setState, ...state}} />
    : isType(ChooseAttackOriginState)(state) ? <ChooseAttackOrigin {...{setState, ...state}} />
    : isType(ChooseAttackTargetState)(state) ? <ChooseAttackTarget {...{setState, ...state}} />
    : isType(ChooseDiceCountState)(state) ? <ChooseDiceCount {...{setState, ...state}} />
    : isType(BattleUndecidedState)(state) ? <ChooseDiceCount {...{setState, ...state}} />
    : isType(AttackerWonState)(state) ? <AttackerWon {...{setState, ...state}} />
    : isType(DefenderWonState)(state) ? <DefenderWon {...{setState, ...state}} />
    : isType(PlayerDefeatedState)(state) ? <PlayerDefeated {...{setState, ...state}} />
    : isType(FillDefeatedTerritoriesState)(state) ? <FillDefeatedTerritories {...{setState, ...state}} />
    : isType(RollingState)(state) ? <Rolling {...{setState, ...state}} />
    : isType(PlaceReinforcementsState)(state) ? <PlaceReinforcements {...{setState, ...state}} />
    : isType(PlayDiplomatState)(state) ? <PlayDiplomat {...{setState, ...state}} />
    : isType(BoardAdmiralShipState)(state) ? <BoardAdmiralShip {...{setState, ...state}} />
    : <p>Invalid State</p>
  