import { AttackerWonState } from "castleRisk/phases/attack/attackerWon/AttackerWonState";
import { BattleUndecidedState } from "castleRisk/phases/attack/battleUndecided/BattleUndecidedState";
import { BoardAdmiralShipState } from "castleRisk/phases/attack/boardAdmiralShip/BoardAdmiralShipState";
import { ChooseAttackOriginState } from "castleRisk/phases/attack/chooseAttackOrigin/ChooseAttackOriginState";
import { ChooseAttackTargetState } from "castleRisk/phases/attack/chooseAttackTarget/ChooseAttackTargetState";
import { ChooseDiceCountState } from "castleRisk/phases/attack/chooseDiceCount/ChooseDiceCountState";
import { DefenderWonState } from "castleRisk/phases/attack/defenderWon/DefenderWonState";
import { RollingState } from "castleRisk/phases/attack/rolling/RollingState";
import { ChooseActionState } from "castleRisk/phases/chooseAction/ChooseActionState";
import { FillDefeatedTerritoriesState } from "castleRisk/phases/fillDefeatedTerritories/FillDefeatedTerritoriesState";
import { RevealHiddenArmiesState } from "castleRisk/phases/hiddenArmies/reveal/RevealHiddenArmiesState";
import { PlaceCastlesState } from "castleRisk/phases/placeCastles/PlaceCastlesState";
import { PlaceReinforcementsState } from "castleRisk/phases/placeReinforcments/PlaceReinforcementsState";
import { PlaceSpoilsState } from "castleRisk/phases/placeSpoils/PlaceSpoilsState";
import { PlayDiplomatState } from "castleRisk/phases/playDiplomat/PlayDiplomatState";
import { PlayerDefeatedState } from "castleRisk/phases/playerDefeated/PlayerDefeatedState";

//TODO: create a DU of the various types that inherit from CastleRiskPlayState, rename CastleRiskPlayState to CastleRiskBasePlayState
export type CardStates =
  | ChooseDiceCountState
  | BattleUndecidedState
  | RollingState
  | ChooseActionState
  | ChooseAttackOriginState
  | ChooseAttackTargetState
  | PlayerDefeatedState
  | AttackerWonState
  | DefenderWonState
  | FillDefeatedTerritoriesState
  | RevealHiddenArmiesState
  | PlaceSpoilsState
  | PlaceCastlesState
  | PlaceReinforcementsState
  | BoardAdmiralShipState
  | PlayDiplomatState;
