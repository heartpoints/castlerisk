import { new_ } from "@utils/zod/new_";
import { Marshall } from "./Marshall";

export const marshallCard = new_(Marshall)({ inBattle: false });
