import { z } from "zod";

import { HasURI } from "@utils/zod/HasURI";

export const Marshall = HasURI("Marshall")
export type Marshall = z.infer<typeof Marshall>;
