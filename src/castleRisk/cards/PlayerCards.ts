import { z } from "zod";
import { PlayerCard } from "./PlayerCard";

export const PlayerCards = z.array(PlayerCard).readonly()
export type PlayerCards = z.infer<typeof PlayerCards>
