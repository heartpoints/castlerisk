import { z } from "zod";

export const HasReinforcementsCount = z.object({
  reinforcementsCount: z.number(),
});
export type HasReinforcementsCount = z.infer<typeof HasReinforcementsCount>;
