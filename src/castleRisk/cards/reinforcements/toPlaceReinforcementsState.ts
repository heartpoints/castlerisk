import { isType } from "@utils/zod/isType";
import { new_ } from "@utils/zod/new_";
import { CastleRiskPlayState } from "castleRisk/phases/common/CastleRiskPlayState";
import { PlaceReinforcementsState } from "castleRisk/phases/placeReinforcments/PlaceReinforcementsState";
import { moveUpToNPiecesFromReservesToHand } from "castleRisk/pieces/moveUpToNPiecesFromReservesToHand";
import { currentPlayerFromState } from "castleRisk/players/currentPlayerFromState";
import { produce } from "immer";
import { Reinforcements } from "./Reinforcements";

export const toPlaceReinforcementsState = (
    state: CastleRiskPlayState,
    reinforcementsCardIndex: number
  ) => {
    const stateWithPiecesInHandAndCardActivated = produce(state, (draft) => {
      const currentPlayer = currentPlayerFromState(draft);
      currentPlayer.cards = currentPlayer.cards.map((c, i) =>
        isType(Reinforcements)(c)
          ? i == reinforcementsCardIndex
            ? { ...c, inPlacement: true }
            : c
          : c
      );
      currentPlayer.pieces = moveUpToNPiecesFromReservesToHand(
        state.reinforcementsCount
      )(currentPlayer.pieces);
    });
    return new_(PlaceReinforcementsState)(stateWithPiecesInHandAndCardActivated);
  };