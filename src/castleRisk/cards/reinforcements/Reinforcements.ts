
import { HasURI } from "@utils/zod/HasURI";
import { z } from "zod";

export const Reinforcements = z.object({
    ...HasURI("Reinforcements").shape,
    inPlacement: z.boolean(),
});
export type Reinforcements = z.infer<typeof Reinforcements>;
