import { isType } from "@utils/zod/isType";
import { ChooseActionState } from "castleRisk/phases/chooseAction/ChooseActionState";
import { CardStates } from "../CardStates";

export const isReinforcementsClickable = (state: CardStates) =>
  !state.hasAttacked && isType(ChooseActionState)(state);
