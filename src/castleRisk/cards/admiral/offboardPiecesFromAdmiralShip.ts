import { isType } from "@utils/zod/isType";
import { new_ } from "@utils/zod/new_";
import { AttackerWonState } from "castleRisk/phases/attack/attackerWon/AttackerWonState";
import { PlacedInTerritory } from "castleRisk/pieces/PlacedInTerritory";
import { PlacedOnBoat } from "castleRisk/pieces/PlacedOnBoat";
import { currentPlayerFromState } from "castleRisk/players/currentPlayerFromState";
import { produce } from "immer";

export const offboardPiecesFromAdmiralShip = (state: AttackerWonState) =>
    produce(state, (draft) => {
      currentPlayerFromState(draft).pieces = currentPlayerFromState(
        draft
      ).pieces.map((p) =>
        isType(PlacedOnBoat)(p)
          ? new_(PlacedInTerritory)({
              ...p,
              territoryName: state.attackTargetTerritory.name,
            })
          : p
      );
    });