export const AdmiralAvatar = () => (
  <img
    src="/images/admiral-avatar.png"
    width="80"
    height="80"
    style={{ width: "1.5em", height: "auto" }} />
);
