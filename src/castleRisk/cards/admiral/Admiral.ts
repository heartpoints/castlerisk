import { HasURI } from "@utils/zod/HasURI";
import { z } from "zod";
import { HasIsActive } from "../HasIsActive";

export const Admiral = z.object({
    ...HasURI("Admiral").shape,
    ...HasIsActive.shape,
});
export type Admiral = z.infer<typeof Admiral>;

//TODO: ChooseDiceCount, various other things that depend on the number of armies attacking must use the boat count if active instead.
    // - ?
//TODO: If attacker wins, admiral is placed back into hand and all remaining armies MUST go to defeated territory
//TODO: If defender wins, active admirals are discarded

//NICE TO HAVES:
//TODO: A general played along with an admiral may not be deactivated
//TODO: A general may only be played along with an admiral BEFORE the first attack
//TODO: If admiral is defeated, any generals are also discarded