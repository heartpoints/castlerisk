import { Territory } from "castleRisk/territories/Territory";
import { bodiesOfWater } from "castleRisk/water/bodiesOfWater";

export const bodiesOfWaterBorderingTerritory = (territory: Territory) => bodiesOfWater.filter((b) => b.territoryNames.includes(territory.name));
