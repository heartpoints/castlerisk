import { isOpponentOccupiedWithNoTruce } from "castleRisk/phases/common/isOpponentOccupiedWithNoTruce";
import { Player } from "castleRisk/players/Player";
import { Players } from "castleRisk/players/Players";
import { Territory } from "castleRisk/territories/Territory";
import { territoriesReachableByWater } from "./territoriesReachableByWater";

export const territoriesAttackableByWater = (
  attackingPlayer: Player,
  fromTerritory: Territory,
  players: Players
) =>
  territoriesReachableByWater(fromTerritory).filter(
    isOpponentOccupiedWithNoTruce(attackingPlayer, players)
  );
