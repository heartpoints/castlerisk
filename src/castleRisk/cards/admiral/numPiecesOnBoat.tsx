import { isType } from "@utils/zod/isType";
import { PlacedOnBoat } from "castleRisk/pieces/PlacedOnBoat";
import { Players } from "castleRisk/players/Players";

export const numPiecesOnBoat = (players: Players) =>
  players.flatMap((p) => p.pieces).filter(isType(PlacedOnBoat)).length;
