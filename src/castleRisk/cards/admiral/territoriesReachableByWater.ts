import { uniqueStrings } from "@utils/hashing/uniqueStrings";
import { Territory } from "castleRisk/territories/Territory";
import { territoryByName } from "castleRisk/territories/territoryByName";
import { bodiesOfWaterBorderingTerritory } from "./bodiesOfWaterBorderingTerritory";

export const territoriesReachableByWater = (fromTerritory: Territory) =>
  uniqueStrings(
    bodiesOfWaterBorderingTerritory(fromTerritory).flatMap(
      (b) => b.territoryNames
    )
  ).map(territoryByName);

export const isTerritoryWaterBordering = (territory:Territory) => bodiesOfWaterBorderingTerritory(territory).length > 0
