import { isType } from "@utils/zod/isType";
import { CastleRiskPlayState } from "castleRisk/phases/common/CastleRiskPlayState";
import { currentPlayerFromState } from "castleRisk/players/currentPlayerFromState";
import { produce } from "immer";
import { Admiral } from "./Admiral";
import { admiralCard } from "./admiralCard";

export const deactivateAdmiral = <T extends CastleRiskPlayState>(state: T) =>
  produce(state, (draft) => {
    const currentPlayer = currentPlayerFromState(draft);
    currentPlayer.cards = currentPlayer.cards.map((c) =>
      isType(Admiral)(c) ? admiralCard : c
    );
  });
