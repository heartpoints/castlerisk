import { isType } from "@utils/zod/isType";
import { currentPlayerFromState } from "castleRisk/players/currentPlayerFromState";
import { produce } from "immer";
import { CardStates } from "../CardStates";
import { Admiral } from "./Admiral";

export const onAdmiralClicked = (state: CardStates, cardIndex: number) => {
  return produce(state, (draft) => {
    const currentCard = currentPlayerFromState(draft).cards[cardIndex];
    if (isType(Admiral)(currentCard))
      currentCard.isActive = !currentCard.isActive;
  });
};
