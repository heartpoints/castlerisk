import { isType } from "@utils/zod/isType";
import { HasPlayers } from "castleRisk/phases/common/HasPlayers";
import { Admiral } from "./Admiral";

export const isAdmiralActive = (state: HasPlayers) =>
  state.players
    .flatMap((p) => p.cards)
    .filter(isType(Admiral))
    .some((c) => c.isActive);
