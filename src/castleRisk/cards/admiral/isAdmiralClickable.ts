import { isType } from "@utils/zod/isType";
import { ChooseAttackTargetState } from "castleRisk/phases/attack/chooseAttackTarget/ChooseAttackTargetState";
import { CastleRiskPlayState } from "castleRisk/phases/common/CastleRiskPlayState";
import { currentPlayerFromState } from "castleRisk/players/currentPlayerFromState";
import { Admiral } from "./Admiral";
import { atLeastOneCurrentPlayerTerritoryCanAttackAnotherByWater } from "./atLeastOneCurrentPlayerTerritoryCanAttackAnotherByWater";
import { isTerritoryWaterBordering } from "./territoriesReachableByWater";

export const isAdmiralClickable = (state: CastleRiskPlayState, card: Admiral) =>
  isType(ChooseAttackTargetState)(state) &&
  isTerritoryWaterBordering(state.attackOriginTerritory) &&
  (card.isActive ||
    (currentPlayerFromState(state)
      .cards.filter(isType(Admiral))
      .every((c) => !c.isActive) &&
      atLeastOneCurrentPlayerTerritoryCanAttackAnotherByWater(state)));
