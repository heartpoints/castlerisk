import { ChooseAttackOriginState } from "castleRisk/phases/attack/chooseAttackOrigin/ChooseAttackOriginState";
import { ChooseAttackTargetState } from "castleRisk/phases/attack/chooseAttackTarget/ChooseAttackTargetState";
import { piecesInTerritory } from "castleRisk/pieces/piecesInTerritory";
import { currentPlayerFromState } from "castleRisk/players/currentPlayerFromState";
import { territoriesForPlayer } from "castleRisk/territories/territoriesForPlayer";
import { territoriesAttackableByWater } from "./territoriesAttackableByWater";

export const atLeastOneCurrentPlayerTerritoryCanAttackAnotherByWater = (
  state: ChooseAttackTargetState | ChooseAttackOriginState
) =>
  territoriesForPlayer(currentPlayerFromState(state)).some(
    (t) =>
      piecesInTerritory(state.players)(t).length > 1 &&
      territoriesAttackableByWater(
        currentPlayerFromState(state),
        t,
        state.players
      ).length > 1
  );
