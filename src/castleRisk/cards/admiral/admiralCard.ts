import { new_ } from "@utils/zod/new_";
import { Admiral } from "./Admiral";

export const admiralCard = new_(Admiral)({ isActive: false });
