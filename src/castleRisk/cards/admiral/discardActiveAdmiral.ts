import { not } from "@utils/predicates/not";
import { isType } from "@utils/zod/isType";
import { CastleRiskPlayState } from "castleRisk/phases/common/CastleRiskPlayState";
import { currentPlayerFromState } from "castleRisk/players/currentPlayerFromState";
import { produce } from "immer";
import { Admiral } from "./Admiral";
import { admiralCard } from "./admiralCard";

export const discardActiveAdmiral = <T extends CastleRiskPlayState>(state: T) => {
    return produce(state, (draft) => {
      currentPlayerFromState(draft).cards = currentPlayerFromState(
        draft
      ).cards.filter(not((c) => isType(Admiral)(c) && c.isActive));
      draft.discardPile.push(admiralCard);
    });
  };