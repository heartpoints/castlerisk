import { isAdmiralActive } from "castleRisk/cards/admiral/isAdmiralActive";
import { adjacentOrDottedLineTerritories } from "castleRisk/territories/adjacentOrDottedLineTerritories";
import { territoryEquals } from "castleRisk/territories/territoryEquals";
import { ChooseDiceCountState } from "../../phases/attack/chooseDiceCount/ChooseDiceCountState";

export const shouldDeactivateAdmiral = (state: ChooseDiceCountState) =>
  isAdmiralActive(state) &&
  adjacentOrDottedLineTerritories(state.attackOriginTerritory).some(
    territoryEquals(state.attackTargetTerritory)
  );
