import { numPiecesOnBoat } from "castleRisk/cards/admiral/numPiecesOnBoat";
import { Players } from "castleRisk/players/Players";
import { Territory } from "castleRisk/territories/Territory";
import { maxMovablePiecesForLandBattle } from "../../phases/attack/general/maxMovablePiecesForLandBattle";

export const maxBoardablePieces = (
  players: Players,
  attackOriginTerritory: Territory
) =>
  numPiecesOnBoat(players) +
  maxMovablePiecesForLandBattle(players, attackOriginTerritory);
