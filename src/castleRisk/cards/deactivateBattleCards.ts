import { CastleRiskPlayState } from "castleRisk/phases/common/CastleRiskPlayState";
import { pipe } from "fp-ts/lib/function";
import { deactivateAdmiral } from "./admiral/deactivateAdmiral";
import { deactivateGenerals } from "./general/deactivateGenerals";

export const deactivateBattleCards = <T extends CastleRiskPlayState>(
  state: T
) => pipe(state, deactivateAdmiral, deactivateGenerals);
