import { PlayerCards } from "./PlayerCards";

import { z } from "zod";

export const HasPlayerCards = z.object({ cards: PlayerCards });
export type HasPlayerCards = z.infer<typeof HasPlayerCards>;
