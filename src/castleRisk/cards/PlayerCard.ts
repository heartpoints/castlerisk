import { Admiral } from "./admiral/Admiral";
import { Diplomat } from "./diplomat/Diplomat";
import { General } from "./general/General";
import { Marshall } from "./marshall/Marshall";
import { Reinforcements } from "./reinforcements/Reinforcements";
import { Spy } from "./spy/Spy";

import { z } from "zod";

export const PlayerCard = z.discriminatedUnion("URI", [Spy, Marshall, Admiral, Reinforcements, Diplomat, General])

export type PlayerCard = z.infer<typeof PlayerCard>;
