import { isType } from "@utils/zod/isType";
import { Diplomat } from "castleRisk/cards/diplomat/Diplomat";
import { HasCurrentPlayerIndex } from "../../phases/common/HasCurrentPlayerIndex";
import { HasPlayers } from "../../phases/common/HasPlayers";

export const activeDiplomats = (state: HasPlayers & HasCurrentPlayerIndex) => state.players
  .flatMap((p) => p.cards.filter(isType(Diplomat)))
  .filter((d) => d.activeAgainstPlayerId !== undefined);
