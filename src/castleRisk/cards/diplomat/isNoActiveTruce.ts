import { Player } from "castleRisk/players/Player";
import { isNoActiveTruceInOneDirection } from "./isNoActiveTruceInOneDirection";

export const isNoActiveTruce = (currentPlayer: Player, targetPlayer: Player) =>
  isNoActiveTruceInOneDirection(currentPlayer, targetPlayer) &&
  isNoActiveTruceInOneDirection(targetPlayer, currentPlayer);
