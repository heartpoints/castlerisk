import { isType } from "@utils/zod/isType";
import { ChooseActionState } from "castleRisk/phases/chooseAction/ChooseActionState";
import { CastleRiskPlayState } from "castleRisk/phases/common/CastleRiskPlayState";
import { Diplomat } from "./Diplomat";
import { diplomatTargetPlayers } from "./diplomatTargetPlayers";

export const isDiplomatClickable = (
  state: CastleRiskPlayState,
  card: Diplomat
) =>
  !card.isActive &&
  !state.hasAttacked &&
  isType(ChooseActionState)(state) &&
  diplomatTargetPlayers(state).length > 0;
