import { Tooltip } from "@mui/material";
import { HasCurrentPlayerIndex } from "castleRisk/phases/common/HasCurrentPlayerIndex";
import { HasPlayers } from "castleRisk/phases/common/HasPlayers";
import { FC } from "react";
import { playerById } from "../../players/playerById";
import { cardDescription } from "../cardDescriptions";
import { Diplomat } from "../diplomat/Diplomat";

export const DiplomatAvatar: FC<
  { diplomat: Diplomat } & HasPlayers & HasCurrentPlayerIndex
> = ({ diplomat, ...state }) => {
  const tooltipTitle =
    diplomat.issuingPlayer && diplomat.activeAgainstPlayerId
      ? `Diplomatic Truce issued by ${
          playerById(diplomat.issuingPlayer, state).name
        } against ${
          playerById(diplomat.activeAgainstPlayerId, state).name
        }. Active until ${
          playerById(diplomat.issuingPlayer, state).name
        }'s next turn. Neither player may attack each other while active.`
      : `${diplomat.URI}: ${cardDescription(diplomat)}`;
  return (
    <Tooltip title={tooltipTitle}>
      <img
        src="images/diplomat-avatar.png"
        width="80"
        height="80"
        style={{ width: "2em", height: "auto" }}
      />
    </Tooltip>
  );
};
