import { HasURI } from "@utils/zod/HasURI";
import { ID } from "../../players/ID";
import { z } from "zod";

export const Diplomat = z.object({
    ...HasURI("Diplomat").shape,
    isActive: z.boolean(),
    activeAgainstPlayerId: ID.optional(),
    issuingPlayer: ID.optional(),
});
export type Diplomat = z.infer<typeof Diplomat>;
