import { not } from "@utils/predicates/not";
import { isType } from "@utils/zod/isType";
import { CastleRiskPlayState } from "castleRisk/phases/common/CastleRiskPlayState";
import { currentPlayerFromState } from "castleRisk/players/currentPlayerFromState";
import { playerEquals } from "castleRisk/players/playerEquals";
import { Diplomat } from "./Diplomat";

export const diplomatTargetPlayers = (state: CastleRiskPlayState) => {
    const currentPlayer = currentPlayerFromState(state);
    return state.players
        .filter((p) => currentPlayer.cards
            .filter(isType(Diplomat))
            .every((c) => c.activeAgainstPlayerId !== p.id)
        )
        .filter(not(playerEquals(currentPlayer)));
};
