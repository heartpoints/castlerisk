import { isType } from "@utils/zod/isType";
import { Player } from "castleRisk/players/Player";
import { Diplomat } from "./Diplomat";

export const isNoActiveTruceInOneDirection = (p1: Player, p2: Player) =>
  p1.cards
    .filter(isType(Diplomat))
    .every((d) => d.activeAgainstPlayerId != p2.id);
