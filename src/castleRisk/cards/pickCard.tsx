import { randomIntegerBetweenZeroAnd } from "@utils/list/randomIntegerBetweenZeroAnd";
import { CastleRiskPlayState } from "castleRisk/phases/common/CastleRiskPlayState";
import { Player } from "castleRisk/players/Player";
import { playerEquals } from "castleRisk/players/playerEquals";
import { produce } from "immer";

export const pickCard = (state: CastleRiskPlayState, playerToPick: Player) => {
  const indexOfPickedCardInDeck = randomIntegerBetweenZeroAnd(
    state.cards.length
  );

  const pickedCard = state.cards[indexOfPickedCardInDeck];
  if (!pickedCard) throw new Error(`could not draw card`);
  return produce(state, draft => {
    draft.cards.splice(indexOfPickedCardInDeck, 1)
    draft.players.find(playerEquals(playerToPick))?.cards.push(pickedCard)
  })
};
