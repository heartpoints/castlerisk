import { max } from '../../utils/math/max';
import { pickCardsForPlayer } from './pickCardsForPlayer';
import { CastleRiskPlayState } from '../phases/common/CastleRiskPlayState';
import { currentPlayerFromState } from '../players/currentPlayerFromState';

export const pickCardsAtStartOfTurn = <T extends CastleRiskPlayState>(state: T) => {
    const currentPlayer = currentPlayerFromState(state);
    const numCardsToPick = max(3 - currentPlayer.cards.length, 0) + 1;
    return pickCardsForPlayer(state, currentPlayer, numCardsToPick);
};
