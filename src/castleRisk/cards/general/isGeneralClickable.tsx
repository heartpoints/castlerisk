import { isType } from "@utils/zod/isType";
import { BattleUndecidedState } from "castleRisk/phases/attack/battleUndecided/BattleUndecidedState";
import { ChooseDiceCountState } from "castleRisk/phases/attack/chooseDiceCount/ChooseDiceCountState";
import { CardStates } from "../CardStates";

export const isGeneralClickable = (state: CardStates) =>
  isType(ChooseDiceCountState)(state) || isType(BattleUndecidedState)(state);
