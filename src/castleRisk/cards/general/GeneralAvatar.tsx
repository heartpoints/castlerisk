import { Tooltip } from "@mui/material";
import { cardDescription } from "castleRisk/cards/cardDescriptions";
import { generalCard } from "./generalCard";

export const GeneralAvatar = () => (
  <Tooltip title={`${generalCard.URI}: ${cardDescription(generalCard)}`}>
    <img
      src="images/general-avatar.png"
      width="80"
      height="80"
      style={{ width: "2em", height: "auto" }}
    />
  </Tooltip>
);
