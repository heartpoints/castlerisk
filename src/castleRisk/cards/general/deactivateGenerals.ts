import { isType } from "@utils/zod/isType";
import { CastleRiskPlayState } from "castleRisk/phases/common/CastleRiskPlayState";
import { currentPlayerFromState } from "castleRisk/players/currentPlayerFromState";
import { produce } from "immer";
import { General } from "./General";
import { generalCard } from "./generalCard";

export const deactivateGenerals = <T extends CastleRiskPlayState>(state: T) =>
  produce(state, (draft) => {
    const currentPlayer = currentPlayerFromState(draft);
    currentPlayer.cards = currentPlayer.cards.map((c) =>
      isType(General)(c) ? generalCard : c
    );
  });
