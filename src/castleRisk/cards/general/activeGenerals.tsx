import { CastleRiskPlayState } from "castleRisk/phases/common/CastleRiskPlayState";
import { currentPlayerFromState } from "castleRisk/players/currentPlayerFromState";
import { isActiveGeneral } from "./isActiveGeneral";

export const activeGenerals = (state: CastleRiskPlayState) =>
  currentPlayerFromState(state)
    .cards.filter(isActiveGeneral);
