import { CastleRiskState } from "castleRisk/phases/common/CastleRiskState";
import { currentPlayerFromState } from "castleRisk/players/currentPlayerFromState";
import { playerEquals } from "castleRisk/players/playerEquals";
import { CardStates } from "../CardStates";
import { General } from "./General";
import { isType } from "@utils/zod/isType";
import { produce } from "immer";
import { SetState } from "@utils/react/SetState";

export const toggleGeneralInBattle = (
  state: CardStates,
  setState: SetState<CastleRiskState>,
  i: number
) => {
  const currentPlayer = currentPlayerFromState(state);
  const newState = produce(state, (draft) => {
    const possibleGeneralCard = draft.players.find(playerEquals(currentPlayer))
      ?.cards[i];
    if (isType(General)(possibleGeneralCard)) {
      possibleGeneralCard.inBattle = !possibleGeneralCard.inBattle;
    }
  });
  setState(newState);
};
