import { new_ } from "@utils/zod/new_";
import { General } from "./General";

export const generalCard = new_(General)({ inBattle: false });
