import { isType } from "@utils/zod/isType";
import { General } from "./General";
import { PlayerCard } from "../PlayerCard";

export const isActiveGeneral = (card: PlayerCard): card is General => isType(General)(card) && card.inBattle;
