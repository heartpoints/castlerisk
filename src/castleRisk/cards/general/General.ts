import { z } from "zod";

import { HasURI } from "@utils/zod/HasURI";
import { HasInBattle } from "../HasInBattle";

export const General = z.object({
  ...HasURI("General").shape,
  ...HasInBattle.shape,
});
export type General = z.infer<typeof General>;
