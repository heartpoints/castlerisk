import { Tooltip } from "@mui/material";
import { FC } from "react";
import { doNothing } from "../../utils/axioms/doNothing";
import { VoidCallback } from "../../utils/axioms/VoidCallback";
import { cardDescription } from "./cardDescriptions";
import { PlayerCard } from "./PlayerCard";
import { boxShadow } from "castleRisk/config/boxShadow";
import { HasIsActive } from "./HasIsActive";

export const Card: FC<HasIsActive &{
  card: PlayerCard;
  disabled: boolean;
  onClick?: VoidCallback;
}> = ({ card, disabled, onClick = doNothing, isActive }) => {
  return (
    <Tooltip title={cardDescription(card)}>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          transition: "transform 0.3s ease",
          cursor: "pointer",
        }}
        className={disabled ? "" : "dock-item"}
      >
        <div
          style={{
            width: "125px",
            height: "200px",
            border: "1px",
            backgroundColor: "white",
            borderRadius: "5px",
            backgroundImage: `url("images/${card.URI.toLowerCase()}.png")`,
            position: "relative",
            ...(isActive ? { top: -20 } : {}),
            boxShadow,
            cursor: disabled ? "not-allowed" : "pointer",
            opacity: disabled ? 0.5 : 1,
          }}
          className="dock-image"
          onClick={disabled ? doNothing : onClick}
        >
          <p
            style={{
              position: "absolute",
              top: 155,
              width: "100%",
              textAlign: "center",
              fontSize: "13px",
              textTransform: "capitalize",
            }}
          >
            {card.URI}
          </p>
        </div>
      </div>
    </Tooltip>
  );
};
