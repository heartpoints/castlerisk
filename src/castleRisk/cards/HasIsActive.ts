
import { z } from "zod";

export const HasIsActive = z.object({ isActive: z.boolean() });
export type HasIsActive = z.infer<typeof HasIsActive>;
