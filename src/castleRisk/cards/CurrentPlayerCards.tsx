import { Tooltip } from "@mui/material";
import { isType } from "@utils/zod/isType";
import { boxShadow } from "castleRisk/config/boxShadow";
import { toPlayDiplomatState } from "castleRisk/phases/playDiplomat/toPlayDiplomatState";
import { FC } from "react";
import { currentPlayerFromState } from "../players/currentPlayerFromState";
import { Card } from "./Card";
import { CardContainer } from "./CardContainer";
import { CurrentPlayerCardsProps } from "./CurrentPlayerCardsProps";
import { Admiral } from "./admiral/Admiral";
import { isAdmiralClickable } from "./admiral/isAdmiralClickable";
import { onAdmiralClicked } from "./admiral/onAdmiralClicked";
import { Diplomat } from "./diplomat/Diplomat";
import { isDiplomatClickable } from "./diplomat/isDiplomatClickable";
import { General } from "./general/General";
import { isGeneralClickable } from "./general/isGeneralClickable";
import { toggleGeneralInBattle } from "./general/toggleGeneralInBattle";
import { Reinforcements } from "./reinforcements/Reinforcements";
import { isReinforcementsClickable } from "./reinforcements/isReinforcementsClickable";
import { toPlaceReinforcementsState } from "./reinforcements/toPlaceReinforcementsState";

export const CurrentPlayerCards: FC<CurrentPlayerCardsProps> = ({
  setState,
  ...state
}) => {
  const currentPlayer = currentPlayerFromState(state);
  return (
    <>
      <CardContainer>
        {currentPlayer.cards.map((card, i) =>
          isType(General)(card) ? (
            <Card
              card={card}
              isActive={card.inBattle}
              key={i}
              disabled={!isGeneralClickable(state)}
              onClick={() => toggleGeneralInBattle(state, setState, i)}
            />
          ) : isType(Reinforcements)(card) ? (
            <Card
              card={card}
              isActive={card.inPlacement}
              key={i}
              disabled={!isReinforcementsClickable(state)}
              onClick={() => setState(toPlaceReinforcementsState(state, i))}
            />
          ) : isType(Diplomat)(card) ? (
            <Card
              card={card}
              isActive={card.isActive}
              key={i}
              disabled={!isDiplomatClickable(state, card)}
              onClick={() => setState(toPlayDiplomatState(state, i))}
            />
          ) : isType(Admiral)(card) ? (
            <Card
              card={card}
              isActive={card.isActive}
              key={i}
              disabled={!isAdmiralClickable(state, card)}
              onClick={() => setState(onAdmiralClicked(state, i))}
            />
          ) : (
            <Card {...{ card }} key={i} disabled={true} isActive={false} />
          )
        )}
      </CardContainer>
      <div
        style={{
          position: "absolute",
          bottom: "2em",
          left: "2em",
          backgroundColor: "rgba(255,255,255,0.3)",
          borderRadius: "3em",
          width: "3em",
          height: "3em",
          justifyContent: "center",
          alignItems: "center",
          display: "flex",
          padding: 0,
          margin: 0,
          borderWidth: "3px",
          borderColor: "black",
          borderStyle: "solid",
          boxShadow,
        }}
      >
        <Tooltip title="The next reinformcents card played will receive this quantity of armies">
          <p
            style={{
              padding: 0,
              margin: 0,
              lineHeight: "100%",
              position: "relative",
              top: "-1px",
              cursor: "default",
            }}
          >
            {state.reinforcementsCount}
          </p>
        </Tooltip>
      </div>
    </>
  );
};
