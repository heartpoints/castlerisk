import { FC, PropsWithChildren } from "react";


export const CardContainer:FC<PropsWithChildren> = ({children}) => {
  return (
    <div
      style={{
        position: "absolute",
        bottom: "20px",
        display: "flex",
        gap: "10px",
        flexDirection: "row",
        justifyContent: "center",
        width: "100%",
      }}
    >
      {children}
    </div>
  );
}
