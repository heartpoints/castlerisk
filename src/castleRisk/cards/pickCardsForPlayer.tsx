import { CastleRiskPlayState } from "../phases/common/CastleRiskPlayState";
import { Player } from "../players/Player";
import { pickCard } from "./pickCard";

export const pickCardsForPlayer = <T extends CastleRiskPlayState>(
  state: T,
  player: Player,
  numCardsToPic: number
) => {
  return numCardsToPic === 0
    ? state
    : pickCardsForPlayer(
      pickCard(state, player),
      player,
      numCardsToPic - 1
    );
};
