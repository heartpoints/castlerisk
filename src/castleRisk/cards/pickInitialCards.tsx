import { CastleRiskPlayState } from "../phases/common/CastleRiskPlayState";
import { Players } from "../players/Players";
import { pickCardsForPlayer } from "./pickCardsForPlayer";

export const pickInitialCards = <T extends CastleRiskPlayState>(state: T) => {
  const pickInitialCardsR = (state: T, playersWithoutCards: Players): T => {
    const [currentPlayer, ...remainingPlayers] = playersWithoutCards;
    return playersWithoutCards.length === 0
      ? state
      : pickInitialCardsR(
          pickCardsForPlayer(state, currentPlayer, 3),
          remainingPlayers
        );
  };
  return pickInitialCardsR(state, state.players);
};
