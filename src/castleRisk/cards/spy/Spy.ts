
import { HasURI } from "@utils/zod/HasURI";
import { z } from "zod";

export const Spy = HasURI("Spy");
export type Spy = z.infer<typeof Spy>;
