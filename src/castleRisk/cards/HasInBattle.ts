import { z } from "zod";


export const HasInBattle = z.object({ inBattle: z.boolean() });
export type HasInBattle = z.infer<typeof HasInBattle>;
