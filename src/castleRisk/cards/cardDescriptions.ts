import { isType } from "@utils/zod/isType";
import { throwError } from "../../utils/debugging/throwError";
import { Admiral } from "./admiral/Admiral";
import { Diplomat } from "./diplomat/Diplomat";
import { Marshall } from "./marshall/Marshall";
import { PlayerCard } from "./PlayerCard";
import { Reinforcements } from "./reinforcements/Reinforcements";
import { Spy } from "./spy/Spy";
import { General } from "./general/General";

export const cardDescription = (card: PlayerCard) =>
  isType(Diplomat)(card)
    ? "Make Truce. Play at start of turn."
    : isType(Spy)(card)
    ? "Spy on opponent or defend against Spy."
    : isType(General)(card)
    ? "Offense. Add 1 to high die. If high die loses, General is discarded."
    : isType(Marshall)(card)
    ? "Defense. Add 1 to high die. If high die loses, Marshall is discarded."
    : isType(Admiral)(card)
    ? "Offense. Attack by sea."
    : isType(Reinforcements)(card)
    ? "Add armies. Play at start of turn."
    : throwError(new Error(`Cannot get description for card ${card}`));
