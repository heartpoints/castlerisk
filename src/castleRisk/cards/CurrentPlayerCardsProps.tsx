import { PropsWithSetState } from "@utils/react/PropsWithSetState";
import { CardStates } from "./CardStates";

export type CurrentPlayerCardsProps = PropsWithSetState<CardStates>;
