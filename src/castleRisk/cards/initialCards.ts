import { new_ } from "@utils/zod/new_";
import { arrayOfLength } from "../../utils/arrays/arrayOfLength";
import { Diplomat } from "./diplomat/Diplomat";
import { PlayerCards } from "./PlayerCards";
import { Reinforcements } from "./reinforcements/Reinforcements";
import { Spy } from "./spy/Spy";
import { generalCard } from "./general/generalCard";
import { admiralCard } from "./admiral/admiralCard";
import { marshallCard } from "./marshall/marshallCard";

export const initialCards: PlayerCards = [
  ...arrayOfLength(7).map(_ => new_(Diplomat)({isActive: false})),
  ...arrayOfLength(7).map(_ => new_(Spy)({})),
  ...arrayOfLength(8).map(_ => generalCard),
  ...arrayOfLength(8).map(_ => marshallCard),
  ...arrayOfLength(6).map(_ => admiralCard),
  ...arrayOfLength(12).map(_ => new_(Reinforcements)({inPlacement: false})),
];
