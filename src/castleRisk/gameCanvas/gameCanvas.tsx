import { MouseEvent, useEffect, useRef } from "react";
import { canvasHeight } from "../config/canvasHeight";
import { canvasWidth } from "../config/canvasWidth";
import { CanvasMouseEventHandlers } from "./CanvasMouseEventHandlers";
import { context2dForRef } from "../../utils/canvas/context2dForRef";
import { Consumer } from "../../utils/axioms/Consumer";
import { Point } from "../../utils/geometry/Point";

type GameCanvasProps = CanvasMouseEventHandlers & {
  drawGame: Consumer<CanvasRenderingContext2D>;
};
export const GameCanvas = ({
  onMouseUp,
  onMouseMove,
  drawGame,
}: GameCanvasProps) => {
  const canvasRef = useRef(null);
  const canvasSizeRef = useRef<any>(null);
  useEffect(() => {
    const resizeAndDrawCanvas = () => {
      const canvas = canvasRef.current as any;
      const context = context2dForRef(canvasRef);
      const parent = canvas.parentElement;
      const { width, top, left } = parent.getBoundingClientRect();
      const height = (canvasHeight / canvasWidth) * width;
      const scaleX = width / canvasWidth;
      const scaleY = height / canvasHeight;

      canvas.width = width;
      canvas.height = height;
      context.setTransform(1, 0, 0, 1, 0, 0);
      context.clearRect(0, 0, canvas.width, canvas.height);
      context.fillStyle = "blue";
      context.fillRect(0, 0, canvas.width, canvas.height);
      context.scale(scaleX, scaleY);
      canvasSizeRef.current = { scaleX, scaleY, top, left };
      drawGame(context);
    };
    resizeAndDrawCanvas();
    window.addEventListener("resize", resizeAndDrawCanvas);
    return () => {
      window.removeEventListener("resize", resizeAndDrawCanvas);
    };
  });
  const scalePoint = (e: MouseEvent<HTMLCanvasElement, globalThis.MouseEvent>): Point => {
    const { current } = canvasSizeRef;
    if (!current) return { x: e.clientX, y: e.clientY };
    else {
      const { scaleX, scaleY, top, left } = current;
      return {
        x: (e.clientX - left) / scaleX,
        y: (e.clientY - top) / scaleY,
      };
    }
  };
  return (
    <div
      style={{ position: "relative", width: "100%", backgroundColor: "red" }}
    >
      <canvas
        ref={canvasRef}
        width={canvasWidth}
        height={canvasHeight}
        {...{
          onMouseUp: (e) => onMouseUp(scalePoint(e)),
          onMouseMove: (e) => onMouseMove(scalePoint(e)),
        }}
        style={{
          position: "absolute",
          display: "block",
          width: "100%",
          height: "auto",
        }}
      >
        Your browser does not support the canvas element.
      </canvas>
      <img
        src="images/compass.png"
        alt="Compass"
        style={{
          position: "absolute",
          top: "175px",
          left: "50px",
          filter: "drop-shadow(0px 10px 10px rgba(0, 0, 0, 0.2))",
        }}
      />
      <img
        src="images/castleRiskLogo.png"
        alt="Castle Risk Logo"
        style={{
          position: "absolute",
          top: "20px",
          left: "20px",
          width: "20%",
          height: "auto",
          filter: "drop-shadow(0px 10px 10px rgba(0, 0, 0, 0.2))",
        }}
      />
    </div>
  );
};
