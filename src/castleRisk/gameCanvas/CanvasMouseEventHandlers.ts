import { OnCanvasMouseEventHandler } from "../../utils/canvas/OnCanvasMouseEventHandler"

export type CanvasMouseEventHandlers = {
    onMouseUp: OnCanvasMouseEventHandler
    onMouseMove: OnCanvasMouseEventHandler
}
