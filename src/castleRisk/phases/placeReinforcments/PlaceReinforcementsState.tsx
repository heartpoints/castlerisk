import { z } from "zod";
import { HasURI } from "@utils/zod/HasURI";
import { CastleRiskPlayState } from "../common/CastleRiskPlayState";

export const PlaceReinforcementsState = z.object({
     ...HasURI("PlaceReinforcementsState").shape,
     ...CastleRiskPlayState.shape,
});
export type PlaceReinforcementsState = z.infer<typeof PlaceReinforcementsState>;