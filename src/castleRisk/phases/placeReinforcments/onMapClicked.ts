import { not } from "@utils/predicates/not";
import { SetState } from "@utils/react/SetState";
import { isType } from "@utils/zod/isType";
import { new_ } from "@utils/zod/new_";
import { Reinforcements } from "castleRisk/cards/reinforcements/Reinforcements";
import { movePieceFromCurrentPlayerHandToTerritory } from "castleRisk/pieces/movePieceFromCurrentPlayerHandToTerritory";
import { currentPlayerFromState } from "castleRisk/players/currentPlayerFromState";
import { produce } from "immer";
import { activeTerritories } from "../chooseAction/activeTerritories";
import { ChooseActionState } from "../chooseAction/ChooseActionState";
import { CastleRiskState } from "../common/CastleRiskState";
import { currentlyHoveredActiveTerritory } from "../common/currentlyHoveredActiveTerritory";
import { isPlayerDonePlacingPieces } from "../common/isPlayerDonePlacingPieces";
import { PlaceReinforcementsState } from "./PlaceReinforcementsState";

export const onMapClicked = (
    state: PlaceReinforcementsState,
    setState: SetState<CastleRiskState>
  ) => {
    const territory = currentlyHoveredActiveTerritory(state.mousePoint)(
      activeTerritories(state)
    ).map((t) => {
      const stateWithPlacedPiece =
        movePieceFromCurrentPlayerHandToTerritory(t)(state);
      const newState = isPlayerDonePlacingPieces(stateWithPlacedPiece)
        ? new_(ChooseActionState)(
            produce(stateWithPlacedPiece, (draft) => {
              const currentPlayer = currentPlayerFromState(draft);
              currentPlayer.cards = currentPlayer.cards.filter(
                not((c) => isType(Reinforcements)(c) && c.inPlacement)
              );
              draft.discardPile.push(
                new_(Reinforcements)({ inPlacement: false })
              );
              draft.reinforcementsCount++
            })
          )
        : stateWithPlacedPiece;
      setState(newState);
    });
  };