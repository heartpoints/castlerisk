import { PropsWithSetState } from "@utils/react/PropsWithSetState";
import { CurrentPlayerCards } from "castleRisk/cards/CurrentPlayerCards";
import { GameCanvas } from "castleRisk/gameCanvas/gameCanvas";
import { numPiecesInHand } from "castleRisk/pieces/numPiecesInHand";
import { currentPlayerFromState } from "castleRisk/players/currentPlayerFromState";
import { SidePanelTemplate } from "../attack/general/SidePanelTemplate";
import { ActionText } from "../common/ActionText";
import { GameGrid } from "../common/GameGrid";
import { PlayerHeading } from "../common/PlayerHeading";
import { ReserveArmies } from "../common/ReserveArmies";
import { onMouseMove } from "../common/onMouseMove";
import { drawBoard } from "../common/placement/drawBoard";
import { PlaceReinforcementsState } from "./PlaceReinforcementsState";
import { onMapClicked } from "./onMapClicked";

export const PlaceReinforcements = (
  props: PropsWithSetState<PlaceReinforcementsState>
) => {
  const { setState, ...state } = props;
  const currentPlayer = currentPlayerFromState(state);
  return (
    <GameGrid
      gameCanvas={
        <GameCanvas
          drawGame={drawBoard(state)}
          onMouseMove={(e) => setState(onMouseMove(state)(e))}
          onMouseUp={() => onMapClicked(state, setState)}
        />
      }
      sidePanel={
        <SidePanelTemplate
          actionTextContent={
            <p>
              <ActionText>
                Place {numPiecesInHand(currentPlayer)}/
                {state.reinforcementsCount} Reinforcment Armies
              </ActionText>{" "}
              one at a time into any territory you currently occupy.
            </p>
          }
          playerHeading={<PlayerHeading {...state} />}
          reserveArmies={<ReserveArmies {...state} />}
        />
      }
      cards={<CurrentPlayerCards {...{ setState, ...state }} />}
    />
  );
};
