import { currentPlayerFromState } from '../../players/currentPlayerFromState'
import { Territory } from '../../territories/Territory'
import { PlaceCastlesState } from './PlaceCastlesState'
import { castlesAfterPlacement } from './castlesAfterPlacement'

export const moveCastleToTerritory = 
    (territory:Territory) =>
    (state:PlaceCastlesState) => 
    ({
        ...state,
        players: state.players.map(
            (player, playerIndex) =>
            playerIndex === state.currentPlayerIndex
                ? ({
                    ...player,
                    castles: castlesAfterPlacement(currentPlayerFromState(state))(territory)
                })
                : player
        )
    })