import { pipe } from 'fp-ts/lib/function'
import { originPoint } from '../../../utils/geometry/originPoint'
import { scramble } from '../../../utils/list/scramble'
import { initialCards } from '../../cards/initialCards'
import { Players } from '../../players/Players'
import { playersWithNewArmyReserves } from '../../players/playersWithNewArmyReserves'
import { assignRandomEmpires } from '../addPlayers/assignRandomEmpires'
import { stateWithUpToNPiecesInHand } from '../common/stateWithUpToNPiecesInHand'
import { PlaceCastlesState } from './PlaceCastlesState'
import { new_ } from '@utils/zod/new_'

export const toPlaceCastlesState = 
    (playersWithoutEmpires: Players):PlaceCastlesState => 
    stateWithUpToNPiecesInHand(1)(new_(PlaceCastlesState)({
        players: pipe(playersWithoutEmpires, playersWithNewArmyReserves, assignRandomEmpires, scramble),
        currentPlayerIndex: 0,
        mousePoint: originPoint,
        reinforcementsCount: 3,
        cards: initialCards,
        discardPile: [],
        hasAttacked: false,
    }));


