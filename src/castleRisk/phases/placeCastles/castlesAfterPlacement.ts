import { mergeProps } from '../../../utils/composition/mergeProps'
import { listFromArray } from '../../../utils/list/listFromArray'
import { Castles } from '../../castles/Castles'
import { Player } from '../../players/Player'
import { Territory } from '../../territories/Territory'
import { noTerritoryName } from '../../territories/noTerritoryName'

export const castlesAfterPlacement = 
    (currentPlayer: Player) =>
    (territory: Territory): Castles => 
    listFromArray(currentPlayer.castles)
        .replaceFirst(
            noTerritoryName,
            mergeProps({ territoryName: territory.name })
        ).asArray
