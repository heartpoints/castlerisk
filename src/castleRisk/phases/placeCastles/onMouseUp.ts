import { activeTerritoryClickHandler } from '../common/activeTerritoryClickHandler'
import { activeTerritories } from './activeTerritories'
import { stateWhenCastlePlaced } from './stateWhenCastlePlaced'

export const onMouseUp = activeTerritoryClickHandler(activeTerritories)(stateWhenCastlePlaced);
