import { PropsWithSetState } from "../../../utils/react/PropsWithSetState";
import { CardContainer } from "../../cards/CardContainer";
import { GameCanvas } from "../../gameCanvas/gameCanvas";
import { GameGrid } from "../common/GameGrid";
import { onMouseMove } from "../common/onMouseMove";
import { SidePanel } from "./SidePanel";
import { PlaceCastlesState } from "./PlaceCastlesState";
import { drawBoard } from "./drawBoard";
import { onMouseUp } from "./onMouseUp";
import { CurrentPlayerCards } from "../../cards/CurrentPlayerCards";

export const PlaceCastles = ({
  setState,
  ...state
}: PropsWithSetState<PlaceCastlesState>) => {
  return (
    <GameGrid
      gameCanvas={
        <GameCanvas
          drawGame={drawBoard(state)}
          onMouseMove={(e) => setState(onMouseMove(state)(e))}
          onMouseUp={onMouseUp(state)(setState)}
        />
      }
      cards={<CurrentPlayerCards {...{setState, ...state}} />}
      sidePanel={<SidePanel {...state} />}
    />
  );
};
