import { pipe } from 'fp-ts/lib/function'
import { movePieceFromCurrentPlayerHandToTerritory } from '../../pieces/movePieceFromCurrentPlayerHandToTerritory'
import { moveToNextPlayer } from '../../players/moveToNextPlayer'
import { playerHandIsEmpty } from '../../players/playerHandIsEmpty'
import { Territory } from '../../territories/Territory'
import { ClaimTerritoriesState } from '../claimTerritories/ClaimTerritoriesState'
import { toClaimTerritoriesState } from '../claimTerritories/toClaimTerritoriesState'
import { stateWithUpToNPiecesInHand } from '../common/stateWithUpToNPiecesInHand'
import { isPhaseComplete } from './isPhaseComplete'
import { moveCastleToTerritory } from './moveCastleToTerritory'
import { PlaceCastlesState } from './PlaceCastlesState'

export const stateWhenCastlePlaced = 
    (territory:Territory) =>
    (state:PlaceCastlesState):PlaceCastlesState | ClaimTerritoriesState => 
    pipe(
        state,
        movePieceFromCurrentPlayerHandToTerritory(territory),
        moveCastleToTerritory(territory),
        s => playerHandIsEmpty(s)
            ? isPhaseComplete(state)
                ? toClaimTerritoriesState(s)
                : pipe(
                    s,
                    moveToNextPlayer,
                    stateWithUpToNPiecesInHand(1),
                )
            : s
    )
