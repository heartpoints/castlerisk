import { FC } from "react"
import { SideNote } from "../chooseAction/SideNote"
import { ActionText } from "../common/ActionText"
import { CastleRiskPlayState } from "../common/CastleRiskPlayState"
import { SidePanelContainer } from "../common/SidePanelContainer"
import { PlayerHeading } from "../common/PlayerHeading"
import { ReserveArmies } from "../common/ReserveArmies"

export const SidePanel:FC<CastleRiskPlayState> = ({...state}) => <SidePanelContainer>
    <PlayerHeading {...state} />
      <p>
        <ActionText>Place Castles</ActionText>: In turn, players place their castle, along with 1 army piece,
        on any territory within the empire that matches their castle
      </p>
      <ReserveArmies {...state} />
      <SideNote>
        HINT: Since castles often have many armies defending them, it can be advantageous to place the castle on the water, so that it may attack by sea with large quantities. The castle can be replinished with the spoils of battle afterward.
      </SideNote>
</SidePanelContainer>
