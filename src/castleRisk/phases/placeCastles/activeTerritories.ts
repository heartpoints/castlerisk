import { currentPlayerInitialEmpire } from "../../empires/currentPlayerInitialEmpire"
import { CastleRiskPlayState } from "../common/CastleRiskPlayState"

export const activeTerritories = (state:CastleRiskPlayState) => currentPlayerInitialEmpire(state).territories