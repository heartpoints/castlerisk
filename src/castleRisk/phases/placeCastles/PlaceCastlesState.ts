import { HasURI } from "@utils/zod/HasURI";
import { z } from "zod";
import { CastleRiskPlayState } from "../common/CastleRiskPlayState";

export const PlaceCastlesState = z.object({
  ...HasURI("PlaceCastlesState").shape,
  ...CastleRiskPlayState.shape,
});
export type PlaceCastlesState = z.infer<typeof PlaceCastlesState>;
