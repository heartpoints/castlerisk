import { pipe } from "fp-ts/lib/function";
import { CanvasEffect } from "../../../utils/canvas/CanvasEffect";
import { sequentially } from "../../../utils/composition/sequentially";
import { pointPlus } from "../../../utils/geometry/pointPlus";
import { drawCastlePiece } from "../../castles/drawCastlePiece";
import { currentPlayerInitialEmpire } from "../../empires/currentPlayerInitialEmpire";
import { uniqueTerritoryGroupSegments } from "../../empires/uniqueTerritoryGroupSegments";
import { drawLayers } from "../common/drawLayers";
import { activeTerritories } from "./activeTerritories";
import { castleInHandOffset } from "./castleInHandOffset";
import { PlaceCastlesState } from "./PlaceCastlesState";

export const drawBoard = (state: PlaceCastlesState): CanvasEffect =>
  pipe(state, currentPlayerInitialEmpire, (empire) =>
    sequentially([
      drawLayers({
        activeTerritories: activeTerritories(state),
        highlightedLineSegments: uniqueTerritoryGroupSegments(empire),
        shouldDrawHighlight: true,
        state,
      }),
      drawCastlePiece(empire)(pointPlus(state.mousePoint)(castleInHandOffset)),
    ])
  );
