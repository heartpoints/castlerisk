import { CastleRiskPlayState } from "../common/CastleRiskPlayState";

import { HasURI } from "@utils/zod/HasURI";
import { z } from "zod";

export const PlaceSpoilsState = z.object({
  ...HasURI("PlaceSpoilsState").shape,
  ...CastleRiskPlayState.shape,
});
export type PlaceSpoilsState = z.infer<typeof PlaceSpoilsState>;
