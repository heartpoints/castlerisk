import { pipe } from "fp-ts/lib/function"
import { movePieceFromCurrentPlayerHandToTerritory } from "../../pieces/movePieceFromCurrentPlayerHandToTerritory"
import { Territory } from "../../territories/Territory"
import { ChooseActionState } from "../chooseAction/ChooseActionState"
import { toChooseActionFromPlaceSpoilsState } from "../chooseAction/toChooseActionFromPlaceSpoilsState"
import { isPlayerDonePlacingPieces } from "../common/isPlayerDonePlacingPieces"
import { PlaceSpoilsState } from "./PlaceSpoilsState"

export const stateWhenActiveTerritoryClicked = 
    (territory:Territory) =>
    (state: PlaceSpoilsState): PlaceSpoilsState | ChooseActionState => 
    pipe(
        state,
        movePieceFromCurrentPlayerHandToTerritory(territory),
        s => isPlayerDonePlacingPieces(s)
            ? toChooseActionFromPlaceSpoilsState(s)
            : s
    )
