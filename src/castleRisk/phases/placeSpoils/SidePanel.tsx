import { FC } from "react";
import { numPiecesInHand } from "../../pieces/numPiecesInHand";
import { currentPlayerFromState } from "../../players/currentPlayerFromState";
import { SideNote } from "../chooseAction/SideNote";
import { ActionText } from "../common/ActionText";
import { CastleRiskPlayState } from "../common/CastleRiskPlayState";
import { SidePanelContainer } from "../common/SidePanelContainer";
import { PlayerHeading } from "../common/PlayerHeading";
import { ReserveArmies } from "../common/ReserveArmies";
import { numberOfSpoilsForPlayer } from "./numberOfSpoilsForPlayer";

export const SidePanel: FC<CastleRiskPlayState> = ({ ...state }) => {
  const currentPlayer = currentPlayerFromState(state);
  const { players } = state;
  return (
    <>
      <SidePanelContainer>
        <PlayerHeading {...state} />
        <p>
          <ActionText>
            Place {numPiecesInHand(currentPlayer)} /{" "}
            {numberOfSpoilsForPlayer(players)(currentPlayer)} Spoils
          </ActionText>{" "}
          by clicking any territory you currently own.
        </p>
        <ReserveArmies {...state} />
        <SideNote>
          NOTE: As a reward for the territories you controlled at the end of
          your attack phase, you can now Place your spoils one at a time in any
          territory you currently occupy. After you have placed all of them,
          play proceeds to the next player.
        </SideNote>
      </SidePanelContainer>
    </>
  );
};
