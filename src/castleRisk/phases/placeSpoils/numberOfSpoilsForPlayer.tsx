import { Player } from "../../players/Player";
import { Players } from "../../players/Players";
import { empiresFullyControlledByPlayer } from "../../empires/empiresFullyControlledByPlayer";
import { isOwnerOfAllIndependentTerritories } from "../../empires/isOwnerOfAllIndependentTerritories";
import { spoilsIfControllingAllIndependentTerritories } from "./spoilsIfControllingAllIndependentTerritories";
import { spoilsPerCastle } from "./spoilsPerCastle";
import { spoilsPerFullyControlledEmpire } from "./spoilsPerFullyControlledEmpire";


export const numberOfSpoilsForPlayer = (players: Players) => (player: Player) => player.castles.length * spoilsPerCastle +
    empiresFullyControlledByPlayer({players}, player).length * spoilsPerFullyControlledEmpire +
    (isOwnerOfAllIndependentTerritories(players)(player) ? spoilsIfControllingAllIndependentTerritories : 0);
