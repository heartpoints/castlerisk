import { activeTerritoryClickHandler } from "../common/activeTerritoryClickHandler";
import { activeTerritories } from "../common/placement/activeTerritories";
import { stateWhenActiveTerritoryClicked } from "./stateWhenActiveTerritoryClicked";

export const onMouseUp = activeTerritoryClickHandler(activeTerritories)(stateWhenActiveTerritoryClicked);
