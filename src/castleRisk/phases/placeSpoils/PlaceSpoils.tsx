import { PropsWithSetState } from "../../../utils/react/PropsWithSetState";
import { CardContainer } from "../../cards/CardContainer";
import { GameCanvas } from "../../gameCanvas/gameCanvas";
import { GameGrid } from "../common/GameGrid";
import { onMouseMove } from "../common/onMouseMove";
import { drawBoard } from "../common/placement/drawBoard";
import { SidePanel } from "./SidePanel";
import { PlaceSpoilsState } from "./PlaceSpoilsState";
import { onMouseUp } from "./onMouseUp";
import { CurrentPlayerCards } from "../../cards/CurrentPlayerCards";

export const PlaceSpoils = (props: PropsWithSetState<PlaceSpoilsState>) => {
  const { setState, ...state } = props;
  return (
    <GameGrid
      gameCanvas={
        <GameCanvas
          drawGame={drawBoard(state)}
          onMouseMove={(e) => setState(onMouseMove(state)(e))}
          onMouseUp={onMouseUp(state)(setState)}
        />
      }
      sidePanel={<SidePanel {...state} />}
      cards={<CurrentPlayerCards {...{setState, ...state}} />}
    />
  );
};
