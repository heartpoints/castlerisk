import { pipe } from "fp-ts/lib/function"
import { stateWithUpToNPiecesInHand } from "../common/stateWithUpToNPiecesInHand"
import { PlaceSpoilsState } from "./PlaceSpoilsState"
import { ChooseActionState } from "../chooseAction/ChooseActionState"
import { new_ } from "@utils/zod/new_"

export const toPlaceSpoils = 
    (numberOfSpoilsToPlace:number) =>
    (state:ChooseActionState):PlaceSpoilsState => 
    pipe(
        state,
        stateWithUpToNPiecesInHand(numberOfSpoilsToPlace),
        new_(PlaceSpoilsState)
    )