import { new_ } from "@utils/zod/new_";
import { deactivateAdmiral } from "castleRisk/cards/admiral/deactivateAdmiral";
import { pipe } from "fp-ts/lib/function";
import { shouldDeactivateAdmiral } from "../../../cards/admiral/shouldDeactivateAdmiral";
import { Territory } from "../../../territories/Territory";
import { BoardAdmiralShipState } from "../boardAdmiralShip/BoardAdmiralShipState";
import { ChooseAttackTargetState } from "../chooseAttackTarget/ChooseAttackTargetState";
import { maxAttackPiecesForState } from "../general/maxAttackPiecesForState";
import { maxDefensePiecesForState } from "../general/maxDefensePiecesForState";
import { ChooseDiceCountState } from "./ChooseDiceCountState";

export const toChooseDiceCount = (
  state: ChooseAttackTargetState | BoardAdmiralShipState,
  attackTargetTerritory: Territory
) =>
  pipe(
    state,
    (s) =>
      new_(ChooseDiceCountState)({
        ...s,
        attackTargetTerritory,
        attackDiceCount: maxAttackPiecesForState({
          ...s,
          attackTargetTerritory,
        }),
        defenseDiceCount: maxDefensePiecesForState({
          ...s,
          attackTargetTerritory,
        }),
        diceRolls: [],
      }),
    (s) => (shouldDeactivateAdmiral(s) ? deactivateAdmiral(s) : s)
  );
