
import { HasURI } from "@utils/zod/HasURI";
import { z } from "zod";
import { AttackReadyState } from "../general/AttackReadyState";

export const ChooseDiceCountState = z.object({
  ...HasURI("ChooseDiceCountState").shape,
  ...AttackReadyState.shape,
});
export type ChooseDiceCountState = z.infer<typeof ChooseDiceCountState>;
