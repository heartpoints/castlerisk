import { Button, Tooltip } from "@mui/material";
import { isType } from "@utils/zod/isType";
import { new_ } from "@utils/zod/new_";
import { isAdmiralActive } from "castleRisk/cards/admiral/isAdmiralActive";
import { deactivateBattleCards } from "castleRisk/cards/deactivateBattleCards";
import { GeneralAvatar } from "castleRisk/cards/general/GeneralAvatar";
import { AdmiralAvatar } from "../../../cards/admiral/AdmiralAvatar";
import { activeGenerals } from "../../../cards/general/activeGenerals";
import { AttackDie } from "../../../dice/AttackDie";
import { DefenseDie } from "../../../dice/DefenseDie";
import { currentPlayerFromState } from "../../../players/currentPlayerFromState";
import { playerForTerritory } from "../../../territories/territoryOwner";
import { ChooseActionState } from "../../chooseAction/ChooseActionState";
import { SideNote } from "../../chooseAction/SideNote";
import { ActionText } from "../../common/ActionText";
import { PlayerActionButtons } from "../../common/PlayerActionButtons";
import { PlayerHeading } from "../../common/PlayerHeading";
import { BattleUndecidedState } from "../battleUndecided/BattleUndecidedState";
import { ChooseAttackTargetState } from "../chooseAttackTarget/ChooseAttackTargetState";
import { BattleHistory } from "../general/BattleHistory";
import { maxAttackPiecesForState } from "../general/maxAttackPiecesForState";
import { maxDefensePiecesForState } from "../general/maxDefensePiecesForState";
import { performDiceRoll } from "../general/performDiceRoll";
import { SidePanelTemplate } from "../general/SidePanelTemplate";
import { ChooseDiceCountProps } from "./ChooseDiceCountProps";
import { numPiecesOnBoat } from "castleRisk/cards/admiral/numPiecesOnBoat";
import { Battleship } from "../boardAdmiralShip/Battleship";

export const SidePanel = ({ setState, ...state }: ChooseDiceCountProps) => {
  const {
    attackOriginTerritory,
    attackTargetTerritory,
    attackDiceCount,
    defenseDiceCount,
    players,
  } = state;
  const maxAttackPieces = maxAttackPiecesForState(state);
  const maxDefensePieces = maxDefensePiecesForState(state);
  const attackerName = currentPlayerFromState(state).name;
  const defendingPlayer = playerForTerritory(players)(attackTargetTerritory)!;
  const canBackOutOrQuit = isAdmiralActive(state) && state.diceRolls.length > 0;
  return (
    <SidePanelTemplate
      playerHeading={<PlayerHeading {...state} />}
      actionTextContent={
        <>
          <p>
            <ActionText>Choose Dice Counts</ActionText> -{" "}
            <strong>{attackerName}</strong> is attacking{" "}
            <strong>{attackTargetTerritory.name}</strong> from{" "}
            <strong>{attackOriginTerritory.name}</strong>
            {isAdmiralActive(state) && (
              <small>
                {" "}
                with Admiral <AdmiralAvatar />
              </small>
            )}
            !{" "}
          </p>
          <small>
            <strong>{defendingPlayer.name}</strong>:{" "}
            <i>&quot;Defend yourself, sir!&quot;</i>
          </small>
        </>
      }
      playerActionButtons={
        <>
          <PlayerActionButtons>
            <p>
              <strong>How many Attack Dice?</strong>
            </p>
            <div style={{ display: "flex", flexDirection: "row", gap: "1em" }}>
              <AttackDie
                sideToDisplay={1}
                selected={attackDiceCount == 1}
                onClick={() => setState({ ...state, attackDiceCount: 1 })}
              />
              <AttackDie
                sideToDisplay={2}
                selected={attackDiceCount == 2}
                onClick={() => setState({ ...state, attackDiceCount: 2 })}
                disabled={maxAttackPieces < 2}
              />
              <AttackDie
                sideToDisplay={3}
                selected={attackDiceCount == 3}
                onClick={() => setState({ ...state, attackDiceCount: 3 })}
                disabled={maxAttackPieces < 3}
              />
            </div>
            <div style={{ display: "flex", flexDirection: "row", gap: "1em" }}>
              {activeGenerals(state).map((_, i) => (
                <GeneralAvatar key={i} />
              ))}
            </div>
          </PlayerActionButtons>
          <PlayerActionButtons>
            <p>
              <strong>How many Defense Dice?</strong>
            </p>
            <div style={{ display: "flex", flexDirection: "row", gap: "1em" }}>
              <DefenseDie
                sideToDisplay={1}
                selected={defenseDiceCount == 1}
                onClick={() => setState({ ...state, defenseDiceCount: 1 })}
              />
              <DefenseDie
                sideToDisplay={2}
                selected={defenseDiceCount == 2}
                onClick={() => setState({ ...state, defenseDiceCount: 2 })}
                disabled={maxDefensePieces < 2}
              />
            </div>
          </PlayerActionButtons>
          <PlayerActionButtons>
            <Button
              variant="contained"
              onClick={() => {
                const { attackDiceCount, defenseDiceCount } = state;
                if (attackDiceCount && defenseDiceCount) {
                  setState(
                    performDiceRoll({
                      ...state,
                      attackDiceCount,
                      defenseDiceCount,
                      diceRolls: isType(BattleUndecidedState)(state)
                        ? state.diceRolls
                        : [],
                    })
                  );
                }
              }}
              disabled={!attackDiceCount || !defenseDiceCount}
            >
              Attack (Roll)
            </Button>
            <Tooltip
              title={
                canBackOutOrQuit
                  ? "Once an Admiral is deployed, the battle must be fought to the death"
                  : ""
              }
            >
              <div
                style={{
                  display: "inline-block",
                  width: "100%",
                  cursor: canBackOutOrQuit ? "not-allowed" : "pointer",
                }}
              >
                <Button
                  variant="outlined"
                  onClick={() =>
                    setState(
                      new_(ChooseActionState)(deactivateBattleCards(state))
                    )
                  }
                  disabled={canBackOutOrQuit}
                  style={{width: "100%"}}
                >
                  End Attack
                </Button>
              </div>
            </Tooltip>
            <Tooltip
              title={
                canBackOutOrQuit
                  ? "Once an Admiral is deployed, the battle must be fought to the death"
                  : ""
              }
            >
              <div
                style={{
                  display: "inline-block",
                  cursor: canBackOutOrQuit ? "not-allowed" : "pointer",
                  width: "100%",
                }}
              >
                <Button
                  variant="outlined"
                  onClick={() =>
                    setState(
                      new_(ChooseAttackTargetState)(
                        deactivateBattleCards(state)
                      )
                    )
                  }
                  disabled={canBackOutOrQuit}
                  style={{width: "100%"}}
                >
                  Back
                </Button>
              </div>
            </Tooltip>
            {isType(BattleUndecidedState)(state) && (
              <BattleHistory {...state} />
            )}
          </PlayerActionButtons>
          {numPiecesOnBoat(state.players) > 0 && <Battleship {...state} />}
        </>
      }
      sideNote={
        <SideNote>
          NOTE: Maximum attack dice is one less than # attacking armies. Maximum
          defense dice is # defending armies. Castles cannot be attacked with
          &gt; 2 dice.
        </SideNote>
      }
    />
  );
};
