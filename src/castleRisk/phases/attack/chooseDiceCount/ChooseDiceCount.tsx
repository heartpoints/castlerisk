import { doNothing } from "../../../../utils/axioms/doNothing";
import { GameCanvas } from "../../../gameCanvas/gameCanvas";
import { GameGrid } from "../../common/GameGrid";
import { onMouseMove } from "../../common/onMouseMove";
import { ChooseDiceCountProps } from "./ChooseDiceCountProps";
import { CurrentPlayerCards } from "../../../cards/CurrentPlayerCards";
import { drawBoard } from "../general/drawBoard";
import { SidePanel } from "./SidePanel";

export const ChooseDiceCount = ({
  setState,
  ...state
}: ChooseDiceCountProps) => (
  <GameGrid
    gameCanvas={
      <GameCanvas
        drawGame={drawBoard(state)}
        onMouseMove={(e) => setState(onMouseMove(state)(e))}
        onMouseUp={doNothing}
      />
    }
    sidePanel={<SidePanel {...{ setState, ...state }} />}
    cards={<CurrentPlayerCards {...{ setState, ...state }} />}
  />
);
