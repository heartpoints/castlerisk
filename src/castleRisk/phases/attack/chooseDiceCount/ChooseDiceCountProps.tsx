import { PropsWithSetState } from "../../../../utils/react/PropsWithSetState";
import { BattleUndecidedState } from "../battleUndecided/BattleUndecidedState";
import { ChooseDiceCountState } from "./ChooseDiceCountState";

export type ChooseDiceCountProps = PropsWithSetState<
  ChooseDiceCountState | BattleUndecidedState
>;
