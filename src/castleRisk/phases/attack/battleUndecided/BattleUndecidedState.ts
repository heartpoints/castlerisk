import { HasURI } from "@utils/zod/HasURI";
import { z } from "zod";
import { AttackReadyState } from "../general/AttackReadyState";

export const BattleUndecidedState = z.object({
  ...HasURI("BattleUndecidedState").shape,
  ...AttackReadyState.shape,
});
export type BattleUndecidedState = z.infer<typeof BattleUndecidedState>;
