import { Button, Slider } from "@mui/material";
import { doNothing } from "@utils/axioms/doNothing";
import { PropsWithSetState } from "@utils/react/PropsWithSetState";
import { new_ } from "@utils/zod/new_";
import { deactivateAdmiral } from "castleRisk/cards/admiral/deactivateAdmiral";
import { numPiecesOnBoat } from "castleRisk/cards/admiral/numPiecesOnBoat";
import { CurrentPlayerCards } from "castleRisk/cards/CurrentPlayerCards";
import { GameCanvas } from "castleRisk/gameCanvas/gameCanvas";
import { SideNote } from "castleRisk/phases/chooseAction/SideNote";
import { ActionText } from "castleRisk/phases/common/ActionText";
import { PlayerActionButtons } from "castleRisk/phases/common/PlayerActionButtons";
import { PlayerHeading } from "castleRisk/phases/common/PlayerHeading";
import { GameGrid } from "../../common/GameGrid";
import { onMouseMove } from "../../common/onMouseMove";
import { ChooseAttackTargetState } from "../chooseAttackTarget/ChooseAttackTargetState";
import { toChooseDiceCount } from "../chooseDiceCount/toChooseDiceCount";
import { drawBoard } from "../general/drawBoard";
import { maxBoardablePieces } from "../../../cards/admiral/maxBoardablePieces";
import { SidePanelTemplate } from "../general/SidePanelTemplate";
import { Battleship } from "./Battleship";
import { BoardAdmiralShipState } from "./BoardAdmiralShipState";
import { boardPiecesOnShip } from "./boardPiecesOnShip";
import { emptyShip } from "./emptyShip";

export const BoardAdmiralShip = ({
  setState,
  ...state
}: PropsWithSetState<BoardAdmiralShipState>) => {
  const maxBoardPieces = maxBoardablePieces(
    state.players,
    state.attackOriginTerritory
  );
  return (
    <GameGrid
      gameCanvas={
        <GameCanvas
          drawGame={drawBoard(state)}
          onMouseMove={(e) => setState(onMouseMove(state)(e))}
          onMouseUp={doNothing}
        />
      }
      sidePanel={
        <SidePanelTemplate
          playerHeading={<PlayerHeading {...state} />}
          actionTextContent={
            <p>
              <ActionText>All Aboard!</ActionText> - how many armies from{" "}
              <strong>{state.attackOriginTerritory.name}</strong> should board
              your Admiral's ship to attack{" "}
              <strong>{state.attackTargetTerritory.name}</strong>?
            </p>
          }
          playerActionButtons={
            <PlayerActionButtons>
              <p style={{ textAlign: "center" }}>
                # Armies to Board:
                <Slider
                  style={{ width: "75%" }}
                  size="small"
                  // defaultValue={1}
                  value={numPiecesOnBoat(state.players)}
                  aria-label="Small"
                  min={1}
                  max={maxBoardPieces}
                  valueLabelDisplay="on"
                  onChange={(_, newValue) => {
                    setState(boardPiecesOnShip(state, Number(newValue)));
                  }}
                  marks={[
                    { value: 1, label: 1 },
                    {
                      value: maxBoardPieces,
                      label: maxBoardPieces,
                    },
                  ]}
                />
              </p>
              <Button variant="contained" onClick={() => setState(toChooseDiceCount(state, state.attackTargetTerritory))}>Set Sail</Button>
              <Button
                variant="outlined"
                onClick={() => setState(onCancelBoarding(state))}
              >
                Cancel
              </Button>
            </PlayerActionButtons>
          }
          reserveArmies={<Battleship {...state} />}
          sideNote={
            <SideNote>
              NOTE: Admiral and crew must battle to the death. You cannot end
              the battle until you either win or lose! Upon loss, your admiral
              is discarded. Upon victory, your admiral is returned to your hand,
              and all remaining crew must occupy the conquered territory.
            </SideNote>
          }
        />
      }
      cards={<CurrentPlayerCards {...{ setState, ...state }} />}
    />
  );
};

const onCancelBoarding = (state: BoardAdmiralShipState) => {
  return new_(ChooseAttackTargetState)(deactivateAdmiral(emptyShip(state)));
};
