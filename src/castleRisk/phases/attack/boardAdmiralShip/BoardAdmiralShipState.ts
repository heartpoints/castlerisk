import { HasURI } from "@utils/zod/HasURI";
import { z } from "zod";
import { HasAttackOriginTerritory } from "../general/HasAttackOriginTerritory";
import { HasAttackTargetTerritory } from "../general/HasAttackTargetTerritory";
import { CastleRiskPlayState } from "castleRisk/phases/common/CastleRiskPlayState";

export const BoardAdmiralShipState = z.object({
  ...HasURI("BoardAdmiralShipState").shape,
  ...CastleRiskPlayState.shape,
  ...HasAttackOriginTerritory.shape,
  ...HasAttackTargetTerritory.shape,
});
export type BoardAdmiralShipState = z.infer<typeof BoardAdmiralShipState>;
