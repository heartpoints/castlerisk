import { rgbaToCSSString } from "@utils/colors/rgbaToCSSString";
import { isType } from "@utils/zod/isType";
import { HasPlayers } from "castleRisk/phases/common/HasPlayers";
import { PlacedOnBoat } from "castleRisk/pieces/PlacedOnBoat";
import { seaColor } from "castleRisk/water/seaColor";

export const Battleship = (state: HasPlayers) => {
  const numBoardedPieces = state.players
    .flatMap((p) => p.pieces)
    .filter(isType(PlacedOnBoat)).length;
  return (
    <div
      style={{
        padding: "1em",
        width: "100%",
        maxWidth: "100%",
        display: "grid",
        gap: "1em",
        gridTemplateColumns: "30% 70%",
      }}
    >
      <img
        src="/images/battleship.jpg"
        width="1081"
        height="1390"
        style={{ width: "100%", height: "auto", borderRadius: "5em" }}
      />
      <div>
        <h3>Admiral's Ship</h3>
        <p>
          <strong
            style={{
              backgroundColor: rgbaToCSSString(seaColor),
              padding: "0.25em",
              borderRadius: "0.7em",
            }}
          >
            {numBoardedPieces}
          </strong>{" "}
          {numBoardedPieces == 1 ? "army" : "armies"} aboard
        </p>
      </div>
    </div>
  );
};
