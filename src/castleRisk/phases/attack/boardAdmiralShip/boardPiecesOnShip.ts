import { isType } from "@utils/zod/isType";
import { new_ } from "@utils/zod/new_";
import { Pieces } from "castleRisk/pieces/Pieces";
import { PlacedInTerritory } from "castleRisk/pieces/PlacedInTerritory";
import { PlacedOnBoat } from "castleRisk/pieces/PlacedOnBoat";
import { currentPlayerFromState } from "castleRisk/players/currentPlayerFromState";
import { produce } from "immer";
import { BoardAdmiralShipState } from "./BoardAdmiralShipState";
import { emptyShip } from "./emptyShip";

export const boardPiecesOnShip = (
  state: BoardAdmiralShipState,
  desiredNumberOnBoard: number
) => {
  const stateWithTemporaryEmptyShip = emptyShip(state);
  const { attackOriginTerritory } = stateWithTemporaryEmptyShip;
  const updatedPieces = currentPlayerFromState(
    stateWithTemporaryEmptyShip
  ).pieces.reduce(
    ({ newPieces, numToMove }, currentPiece) => {
      const isMoving =
        numToMove > 0 &&
        isType(PlacedInTerritory)(currentPiece) &&
        currentPiece.territoryName == attackOriginTerritory.name;

      return {
        newPieces: [
          ...newPieces,
          isMoving ? new_(PlacedOnBoat)(currentPiece) : currentPiece,
        ],
        numToMove: isMoving ? numToMove - 1 : numToMove,
      };
    },
    { newPieces: [] as Pieces, numToMove: desiredNumberOnBoard }
  ).newPieces;

  return produce(stateWithTemporaryEmptyShip, (draft) => {
    currentPlayerFromState(draft).pieces = updatedPieces;
  });
};
