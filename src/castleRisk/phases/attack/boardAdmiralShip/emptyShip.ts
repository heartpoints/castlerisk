import { isType } from "@utils/zod/isType";
import { new_ } from "@utils/zod/new_";
import { PlacedInTerritory } from "castleRisk/pieces/PlacedInTerritory";
import { PlacedOnBoat } from "castleRisk/pieces/PlacedOnBoat";
import { currentPlayerFromState } from "castleRisk/players/currentPlayerFromState";
import { produce } from "immer";
import { BoardAdmiralShipState } from "./BoardAdmiralShipState";

export const emptyShip = (state:BoardAdmiralShipState) => {
  const { pieces } = currentPlayerFromState(state);
  const piecesAfterEmptyingShip = pieces.map((p) =>
    isType(PlacedOnBoat)(p)
      ? new_(PlacedInTerritory)({ ...p, territoryName: state.attackOriginTerritory.name })
      : p
  );
  return produce(state, draft => {
    currentPlayerFromState(draft).pieces = piecesAfterEmptyingShip
  })
}