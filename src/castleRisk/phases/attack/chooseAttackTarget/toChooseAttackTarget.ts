import { new_ } from "@utils/zod/new_";
import { pipe } from "fp-ts/lib/function";
import { Territory } from "../../../territories/Territory";
import { ChooseAttackOriginState } from "../chooseAttackOrigin/ChooseAttackOriginState";
import { ChooseAttackTargetState } from "./ChooseAttackTargetState";

export const toChooseAttackTarget = (
  state: ChooseAttackOriginState,
  attackSourceTerritory: Territory
) =>
  pipe(state, (s) =>
    new_(ChooseAttackTargetState)({
      ...s,
      attackOriginTerritory: attackSourceTerritory,
    })
  );
