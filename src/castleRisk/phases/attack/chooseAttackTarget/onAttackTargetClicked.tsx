import { new_ } from "@utils/zod/new_";
import { deactivateAdmiral } from "castleRisk/cards/admiral/deactivateAdmiral";
import { currentlyHoveredActiveTerritory } from "castleRisk/phases/common/currentlyHoveredActiveTerritory";
import { territoriesAttackableByLand } from "castleRisk/phases/common/territoriesAttackableByLand";
import { currentPlayerFromState } from "castleRisk/players/currentPlayerFromState";
import { territoryEquals } from "castleRisk/territories/territoryEquals";
import { BoardAdmiralShipState } from "../boardAdmiralShip/BoardAdmiralShipState";
import { toChooseDiceCount } from "../chooseDiceCount/toChooseDiceCount";
import { activeTerritories } from "./activeTerritories";
import { ChooseAttackTargetState } from "./ChooseAttackTargetState";
import { boardPiecesOnShip } from "../boardAdmiralShip/boardPiecesOnShip";

export const onAttackTargetClicked = (state: ChooseAttackTargetState) => {
  const { mousePoint, players, attackOriginTerritory } = state;
  return currentlyHoveredActiveTerritory(mousePoint)(
    activeTerritories(state)
  ).mapOrDefault((attackTargetTerritory) => {
    const currentPlayer = currentPlayerFromState(state);
    const isLandAttack = territoriesAttackableByLand(players)(currentPlayer)(attackOriginTerritory).some(territoryEquals(attackTargetTerritory));
    return isLandAttack
      ? deactivateAdmiral(toChooseDiceCount(state, attackTargetTerritory))
      : boardPiecesOnShip(new_(BoardAdmiralShipState)({ ...state, attackTargetTerritory }), 1);
  }, state);
};
