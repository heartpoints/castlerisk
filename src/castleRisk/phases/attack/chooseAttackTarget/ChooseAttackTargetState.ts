import { CastleRiskPlayState } from "../../common/CastleRiskPlayState";
import { HasAttackOriginTerritory } from "../general/HasAttackOriginTerritory";

import { HasURI } from "@utils/zod/HasURI";
import { z } from "zod";

export const ChooseAttackTargetState = z.object({
  ...HasURI("ChooseAttackTargetState").shape,
  ...CastleRiskPlayState.shape,
  ...HasAttackOriginTerritory.shape,
});
export type ChooseAttackTargetState = z.infer<typeof ChooseAttackTargetState>;
