import { Button } from "@mui/material";
import { new_ } from "@utils/zod/new_";
import { PropsWithSetState } from "../../../../utils/react/PropsWithSetState";
import { ChooseActionState } from "../../chooseAction/ChooseActionState";
import { ActionText } from "../../common/ActionText";
import { PlayerActionButtons } from "../../common/PlayerActionButtons";
import { PlayerHeading } from "../../common/PlayerHeading";
import { ReserveArmies } from "../../common/ReserveArmies";
import { SidePanelContainer } from "../../common/SidePanelContainer";
import { ChooseAttackOriginState } from "../chooseAttackOrigin/ChooseAttackOriginState";
import { SideNoteForAttack } from "../general/SideNoteForAttack";
import { ChooseAttackTargetState } from "./ChooseAttackTargetState";
import { deactivateBattleCards } from "castleRisk/cards/deactivateBattleCards";

type SidePanelProps = PropsWithSetState<ChooseAttackTargetState>;
export const SidePanel = ({
  setState,
  ...state
}: SidePanelProps) => (
  <SidePanelContainer>
    <PlayerHeading {...state} />
    <p>
      <ActionText>Attack: Choose Target</ActionText> to attack from{" "}
      <strong>{state.attackOriginTerritory.name}</strong>. You may attack any
      adjacent opponent territory.
    </p>
    <PlayerActionButtons>
      <Button
        variant="outlined"
        onClick={() => setState(new_(ChooseActionState)(deactivateBattleCards(state)))}
      >
        Cancel
      </Button>{" "}
      <Button
        variant="outlined"
        onClick={() => setState(new_(ChooseAttackOriginState)(deactivateBattleCards(state)))}
      >
        Back
      </Button>{" "}
    </PlayerActionButtons>
    <ReserveArmies {...state} />
    <SideNoteForAttack />
  </SidePanelContainer>
);
