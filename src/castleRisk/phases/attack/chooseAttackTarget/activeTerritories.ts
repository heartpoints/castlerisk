import { currentPlayerFromState } from "../../../players/currentPlayerFromState"
import { attackableTerritories } from "../../common/attackableTerritories"
import { ChooseAttackTargetState } from "./ChooseAttackTargetState"

export const activeTerritories = 
    (state:ChooseAttackTargetState) =>
    attackableTerritories(state.players)(currentPlayerFromState(state))(state.attackOriginTerritory)
