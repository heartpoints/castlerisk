import { CanvasEffect } from "../../../../utils/canvas/CanvasEffect";
import { segmentsFromShape } from "../../../../utils/geometry/segmentsFromShape";
import { drawLayers } from "../../common/drawLayers";
import { activeTerritories } from "./activeTerritories";
import { ChooseAttackTargetState } from "./ChooseAttackTargetState";

export const drawBoard = (state: ChooseAttackTargetState): CanvasEffect =>
  drawLayers({
    activeTerritories: activeTerritories(state),
    highlightedLineSegments: segmentsFromShape(
      state.attackOriginTerritory.boundary
    ),
    shouldDrawHighlight: true,
    state,
  });
