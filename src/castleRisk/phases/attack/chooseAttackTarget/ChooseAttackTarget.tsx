import { PropsWithSetState } from "../../../../utils/react/PropsWithSetState";
import { CardContainer } from "../../../cards/CardContainer";
import { GameCanvas } from "../../../gameCanvas/gameCanvas";
import { GameGrid } from "../../common/GameGrid";
import { onMouseMove } from "../../common/onMouseMove";
import { CurrentPlayerCards } from "../../../cards/CurrentPlayerCards";
import { ChooseAttackTargetState } from "./ChooseAttackTargetState";
import { drawBoard } from "./drawBoard";
import { SidePanel } from "./SidePanel";
import { onAttackTargetClicked } from "./onAttackTargetClicked";

export const ChooseAttackTarget = ({
  setState,
  ...state
}: PropsWithSetState<ChooseAttackTargetState>) => (
  <GameGrid
    gameCanvas={
      <GameCanvas
        drawGame={drawBoard(state)}
        onMouseMove={(e) => setState(onMouseMove(state)(e))}
        onMouseUp={() => setState(onAttackTargetClicked(state))}
      />
    }
    sidePanel={<SidePanel {...{ setState, ...state }} />}
    cards={<CurrentPlayerCards {...{ setState, ...state }} />}
  />
);
