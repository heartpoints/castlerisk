import { doNothing } from "../../../../utils/axioms/doNothing";
import { PropsWithSetState } from "../../../../utils/react/PropsWithSetState";
import { CurrentPlayerCards } from "../../../cards/CurrentPlayerCards";
import { GameCanvas } from "../../../gameCanvas/gameCanvas";
import { GameGrid } from "../../common/GameGrid";
import { onMouseMove } from "../../common/onMouseMove";
import { drawBoard } from "../general/drawBoard";
import { DefenderWonState } from "./DefenderWonState";
import { SidePanel } from "./SidePanel";

export const DefenderWon = ({
  setState,
  ...state
}: PropsWithSetState<DefenderWonState>) => (
  <GameGrid
    gameCanvas={
      <GameCanvas
        drawGame={drawBoard(state)}
        onMouseMove={(e) => setState(onMouseMove(state)(e))}
        onMouseUp={doNothing}
      />
    }
    sidePanel={<SidePanel setState={setState} {...state} />}
    cards={<CurrentPlayerCards {...{setState, ...state}} />}
  />
);
