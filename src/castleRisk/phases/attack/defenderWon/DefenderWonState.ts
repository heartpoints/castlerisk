import { AttackReadyState } from "../general/AttackReadyState";

import { HasURI } from "@utils/zod/HasURI";
import { z } from "zod";
import { HasWasAdmiralActive } from "../general/HasWasAdmiralActive";

export const DefenderWonState = z.object({
  ...HasURI("DefenderWonState").shape,
  ...AttackReadyState.shape,
  ...HasWasAdmiralActive.shape,
});
export type DefenderWonState = z.infer<typeof DefenderWonState>;
