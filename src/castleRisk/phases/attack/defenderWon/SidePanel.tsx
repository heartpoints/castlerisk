import { Alert, Button } from "@mui/material";
import { new_ } from "@utils/zod/new_";
import { PropsWithSetState } from "../../../../utils/react/PropsWithSetState";
import { currentPlayerName } from "../../../players/currentPlayerName";
import { ChooseActionState } from "../../chooseAction/ChooseActionState";
import { PlayerActionButtons } from "../../common/PlayerActionButtons";
import { PlayerHeading } from "../../common/PlayerHeading";
import { SidePanelContainer } from "../../common/SidePanelContainer";
import { BattleHistory } from "../general/BattleHistory";
import { defendingPlayer } from "../general/defendingPlayer";
import { DefenderWonState } from "./DefenderWonState";
import { FC } from "react";
import { AdmiralAvatar } from "castleRisk/cards/admiral/AdmiralAvatar";

export const SidePanel: FC<PropsWithSetState<DefenderWonState>> = ({
  setState,
  ...state
}) => {
  const { attackTargetTerritory, wasAdmiralActive, attackOriginTerritory } =
    state;
  return (
    <SidePanelContainer>
      <PlayerHeading {...state} />
      <Alert severity="error" variant="filled">
        {defendingPlayer(state).name} successfully defended{" "}
        {attackTargetTerritory.name} against {currentPlayerName(state)}'s attack
        from {attackOriginTerritory.name}!{" "}
        {wasAdmiralActive ? (
          <span>
            Unfortunately, the Admiral <AdmiralAvatar /> was lost as well.
          </span>
        ) : (
          ""
        )}
      </Alert>
      <PlayerActionButtons>
        <Button
          variant="contained"
          onClick={() => setState(new_(ChooseActionState)(state))}
        >
          Continue
        </Button>
        <BattleHistory {...state} />
      </PlayerActionButtons>
    </SidePanelContainer>
  );
};
