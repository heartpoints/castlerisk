import { Alert, Button, Slider } from "@mui/material";
import { PropsWithSetState } from "../../../../utils/react/PropsWithSetState";
import { currentPlayerName } from "../../../players/currentPlayerName";
import { SideNote } from "../../chooseAction/SideNote";
import { ActionText } from "../../common/ActionText";
import { PlayerActionButtons } from "../../common/PlayerActionButtons";
import { PlayerHeading } from "../../common/PlayerHeading";
import { SidePanelContainer } from "../../common/SidePanelContainer";
import { BattleHistory } from "../general/BattleHistory";
import { defendingPlayer } from "../general/defendingPlayer";
import { maxMovablePiecesForLandBattle } from "../general/maxMovablePiecesForLandBattle";
import { AttackerWonState } from "./AttackerWonState";
import { toChooseStateFromAttackerWon } from "./toChooseStateFromAttackerWon";
import { Battleship } from "../boardAdmiralShip/Battleship";

type SidePanelProps = PropsWithSetState<AttackerWonState>;
export const SidePanel = ({ setState, ...state }: SidePanelProps) => {
  const {
    attackTargetTerritory,
    attackDiceCount,
    players,
    attackOriginTerritory,
    numPiecesToMove,
    wasAdmiralActive: wasWonWithAdmiral,
  } = state;
  const max = maxMovablePiecesForLandBattle(players, attackOriginTerritory);
  return (
    <SidePanelContainer>
      <PlayerHeading {...state} />
      <Alert variant="filled" severity="success">
        {currentPlayerName(state)} defeats {defendingPlayer(state).name} in{" "}
        {attackTargetTerritory.name} from {attackOriginTerritory.name}!
      </Alert>
      {wasWonWithAdmiral ? (
        <p>
          <ActionText>
            Offboard {numPiecesToMove}{" "}
            {numPiecesToMove == 1 ? "army" : "armies"}
          </ActionText>{" "}
          to occupy {attackTargetTerritory.name}
        </p>
      ) : (
        <p>
          <ActionText>Choose # Armies</ActionText> to occupy{" "}
          {attackTargetTerritory.name}
        </p>
      )}
      <PlayerActionButtons>
        {!wasWonWithAdmiral && (
          <p style={{ textAlign: "center" }}>
            # Armies to Move:
            <Slider
              style={{ width: "75%" }}
              size="small"
              defaultValue={70}
              aria-label="Small"
              min={attackDiceCount}
              max={max}
              valueLabelDisplay="on"
              onChange={(_, newValue) =>
                setState({ ...state, numPiecesToMove: Number(newValue) })
              }
              marks={[
                { value: attackDiceCount, label: attackDiceCount },
                { value: max, label: max },
              ]}
            />
          </p>
        )}
        <Button
          variant="contained"
          onClick={() =>
            setState(
              toChooseStateFromAttackerWon(
                state,
                numPiecesToMove,
                wasWonWithAdmiral
              )
            )
          }
        >
          Move {numPiecesToMove} Armies
        </Button>{" "}
        {!wasWonWithAdmiral && (
          <Button
            variant="outlined"
            onClick={() =>
              setState(
                toChooseStateFromAttackerWon(
                  state,
                  maxMovablePiecesForLandBattle(players, attackOriginTerritory),
                  wasWonWithAdmiral
                )
              )
            }
          >
            Move Maximum of{" "}
            {maxMovablePiecesForLandBattle(players, attackOriginTerritory)}
          </Button>
        )}
        <BattleHistory {...state} />
      </PlayerActionButtons>
      {wasWonWithAdmiral && <Battleship {...state} />}
      {wasWonWithAdmiral ? (
        <SideNote>
          NOTE: When victorious with an Admiral, all surviving armies on the
          ship must be moved to the conquered territory.
        </SideNote>
      ) : (
        <SideNote>
          NOTE: Moved armies must be at least the number of dice rolled on the
          winning roll ({attackDiceCount} in this case), and at most one fewer
          than the number of remaining armies on the attacking territory.
        </SideNote>
      )}
    </SidePanelContainer>
  );
};
