import { z } from "zod";


export const HasNumPiecesToMove = z.object({ numPiecesToMove: z.number() });
export type HasNumPiecesToMove = z.infer<typeof HasNumPiecesToMove>;
