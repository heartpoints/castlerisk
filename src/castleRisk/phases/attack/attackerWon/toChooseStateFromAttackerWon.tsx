import { new_ } from "@utils/zod/new_";
import { currentPlayerFromState } from "../../../players/currentPlayerFromState";
import { playerEquals } from "../../../players/playerEquals";
import { ChooseActionState } from "../../chooseAction/ChooseActionState";
import { AttackerWonState } from "./AttackerWonState";
import { movePieces } from "./movePieces";
import { offboardPiecesFromAdmiralShip } from "castleRisk/cards/admiral/offboardPiecesFromAdmiralShip";

export const toChooseStateFromAttackerWon = (
  state: AttackerWonState,
  numPiecesToMove: number,
  wasWonWithAdmiral: boolean
) =>
  new_(ChooseActionState)(
    wasWonWithAdmiral
      ? offboardPiecesFromAdmiralShip(state)
      : {
          ...state,
          players: state.players.map((player) =>
            playerEquals(player)(currentPlayerFromState(state))
              ? {
                  ...player,
                  pieces: movePieces(
                    player.pieces,
                    state.attackOriginTerritory,
                    state.attackTargetTerritory,
                    numPiecesToMove
                  ),
                }
              : player
          ),
        }
  );


