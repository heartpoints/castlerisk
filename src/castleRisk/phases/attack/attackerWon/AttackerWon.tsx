import { doNothing } from "../../../../utils/axioms/doNothing";
import { PropsWithSetState } from "../../../../utils/react/PropsWithSetState";
import { GameCanvas } from "../../../gameCanvas/gameCanvas";
import { GameGrid } from "../../common/GameGrid";
import { onMouseMove } from "../../common/onMouseMove";
import { CurrentPlayerCards } from "../../../cards/CurrentPlayerCards";
import { drawBoard } from "../general/drawBoard";
import { AttackerWonState } from "./AttackerWonState";
import { SidePanel } from "./SidePanel";

export const AttackerWon = ({
  setState,
  ...state
}: PropsWithSetState<AttackerWonState>) => (
  <GameGrid
    gameCanvas={
      <GameCanvas
        drawGame={drawBoard(state)}
        onMouseMove={(e) => setState(onMouseMove(state)(e))}
        onMouseUp={doNothing}
      />
    }
    sidePanel={<SidePanel {...{ setState, ...state }} />}
    cards={<CurrentPlayerCards {...{setState, ...state}} />}
  />
);
