import { isType } from "@utils/zod/isType";
import { Pieces } from "../../../pieces/Pieces";
import { PlacedInTerritory } from "../../../pieces/PlacedInTerritory";
import { Territory } from "../../../territories/Territory";

export const movePieces = (
  pieces: Pieces,
  fromTerritory: Territory,
  toTerritory: Territory,
  num: number
) => pieces.reduce(
  ({ piecesSoFar, numRemaining }, currentPiece) => numRemaining === 0
    ? { piecesSoFar: [...piecesSoFar, currentPiece], numRemaining }
    : isType(PlacedInTerritory)(currentPiece) &&
      currentPiece.territoryName === fromTerritory.name
      ? {
        piecesSoFar: [
          ...piecesSoFar,
          { ...currentPiece, territoryName: toTerritory.name },
        ],
        numRemaining: numRemaining - 1,
      }
      : { piecesSoFar: [...piecesSoFar, currentPiece], numRemaining },
  { piecesSoFar: [] as Pieces, numRemaining: num }
).piecesSoFar;
