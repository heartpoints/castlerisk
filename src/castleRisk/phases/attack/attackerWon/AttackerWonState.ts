import { AttackReadyState } from "../general/AttackReadyState";

import { HasURI } from "@utils/zod/HasURI";
import { z } from "zod";

import { HasNumPiecesToMove } from "./HasNumPiecesToMove";
import { HasWasAdmiralActive } from "../general/HasWasAdmiralActive";

export const AttackerWonState = z.object({
  ...HasURI("AttackerWonState").shape,
  ...AttackReadyState.shape,
  ...HasNumPiecesToMove.shape,
  ...HasWasAdmiralActive.shape,
})

export type AttackerWonState = z.infer<typeof AttackerWonState>;
