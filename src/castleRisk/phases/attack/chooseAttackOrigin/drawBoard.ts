import { CanvasEffect } from "../../../../utils/canvas/CanvasEffect";
import { drawLayers } from "../../common/drawLayers";
import { activeTerritories } from "./activeTerritories";
import { ChooseAttackOriginState } from "./ChooseAttackOriginState";

export const drawBoard = (state: ChooseAttackOriginState): CanvasEffect =>
  drawLayers({
    activeTerritories: activeTerritories(state),
    shouldDrawHighlight: true,
    state,
  });
