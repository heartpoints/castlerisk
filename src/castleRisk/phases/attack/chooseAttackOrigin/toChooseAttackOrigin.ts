import { new_ } from "@utils/zod/new_";
import { pipe } from "fp-ts/lib/function";
import { ChooseActionState } from "../../chooseAction/ChooseActionState";
import { ChooseAttackOriginState } from "./ChooseAttackOriginState";

export const toChooseAttackOrigin = (state: ChooseActionState) =>
  pipe(state, new_(ChooseAttackOriginState));
