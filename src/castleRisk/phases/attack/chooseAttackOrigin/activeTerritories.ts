import { CastleRiskPlayState } from "../../common/CastleRiskPlayState"
import { attackOriginTerritories } from "../general/attackOriginTerritories"

export const activeTerritories = 
    (state:CastleRiskPlayState) =>
    attackOriginTerritories(state)
