import { Button } from "@mui/material";
import { new_ } from "@utils/zod/new_";
import { PropsWithSetState } from "../../../../utils/react/PropsWithSetState";
import { ChooseActionState } from "../../chooseAction/ChooseActionState";
import { ActionText } from "../../common/ActionText";
import { PlayerActionButtons } from "../../common/PlayerActionButtons";
import { PlayerHeading } from "../../common/PlayerHeading";
import { ReserveArmies } from "../../common/ReserveArmies";
import { SidePanelContainer } from "../../common/SidePanelContainer";
import { SideNoteForAttack } from "../general/SideNoteForAttack";
import { ChooseAttackOriginState } from "./ChooseAttackOriginState";
import { deactivateBattleCards } from "castleRisk/cards/deactivateBattleCards";
import { attackRulesOverview } from "./attackRulesOverview";

type SidePanelProps = PropsWithSetState<ChooseAttackOriginState>;
export const SidePanel = ({ setState, ...state }: SidePanelProps) => (
  <SidePanelContainer>
    <PlayerHeading {...state} />
    <p>
      <ActionText>Attack: Choose Origin</ActionText> - {attackRulesOverview}{" "}
      <small>(see NOTEs below for details)</small>
    </p>
    <PlayerActionButtons>
      <Button
        variant="outlined"
        onClick={() =>
          setState(new_(ChooseActionState)(deactivateBattleCards(state)))
        }
      >
        Cancel
      </Button>
    </PlayerActionButtons>
    <ReserveArmies {...state} />
    <SideNoteForAttack />
  </SidePanelContainer>
);
