import { PropsWithSetState } from "../../../../utils/react/PropsWithSetState";
import { CardContainer } from "../../../cards/CardContainer";
import { GameCanvas } from "../../../gameCanvas/gameCanvas";
import { currentlyHoveredActiveTerritory } from "../../common/currentlyHoveredActiveTerritory";
import { GameGrid } from "../../common/GameGrid";
import { onMouseMove } from "../../common/onMouseMove";
import { toChooseAttackTarget } from "../chooseAttackTarget/toChooseAttackTarget";
import { CurrentPlayerCards } from "../../../cards/CurrentPlayerCards";
import { activeTerritories } from "./activeTerritories";
import { ChooseAttackOriginState } from "./ChooseAttackOriginState";
import { drawBoard } from "./drawBoard";
import { SidePanel } from "./SidePanel";

export const ChooseAttackOrigin = ({
  setState,
  ...state
}: PropsWithSetState<ChooseAttackOriginState>) => (
  <GameGrid
    cards={<CurrentPlayerCards {...{setState, ...state}} />}
    gameCanvas={
      <GameCanvas
        drawGame={drawBoard(state)}
        onMouseMove={(e) => setState(onMouseMove(state)(e))}
        onMouseUp={() =>
          currentlyHoveredActiveTerritory(state.mousePoint)(
            activeTerritories(state)
          ).map((attackSourceTerritory) =>
            setState(toChooseAttackTarget(state, attackSourceTerritory))
          )
        }
      />
    }
    sidePanel={<SidePanel setState={setState} {...state} />}
  />
);
