import { CastleRiskPlayState } from "../../common/CastleRiskPlayState";

import { HasURI } from "@utils/zod/HasURI";
import { z } from "zod";

export const ChooseAttackOriginState = z.object({
  ...HasURI("ChooseAttackOriginState").shape,
  ...CastleRiskPlayState.shape,
});
export type ChooseAttackOriginState = z.infer<typeof ChooseAttackOriginState>;
