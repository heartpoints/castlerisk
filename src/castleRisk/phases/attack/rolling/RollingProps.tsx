import { PropsWithSetState } from "@utils/react/PropsWithSetState";
import { RollingState } from "./RollingState";

export type RollingProps = PropsWithSetState<RollingState>;
