import { doNothing } from "@utils/axioms/doNothing";
import { Dice } from "castleRisk/dice/Dice";
import { GameCanvas } from "castleRisk/gameCanvas/gameCanvas";
import { SideNote } from "castleRisk/phases/chooseAction/SideNote";
import { ActionText } from "castleRisk/phases/common/ActionText";
import { GameGrid } from "castleRisk/phases/common/GameGrid";
import { PlayerHeading } from "castleRisk/phases/common/PlayerHeading";
import { ReserveArmies } from "castleRisk/phases/common/ReserveArmies";
import { FC } from "react";
import { CurrentPlayerCards } from "../../../cards/CurrentPlayerCards";
import { drawBoard } from "../general/drawBoard";
import { SidePanelTemplate } from "../general/SidePanelTemplate";
import { RollingProps } from "./RollingProps";
import { numPiecesInTerritory } from "castleRisk/pieces/numPiecesInTerritory";

export const Rolling: FC<RollingProps> = ({ setState, ...state }) => {
  const numArmiesInAttackingTerritory = numPiecesInTerritory(state.players)(state.attackOriginTerritory)
  const numArmiesInDefendingTerritory = numPiecesInTerritory(state.players)(state.attackTargetTerritory)
  return (
    <GameGrid
      gameCanvas={
        <GameCanvas
          drawGame={drawBoard(state)}
          onMouseMove={doNothing}
          onMouseUp={doNothing}
        />
      }
      sidePanel={
        <SidePanelTemplate
          actionTextContent={
            <>
              <p>
                <ActionText>Rolling Now</ActionText> to decide the battle of{" "}
                <strong>{state.attackOriginTerritory.name}</strong> vs.{" "}
                <strong>{state.attackTargetTerritory.name}</strong>!
              </p>
              <p><strong>{state.attackOriginTerritory.name}</strong>: {numArmiesInAttackingTerritory} {numArmiesInAttackingTerritory !== 1 ? "armies" : "army"}</p>
              <p><strong>{state.attackTargetTerritory.name}</strong>: {numArmiesInDefendingTerritory} {numArmiesInDefendingTerritory !== 1 ? "armies" : "army"}</p>
            </>
          }
          playerActionButtons={""}
          reserveArmies={<ReserveArmies {...state} />}
          playerHeading={<PlayerHeading {...state} />}
          sideNote={
            <SideNote>
              NOTE: Defender wins ties.
            </SideNote>
          }
        />
      }
      dice={<Dice {...{ setState, ...state }} />}
      cards={<CurrentPlayerCards {...{ setState, ...state }} />}
    />
  );
};
