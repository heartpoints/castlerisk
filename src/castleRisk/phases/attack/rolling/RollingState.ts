
import { HasURI } from "@utils/zod/HasURI";
import { z } from "zod";
import { AttackReadyState } from "../general/AttackReadyState";
import { HasDiceRolls } from "castleRisk/dice/HasDiceRolls";

export const RollingState = z.object({
  ...HasURI("RollingState").shape,
  ...AttackReadyState.shape,
  ...HasDiceRolls.shape,
});
export type RollingState = z.infer<typeof RollingState>;
