import { AttackReadyState } from "./AttackReadyState";
import { maxAttackPiecesForState } from "./maxAttackPiecesForState";
import { maxDefensePiecesForState } from "./maxDefensePiecesForState";

export const constrainRollingDiceCount = (state: AttackReadyState) => {
  const {
    attackDiceCount, defenseDiceCount,
  } = state;
  const maxAttackPieces = maxAttackPiecesForState(state);
  const maxDefensePieces = maxDefensePiecesForState(state);
  return {
    ...state,
    attackDiceCount: attackDiceCount > maxAttackPieces ? maxAttackPieces : attackDiceCount,
    defenseDiceCount: defenseDiceCount > maxDefensePieces ? maxDefensePieces : defenseDiceCount,
  };
};
