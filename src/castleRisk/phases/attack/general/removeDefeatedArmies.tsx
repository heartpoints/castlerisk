import { last } from "@utils/arrays/last";
import { diceRollSummaryFromDiceRoll } from "castleRisk/dice/diceRollSummaryFromDiceRoll";
import { piecesAfterRemovingDefeated } from "castleRisk/pieces/piecesAfterRemovingDefeated";
import { currentPlayerFromState } from "castleRisk/players/currentPlayerFromState";
import { playerEquals } from "castleRisk/players/playerEquals";
import { playerForTerritory } from "castleRisk/territories/territoryOwner";
import { AttackReadyState } from "./AttackReadyState";
import { isAdmiralActive } from "castleRisk/cards/admiral/isAdmiralActive";

export const removeDefeatedArmies = (state: AttackReadyState) => {
  const { attackTargetTerritory, attackOriginTerritory, players, diceRolls } = state;
  const defendingPlayer = playerForTerritory(players)(attackTargetTerritory)!;
  const currentPlayer = currentPlayerFromState(state);
  const latestDiceRoll = last(diceRolls);
  const summary = diceRollSummaryFromDiceRoll(latestDiceRoll);
  return {
    ...state,
    players: players.map((player) => ({
      ...player,
      ...(playerEquals(player)(currentPlayer)
        ? {
          pieces: piecesAfterRemovingDefeated(player.pieces)(
            attackOriginTerritory
          )(summary.armiesLost.attacker)(isAdmiralActive(state)),
        }
        : playerEquals(player)(defendingPlayer)
          ? {
            pieces: piecesAfterRemovingDefeated(player.pieces)(
              attackTargetTerritory
            )(summary.armiesLost.defender)(false),
          }
          : {}),
    })),
  };
};
