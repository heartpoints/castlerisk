export type AttackerAndDefenderNumbers = {
  attacker: number;
  defender: number;
};
