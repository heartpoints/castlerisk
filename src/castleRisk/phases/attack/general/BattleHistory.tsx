import { Button } from "@mui/material";
import { DiceGrid } from "castleRisk/dice/DiceGrid";
import { DiceRollGridHeadings } from "castleRisk/dice/DiceRollGridHeadings";
import { SingleDiceRollSummary } from "castleRisk/dice/SingleDiceRollSummary";
import { FC, Fragment, useState } from "react";
import { reverse } from "../../../../utils/arrays/reverse";
import { AttackReadyState } from "./AttackReadyState";
import { BattleHistoryDialog } from "./BattleHistoryDialog";

export const BattleHistory: FC<AttackReadyState> = ({ ...state }) => {
  const { diceRolls } = state;
  const [open, setOpen] = useState<boolean>(false);
  return (
    <>
      <Button variant="outlined" onClick={() => setOpen(true)}>
        View Battle History
      </Button>
      <BattleHistoryDialog {...{ open, onClose: () => setOpen(false) }}>
        <DiceGrid>
          <DiceRollGridHeadings />
          {reverse(diceRolls).map((currentDiceRoll, i) => (
            <Fragment key={i}>
              <div style={{ gridColumn: "span 4", backgroundColor: "rgba(0,0,0,0.1)", padding: "0.5em" }}>
                <strong>Roll {diceRolls.length - i}</strong>
              </div>
              <SingleDiceRollSummary shouldAnimate={false} {...{ currentDiceRoll, ...state }} />
            </Fragment>
          ))}
        </DiceGrid>
      </BattleHistoryDialog>
    </>
  );
};
