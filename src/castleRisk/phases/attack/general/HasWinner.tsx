import { Winner } from "./Winner";

export type HasWinner = { winner: Winner; };
