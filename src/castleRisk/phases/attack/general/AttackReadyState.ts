import { CastleRiskPlayState } from "../../common/CastleRiskPlayState";
import { HasAttackDiceCount } from "../../../dice/HasAttackDiceCount";
import { HasAttackOriginTerritory } from "./HasAttackOriginTerritory";
import { HasAttackTargetTerritory } from "./HasAttackTargetTerritory";
import { HasDefenseDiceCount } from "../../../dice/HasDefenseDiceCount";
import { HasDiceRolls } from "../../../dice/HasDiceRolls";

import { z } from "zod";

export const AttackReadyState = z.object({
  ...CastleRiskPlayState.shape,
  ...HasAttackDiceCount.shape,
  ...HasDefenseDiceCount.shape,
  ...HasDiceRolls.shape,
  ...HasAttackOriginTerritory.shape,
  ...HasAttackTargetTerritory.shape,
});

export type AttackReadyState = z.infer<typeof AttackReadyState>;
