import { numPiecesInTerritory } from "castleRisk/pieces/numPiecesInTerritory";
import { Players } from "castleRisk/players/Players";
import { Territory } from "castleRisk/territories/Territory";


export const maxMovablePiecesForLandBattle = (
  players: Players,
  attackOriginTerritory: Territory
) => numPiecesInTerritory(players)(attackOriginTerritory) - 1;
