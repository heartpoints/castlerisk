import { Territory } from "../../../territories/Territory";

import { z } from "zod";

export const HasAttackTargetTerritory = z.object({ attackTargetTerritory: Territory });
export type HasAttackTargetTerritory = z.infer<typeof HasAttackTargetTerritory>;
