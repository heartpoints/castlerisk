import { CastleRiskPlayState } from "../../common/CastleRiskPlayState";
import { drawLayers } from "../../common/drawLayers";
import { HasAttackOriginTerritory } from "./HasAttackOriginTerritory";
import { HasAttackTargetTerritory } from "./HasAttackTargetTerritory";
import { activeTerritories } from "./activeTerritories";
import { CanvasEffect } from "../../../../utils/canvas/CanvasEffect";

export const drawBoard = (
  state: CastleRiskPlayState &
    HasAttackOriginTerritory &
    HasAttackTargetTerritory
): CanvasEffect =>
  drawLayers({
    activeTerritories: activeTerritories(state),
    shouldDrawHighlight: false,
    state,
  });
