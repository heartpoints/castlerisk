import { attacker } from "./attacker";
import { defender } from "./defender";

export type Winner = typeof attacker | typeof defender;
