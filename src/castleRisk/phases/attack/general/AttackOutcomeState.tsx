import { z } from "zod";
import { PlayerDefeatedState } from "../../playerDefeated/PlayerDefeatedState";
import { AttackerWonState } from "../attackerWon/AttackerWonState";
import { BattleUndecidedState } from "../battleUndecided/BattleUndecidedState";
import { DefenderWonState } from "../defenderWon/DefenderWonState";

export const AttackOutcomeState = z.discriminatedUnion("URI", [
  AttackerWonState,
  BattleUndecidedState,
  DefenderWonState,
  PlayerDefeatedState,
]);
export type AttackOutcomeState = z.infer<typeof AttackOutcomeState>;
