import { min } from "../../../../utils/math/min";
import { numPiecesInTerritory } from "../../../pieces/numPiecesInTerritory";
import { HasPlayers } from "../../common/HasPlayers";
import { HasAttackTargetTerritory } from "./HasAttackTargetTerritory";

export const maxDefensePiecesForState = ({
  players,
  attackTargetTerritory,
}: HasPlayers & HasAttackTargetTerritory) =>
  min(numPiecesInTerritory(players)(attackTargetTerritory), 2);
