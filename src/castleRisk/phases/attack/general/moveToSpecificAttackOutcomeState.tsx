import { new_ } from "@utils/zod/new_";
import { deactivateBattleCards } from "castleRisk/cards/deactivateBattleCards";
import { toPlayerDefeatedState } from "castleRisk/phases/playerDefeated/toPlayerDefeatedState";
import { numPiecesInTerritory } from "castleRisk/pieces/numPiecesInTerritory";
import { Player } from "castleRisk/players/Player";
import { AttackerWonState } from "../attackerWon/AttackerWonState";
import { BattleUndecidedState } from "../battleUndecided/BattleUndecidedState";
import { DefenderWonState } from "../defenderWon/DefenderWonState";
import { AttackReadyState } from "./AttackReadyState";
import { throwError } from "@utils/debugging/throwError";
import { maxAttackPiecesForState } from "./maxAttackPiecesForState";
import { isAdmiralActive } from "castleRisk/cards/admiral/isAdmiralActive";
import { numPiecesOnBoat } from "castleRisk/cards/admiral/numPiecesOnBoat";
import { produce } from "immer";
import { currentPlayerFromState } from "castleRisk/players/currentPlayerFromState";
import { isType } from "@utils/zod/isType";
import { Admiral } from "castleRisk/cards/admiral/Admiral";
import { not } from "@utils/predicates/not";
import { CastleRiskPlayState } from "castleRisk/phases/common/CastleRiskPlayState";
import { admiralCard } from "castleRisk/cards/admiral/admiralCard";
import { discardActiveAdmiral } from "castleRisk/cards/admiral/discardActiveAdmiral";

export const moveToSpecificAttackOutcomeState =
  (defendingPlayer: Player) => (state: AttackReadyState) => {
    const {
      attackDiceCount,
      players,
      attackTargetTerritory,
      attackOriginTerritory,
    } = state;

    const hasAttackerWon =
      numPiecesInTerritory(players)(attackTargetTerritory) === 0;

    const canAttack =
      maxAttackPiecesForState({
        players,
        attackOriginTerritory,
        attackTargetTerritory,
      }) > 0;

    const wasPlayerCastleDefeated =
      hasAttackerWon &&
      defendingPlayer.castles.some(
        (c) => c.territoryName === attackTargetTerritory.name
      );

    return hasAttackerWon
      ? wasPlayerCastleDefeated
        ? players.length === 2
          ? throwError(new Error(`Game Over not implemented`))
          : toPlayerDefeatedState(state, attackDiceCount)
        : new_(AttackerWonState)({
            ...deactivateBattleCards(state),
            wasAdmiralActive: isAdmiralActive(state),
            numPiecesToMove: isAdmiralActive(state)
              ? numPiecesOnBoat(players)
              : attackDiceCount,
          })
      : canAttack
      ? new_(BattleUndecidedState)(state)
      : new_(DefenderWonState)({
          ...deactivateBattleCards(discardActiveAdmiral(state)),
          wasAdmiralActive: isAdmiralActive(state),
        });
  };
