import { Territory } from "../../../territories/Territory";

import { z } from "zod";

export const HasAttackOriginTerritory = z.object({ attackOriginTerritory: Territory });
export type HasAttackOriginTerritory = z.infer<typeof HasAttackOriginTerritory>;
