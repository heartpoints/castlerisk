import { HasAttackOriginTerritory } from "./HasAttackOriginTerritory";
import { HasAttackTargetTerritory } from "./HasAttackTargetTerritory";

export const activeTerritories = (
  state: HasAttackOriginTerritory & HasAttackTargetTerritory
) => [state.attackOriginTerritory, state.attackTargetTerritory];
