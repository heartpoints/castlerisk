import { SideNote } from "../../chooseAction/SideNote";

export const SideNoteForAttack = () => (
  <SideNote>
    <p>
      NOTE: If you play an admiral card, then you may also attack any
      sea-bordering territory from one of your sea-bordering territories.
    </p>
    <p>
      NOTE: Territories connected by dotted lines are considered adjacent
      without the use of an admiral.
    </p>
  </SideNote>
);
