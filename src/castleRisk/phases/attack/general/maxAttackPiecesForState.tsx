import { isAdmiralActive } from "castleRisk/cards/admiral/isAdmiralActive";
import { numPiecesOnBoat } from "castleRisk/cards/admiral/numPiecesOnBoat";
import { min } from "../../../../utils/math/min";
import { territoryHasCastle } from "../../../castles/territoryHasCastle";
import { HasPlayers } from "../../common/HasPlayers";
import { HasAttackOriginTerritory } from "./HasAttackOriginTerritory";
import { HasAttackTargetTerritory } from "./HasAttackTargetTerritory";
import { maxMovablePiecesForLandBattle } from "./maxMovablePiecesForLandBattle";

export const maxAttackPiecesForState = ({
  players,
  attackOriginTerritory,
  attackTargetTerritory,
}: HasPlayers & HasAttackOriginTerritory & HasAttackTargetTerritory) =>
  min(
    isAdmiralActive({ players })
      ? numPiecesOnBoat(players)
      : maxMovablePiecesForLandBattle(players, attackOriginTerritory),
    territoryHasCastle(players)(attackTargetTerritory) ? 2 : 3
  );
