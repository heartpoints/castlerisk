import { last } from "@utils/arrays/last";
import { not } from "@utils/predicates/not";
import { activeGenerals } from "castleRisk/cards/general/activeGenerals";
import { isActiveGeneral } from "castleRisk/cards/general/isActiveGeneral";
import { diceRollSummaryFromDiceRoll } from "castleRisk/dice/diceRollSummaryFromDiceRoll";
import { currentPlayerFromState } from "castleRisk/players/currentPlayerFromState";
import { produce } from "immer";
import { AttackReadyState } from "./AttackReadyState";

export const removeDefeatedGenerals = (state: AttackReadyState) => {
  const { diceRolls } = state;
  const latestDiceRoll = last(diceRolls);
  const summary = diceRollSummaryFromDiceRoll(latestDiceRoll);
  return summary.highest.winner == "defender" && latestDiceRoll.numGenerals > 0
    ? produce(state, draft => {
      const currentPlayer = currentPlayerFromState(draft);
      const cardsToDiscard = activeGenerals(draft).map(c => ({ ...c, inBattle: false }));
      draft.discardPile.push(...cardsToDiscard);
      currentPlayer.cards = currentPlayer.cards.filter(not(isActiveGeneral));
    })
    : state;
};
