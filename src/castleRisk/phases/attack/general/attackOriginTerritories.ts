import { filter } from "fp-ts/lib/Array";
import { pipe } from "fp-ts/lib/function";
import { currentPlayerFromState } from "../../../players/currentPlayerFromState";
import { CastleRiskPlayState } from "../../common/CastleRiskPlayState";
import { hasAttackableTerritory } from "../../common/hasAttackableTerritory";
import { territoriesWith2OrMorePieces } from "../../common/territoriesWith2OrMorePieces";

export const attackOriginTerritories = (state: CastleRiskPlayState) => pipe(
    state,
    currentPlayerFromState,
    currentPlayer => pipe(
        currentPlayer,
        territoriesWith2OrMorePieces(state.players),
        filter(
            hasAttackableTerritory(state.players)(currentPlayer)
        )
    )
);
