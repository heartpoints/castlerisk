import { new_ } from "@utils/zod/new_";
import { randomDiceRolls } from "castleRisk/dice/randomDiceRolls";
import { playerIndexForTerritory } from "castleRisk/territories/playerIndexForTerritory";
import { RollingState } from "../rolling/RollingState";
import { AttackReadyState } from "./AttackReadyState";
import { activeGenerals } from "castleRisk/cards/general/activeGenerals";

export const performDiceRoll = (state: AttackReadyState): RollingState => {
  const { attackDiceCount, defenseDiceCount, players, attackTargetTerritory, currentPlayerIndex, diceRolls } = state;
  const newDiceRoll = {
    attacker: {
      dice: randomDiceRolls(attackDiceCount),
      playerIndex: currentPlayerIndex,
    },
    defender: {
      dice: randomDiceRolls(defenseDiceCount),
      playerIndex: playerIndexForTerritory(players)(attackTargetTerritory),
    },
    numGenerals: activeGenerals(state).length,
    numMarshalls: 0,
    isAdmiral: false,
  };
  return new_(RollingState)({
    ...state,
    diceRolls: [...diceRolls, newDiceRoll],
    hasAttacked: true,
  });
};
