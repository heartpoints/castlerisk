import { pipe } from "fp-ts/lib/function";
import { AttackReadyState } from "./AttackReadyState";
import { constrainRollingDiceCount } from "./constrainRollingDiceCount";
import { moveToSpecificAttackOutcomeState } from "./moveToSpecificAttackOutcomeState";
import { removeDefeatedArmies } from "./removeDefeatedArmies";
import { removeDefeatedGenerals } from "./removeDefeatedGenerals";
import { playerForTerritory } from "castleRisk/territories/territoryOwner";

export const applyAttackOutcome = (state: AttackReadyState) =>
  pipe(
    state,
    removeDefeatedArmies,
    constrainRollingDiceCount,
    removeDefeatedGenerals,
    moveToSpecificAttackOutcomeState(
      playerForTerritory(state.players)(state.attackTargetTerritory)!
    )
  );
