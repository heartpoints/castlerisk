import { z } from "zod";


export const HasWasAdmiralActive = z.object({ wasAdmiralActive: z.boolean() });
export type HasWasAdmiralActive = z.infer<typeof HasWasAdmiralActive>;
