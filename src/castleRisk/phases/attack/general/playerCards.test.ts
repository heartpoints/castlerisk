import { when } from "@utils/testing/when"
import { initialCards } from "../../../cards/initialCards"
import { PlayerCards } from "../../../cards/PlayerCards"

describe("PlayerCards.safeParse", () => {
    when("i send an empty array", () => {
        it("is valid", () => {
            expect(PlayerCards.safeParse([]).success).toBeTruthy()
        })
    })
    when("I send in the initial card deck", () => {
        it("is valid", () => {
            expect(PlayerCards.safeParse(initialCards).success).toBeTruthy();
        })
    })
    when("I send in a card with a URI that is not familiar", () => {
        it("is invalid", () => {
            expect(PlayerCards.safeParse([{URI: "NotACardType"}]).success).toBeFalsy();
        })
    })
})