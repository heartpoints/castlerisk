import { last } from "../../../../utils/arrays/last";
import { CastleRiskPlayState } from "../../common/CastleRiskPlayState";
import { HasDiceRolls } from "../../../dice/HasDiceRolls";

export const defendingPlayer = ({ players, diceRolls }: CastleRiskPlayState & HasDiceRolls) =>
  players[last(diceRolls).defender.playerIndex];
