import { FC, ReactNode } from "react";
import { SidePanelContainer } from "../../common/SidePanelContainer";

type SidePanelTemplateProps = {
  playerHeading: ReactNode;
  actionTextContent: ReactNode;
  playerActionButtons?: ReactNode;
  reserveArmies?: ReactNode;
  sideNote?: ReactNode;
};

export const SidePanelTemplate: FC<
  SidePanelTemplateProps
> = ({ playerHeading, actionTextContent, playerActionButtons, sideNote, reserveArmies }) => (
  <SidePanelContainer>
    {playerHeading}
    {actionTextContent}
    {playerActionButtons}
    {reserveArmies}
    {sideNote}
  </SidePanelContainer>
);
