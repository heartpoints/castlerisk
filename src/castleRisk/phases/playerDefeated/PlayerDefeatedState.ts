import { AttackReadyState } from "../attack/general/AttackReadyState";
import { HasDefeatedPlayer } from "./HasDefeatedPlayer";

import { z } from "zod";
import { HasURI } from "@utils/zod/HasURI";

export const PlayerDefeatedState = z.object({
  ...HasURI("PlayerDefeatedState").shape,
  ...HasDefeatedPlayer.shape,
  ...AttackReadyState.shape
}).strict()

export type PlayerDefeatedState = z.infer<typeof PlayerDefeatedState>;
