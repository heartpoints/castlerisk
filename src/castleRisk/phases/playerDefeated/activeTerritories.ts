import { not } from '../../../utils/predicates/not'
import { territories } from '../../map/territories'
import { isTerritoryOccupiedByPlayers } from '../../territories/isTerritoryOccupiedByPlayers'
import { CastleRiskPlayState } from '../common/CastleRiskPlayState'

export const activeTerritories = 
    ({players}:CastleRiskPlayState) => 
    territories().filter(not(isTerritoryOccupiedByPlayers(players)))