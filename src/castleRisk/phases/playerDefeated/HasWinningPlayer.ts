import { z } from "zod";
import { Player } from "../../players/Player";


export const HasWinningPlayer = z.object({ winningPlayer: Player });
export type HasWinningPlayer = z.infer<typeof HasWinningPlayer>;
