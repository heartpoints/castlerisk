import { Player } from "../../players/Player";

import { z } from "zod";

export const HasDefeatedPlayer = z.object({ defeatedPlayer: Player });
export type HasDefeatedPlayer = z.infer<typeof HasDefeatedPlayer>;
