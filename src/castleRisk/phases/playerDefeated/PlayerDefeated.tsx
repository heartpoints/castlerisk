import { PropsWithSetState } from "../../../utils/react/PropsWithSetState";
import { CardContainer } from "../../cards/CardContainer";
import { GameCanvas } from "../../gameCanvas/gameCanvas";
import { GameGrid } from "../common/GameGrid";
import { onMouseMove } from "../common/onMouseMove";
import { drawBoard } from "./drawBoard";
import { SidePanel } from "./SidePanel";
import { PlayerDefeatedState } from "./PlayerDefeatedState";
import { CurrentPlayerCards } from "../../cards/CurrentPlayerCards";

export const PlayerDefeated = ({
  setState,
  ...state
}: PropsWithSetState<PlayerDefeatedState>) => (
  <GameGrid
    gameCanvas={
      <GameCanvas
        drawGame={drawBoard(state)}
        onMouseMove={(e) => setState(onMouseMove(state)(e))}
        onMouseUp={alert}
      />
    }
    sidePanel={<SidePanel {...{ setState, ...state }} />}
    cards={<CurrentPlayerCards {...{setState, ...state}} />}
  />
);
