import { drawLayers } from "../common/drawLayers";
import { PlayerDefeatedState } from "./PlayerDefeatedState";
import { activeTerritories } from "./activeTerritories";
import { CanvasEffect } from "../../../utils/canvas/CanvasEffect";

export const drawBoard = (state: PlayerDefeatedState): CanvasEffect =>
  drawLayers({
    activeTerritories: activeTerritories(state),
    shouldDrawHighlight: true,
    state,
  });
