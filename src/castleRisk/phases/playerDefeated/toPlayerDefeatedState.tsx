import { not } from "../../../utils/predicates/not";
import { currentPlayerFromState } from "../../players/currentPlayerFromState";
import { playerEquals } from "../../players/playerEquals";
import { PlayerDefeatedState } from "./PlayerDefeatedState";
import { AttackReadyState } from "../attack/general/AttackReadyState";
import { defendingPlayer } from "../attack/general/defendingPlayer";
import { HasDiceRolls } from "../../dice/HasDiceRolls";
import { movePieces } from "../attack/attackerWon/movePieces";
import { new_ } from "@utils/zod/new_";
import { deactivateBattleCards } from "castleRisk/cards/deactivateBattleCards";
import { isType } from "@utils/zod/isType";
import { Diplomat } from "castleRisk/cards/diplomat/Diplomat";

export const toPlayerDefeatedState = (
  state: AttackReadyState & HasDiceRolls,
  numPiecesToMove: number
): PlayerDefeatedState => {
  const defeatedPlayer = defendingPlayer(state);
  const currentPlayer = currentPlayerFromState(state);
  const { attackOriginTerritory, attackTargetTerritory } = state;
  const updatedPlayers = state.players
    .filter(not(playerEquals(defeatedPlayer)))
    .map((player) =>
      playerEquals(player)(currentPlayer)
        ? {
            ...player,
            castles: [
              ...player.castles,
              ...defeatedPlayer.castles.map((castle) => ({
                ...castle,
                territoryName: undefined,
              })),
            ],
            hiddenArmyTerritoryNames: [
              ...player.hiddenArmyTerritoryNames,
              ...defeatedPlayer.hiddenArmyTerritoryNames,
            ],
            pieces: movePieces(
              player.pieces,
              attackOriginTerritory,
              attackTargetTerritory,
              numPiecesToMove
            ),
            cards: [...player.cards, ...defeatedPlayer.cards.map(c => isType(Diplomat)(c) ? new_(Diplomat)({isActive: false}) : c)]
          }
        : player
    );
  
  const currentPlayerIndex = updatedPlayers.findIndex(playerEquals(currentPlayer))
  return new_(PlayerDefeatedState)({
    ...deactivateBattleCards(state),
    currentPlayerIndex,
    defeatedPlayer,
    players: updatedPlayers,
  });
};
