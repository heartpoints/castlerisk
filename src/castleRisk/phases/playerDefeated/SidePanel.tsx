import { Alert, Button } from "@mui/material";
import { new_ } from "@utils/zod/new_";
import { PropsWithSetState } from "../../../utils/react/PropsWithSetState";
import { currentPlayerFromState } from "../../players/currentPlayerFromState";
import { unoccupiedTerritories } from "../../territories/unoccupiedTerritories";
import { BattleHistory } from "../attack/general/BattleHistory";
import { SidePanelTemplate } from "../attack/general/SidePanelTemplate";
import { ChooseActionState } from "../chooseAction/ChooseActionState";
import { ActionText } from "../common/ActionText";
import { PlayerActionButtons } from "../common/PlayerActionButtons";
import { PlayerHeading } from "../common/PlayerHeading";
import { toFillDefeatedOrChooseNextActionState } from "../fillDefeatedTerritories/toFillDefeatedOrChooseNextActionState";
import { PlayerDefeatedState } from "./PlayerDefeatedState";

export const SidePanel = ({
  setState,
  ...state
}: PropsWithSetState<PlayerDefeatedState>) => {
  const currentPlayer = currentPlayerFromState(state);
  const { defeatedPlayer } = state;
  const { castles, hiddenArmyTerritoryNames } = defeatedPlayer;
  const needToPlaceArmies = unoccupiedTerritories(state).length > 0;
  return (
    <SidePanelTemplate
      actionTextContent={
        <>
          <Alert severity="warning" variant="filled">
            {currentPlayer.name} has defeated {defeatedPlayer.name}!
          </Alert>
          <p>
            <ActionText>Congratulations!</ActionText> any territories occupied
            by the defeated player have been emptied.
          </p>
          <p>The spoils of this victory also include:</p>
          <ul style={{ marginLeft: "1em" }}>
            <li>
              <strong>{castles.length} castle piece(s)</strong> -{" "}
              {castles.map((c) => c.empireName).join(", ")}
            </li>
            <li>
              <strong>
                {hiddenArmyTerritoryNames.length} hidden army location(s)
              </strong>{" "}
            </li>
            <li>
              <strong>{defeatedPlayer.cards.length}</strong> card(s)
            </li>
          </ul>
        </>
      }
      playerActionButtons={
        <PlayerActionButtons>
          {needToPlaceArmies ? (
            <Button
              variant="contained"
              onClick={() =>
                setState(
                  toFillDefeatedOrChooseNextActionState(state, currentPlayer)
                )
              }
            >
              Fill Defeated Territories
            </Button>
          ) : (
            <Button variant="contained" onClick={() => new_(ChooseActionState)}>
              Continue
            </Button>
          )}
          <BattleHistory {...state} />
        </PlayerActionButtons>
      }
      playerHeading={<PlayerHeading {...state} />}
    />
  );
};
