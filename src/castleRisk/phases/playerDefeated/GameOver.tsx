import { Alert } from "@mui/material";
import { Player } from "../../players/Player";

export const GameOver = ({
  currentPlayer,
  defeatedPlayer,
}: {
  currentPlayer: Player;
  defeatedPlayer: Player;
}) => (
  <Alert severity="success" variant="filled">
    Game Over! {currentPlayer.name} has defeated {defeatedPlayer.name} and won
    the game!
  </Alert>
);
