import { Button, Tooltip } from "@mui/material";
import { FC } from "react";
import { attackRulesOverview } from "../attack/chooseAttackOrigin/attackRulesOverview";
import { attackOriginTerritories } from "../attack/general/attackOriginTerritories";
import { CastleRiskPlayState } from "../common/CastleRiskPlayState";
import { HasOnClick } from "../common/HasOnClick";

export const AttackButton: FC<CastleRiskPlayState & HasOnClick> = ({
  onClick,
  ...state
}) => {
  const numAttackOrigins = attackOriginTerritories(state).length;
  const disabled = numAttackOrigins === 0;
  return (
    <Tooltip title={attackRulesOverview} placement="left">
      <div
        style={{
          display: "inline-block",
          cursor: disabled ? "not-allowed" : "pointer",
        }}
      >
        <Button
          variant="contained"
          onClick={(e) => onClick()}
          disabled={disabled}
          style={{width: "100%"}}
        >
          Attack
        </Button>
      </div>
    </Tooltip>
  );
};
