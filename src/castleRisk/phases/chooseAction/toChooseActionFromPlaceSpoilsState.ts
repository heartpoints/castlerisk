import { new_ } from "@utils/zod/new_";
import { pipe } from "fp-ts/lib/function";
import { moveToNextPlayerAndDrawCards } from "../../players/moveToNextPlayerAndDrawCards";
import { PlaceSpoilsState } from "../placeSpoils/PlaceSpoilsState";
import { ChooseActionState } from "./ChooseActionState";

export const toChooseActionFromPlaceSpoilsState = (
  state: PlaceSpoilsState
): ChooseActionState =>
  pipe(state, new_(ChooseActionState), moveToNextPlayerAndDrawCards);
