import { new_ } from "@utils/zod/new_";
import { range } from "../../../utils/list/range";
import { PlacedInReserves } from "../../pieces/PlacedInReserves";
import { NumPlayers } from "../../players/NumPlayers";
import { numberOfReserveArmiesAfterSetup } from "../../players/numberOfReserveArmiesAfterSetup";
import { CastleRiskPlayState } from "../common/CastleRiskPlayState";

export const addRemainingReserves = (state: CastleRiskPlayState) => ({
  ...state,
  players: state.players.map((player) => ({
    ...player,
    pieces: [
      ...player.pieces,
      ...range(
        numberOfReserveArmiesAfterSetup(state.players.length as NumPlayers)
      ).map((_) => new_(PlacedInReserves)({ color: player.color })), //TODO: could strip off unneeded extra params inside _new (maybe warn?)
    ],
  })),
});
