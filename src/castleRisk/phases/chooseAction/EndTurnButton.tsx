import { Button, Tooltip } from "@mui/material";
import { FC } from "react";
import { currentPlayerFromState } from "../../players/currentPlayerFromState";
import { CastleRiskPlayState } from "../common/CastleRiskPlayState";
import { numberOfSpoilsForPlayer } from "../placeSpoils/numberOfSpoilsForPlayer";
import { spoilsIfControllingAllIndependentTerritories } from "../placeSpoils/spoilsIfControllingAllIndependentTerritories";
import { spoilsPerCastle } from "../placeSpoils/spoilsPerCastle";
import { spoilsPerFullyControlledEmpire } from "../placeSpoils/spoilsPerFullyControlledEmpire";
import { HasOnClick } from "../common/HasOnClick";

export const EndTurnButton: FC<CastleRiskPlayState & HasOnClick> = ({
  onClick, ...state
}) => {
  const numberOfSpoils = numberOfSpoilsForPlayer(state.players)(
    currentPlayerFromState(state)
  );
  return (
    <Tooltip
      placement="left"
      title={<>
        <p>Collect and fortify your territories with new armies.</p>
        <ul>
          <li>{spoilsPerCastle} armies for every castle piece you control</li>
          <li>
            {spoilsPerFullyControlledEmpire} armies for every empire you fully
            occupy
          </li>
          <li>
            {spoilsIfControllingAllIndependentTerritories} armies if you fully
            occupy all the independent territories
          </li>
        </ul>
      </>}
    >
      <div style={{ display: "inline-block" }}>
        <Button onClick={(e) => onClick()} variant="outlined" style={{width: "100%"}}>
          Collect {numberOfSpoils} Spoils + End Turn
        </Button>
      </div>
    </Tooltip>
  );
};
