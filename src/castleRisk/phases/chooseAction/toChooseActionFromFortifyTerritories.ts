import { new_ } from "@utils/zod/new_";
import { pipe } from "fp-ts/lib/function";
import { mergeProps } from "../../../utils/composition/mergeProps";
import { scramble } from "../../../utils/list/scramble";
import { moveToNextPlayerAndDrawCards } from "../../players/moveToNextPlayerAndDrawCards";
import { FortifyTerritoriesState } from "../fortifyTerritories/FortifyTerritoriesState";
import { ChooseActionState } from "./ChooseActionState";
import { addRemainingReserves } from "./addRemainingReserves";

export const toChooseActionFromFortifyTerritories = (
  state: FortifyTerritoriesState
): ChooseActionState =>
  pipe(
    state,
    mergeProps({ players: scramble(state.players) }),
    addRemainingReserves,
    new_(ChooseActionState),
    moveToNextPlayerAndDrawCards
  );
