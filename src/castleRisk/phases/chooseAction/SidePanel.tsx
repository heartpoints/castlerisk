import { VoidCallback } from "../../../utils/axioms/VoidCallback";
import { AttackButton } from "./AttackButton";
import { ChooseActionState } from "./ChooseActionState";
import { EndTurnButton } from "./EndTurnButton";
import { PlayerActionButtons } from "../common/PlayerActionButtons";
import { PlayerHeading } from "../common/PlayerHeading";
import { ReserveArmies } from "../common/ReserveArmies";
import { RevealHiddenArmiesButton } from "./RevealHiddenArmiesButton";
import { SideNote } from "./SideNote";
import { SidePanelContainer } from "../common/SidePanelContainer";
import { ActionText } from "../common/ActionText";

type SidePanelProps = {
  onClickSpoils: VoidCallback;
  state: ChooseActionState;
  onClickAttack: VoidCallback;
  onClickHiddenArmies: VoidCallback;
};
export const SidePanel = ({
  onClickSpoils,
  state,
  onClickAttack,
  onClickHiddenArmies,
}: SidePanelProps) => {
  return (
    <SidePanelContainer>
      <PlayerHeading {...state} />
      <p>
        <ActionText>Choose Action</ActionText> from the choices below:
      </p>
      <PlayerActionButtons>
        <AttackButton {...{ onClick: onClickAttack, ...state }} />
        <RevealHiddenArmiesButton
          {...{ onClick: onClickHiddenArmies, ...state }}
        />
        <EndTurnButton {...{ onClick: onClickSpoils, ...state }} />
      </PlayerActionButtons>
      <ReserveArmies {...state} />
      <SideNote>
        NOTE: Borders of fully-controlled empires are outlined in white
      </SideNote>
    </SidePanelContainer>
  );
};
