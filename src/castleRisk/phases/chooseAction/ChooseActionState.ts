import { HasURI } from "@utils/zod/HasURI";
import { z } from "zod";
import { CastleRiskPlayState } from "../common/CastleRiskPlayState";

export const ChooseActionState = z.object({
  ...HasURI("ChooseActionState").shape,
  ...CastleRiskPlayState.shape,
});
export type ChooseActionState = z.infer<typeof ChooseActionState>;
