import { new_ } from "@utils/zod/new_";
import { doNothing } from "../../../utils/axioms/doNothing";
import { PropsWithSetState } from "../../../utils/react/PropsWithSetState";
import { CardContainer } from "../../cards/CardContainer";
import { GameCanvas } from "../../gameCanvas/gameCanvas";
import { currentPlayerFromState } from "../../players/currentPlayerFromState";
import { toChooseAttackOrigin } from "../attack/chooseAttackOrigin/toChooseAttackOrigin";
import { GameGrid } from "../common/GameGrid";
import { onMouseMove } from "../common/onMouseMove";
import { RevealHiddenArmiesState } from "../hiddenArmies/reveal/RevealHiddenArmiesState";
import { numberOfSpoilsForPlayer } from "../placeSpoils/numberOfSpoilsForPlayer";
import { toPlaceSpoils } from "../placeSpoils/toPlaceSpoils";
import { ChooseActionState } from "./ChooseActionState";
import { drawBoard } from "./drawBoard";
import { SidePanel } from "./SidePanel";
import { CurrentPlayerCards } from "../../cards/CurrentPlayerCards";

export const ChooseAction = ({
  setState,
  ...state
}: PropsWithSetState<ChooseActionState>) => {
  const currentPlayer = currentPlayerFromState(state);
  const numberOfSpoils = numberOfSpoilsForPlayer(state.players)(currentPlayer);
  return (
    <GameGrid
      cards={<CurrentPlayerCards {...{setState, ...state}} />}
      sidePanel={
        <SidePanel
          onClickAttack={() => setState(toChooseAttackOrigin(state))}
          onClickSpoils={() => setState(toPlaceSpoils(numberOfSpoils)(state))}
          onClickHiddenArmies={() =>
            setState(new_(RevealHiddenArmiesState)(state))
          }
          {...{ numberOfSpoils, state }}
        />
      }
      gameCanvas={
        <GameCanvas
          drawGame={drawBoard(state)}
          onMouseMove={(e) => setState(onMouseMove(state)(e))}
          onMouseUp={doNothing}
        />
      }
    />
  );
};
