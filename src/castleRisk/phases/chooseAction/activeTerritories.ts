import { CastleRiskPlayState } from "../common/CastleRiskPlayState"
import { territories } from "../../map/territories"

export const activeTerritories = 
    (state:CastleRiskPlayState) =>
    territories()
