import { generalCard } from "castleRisk/cards/general/generalCard";
import { initialCards } from "../../cards/initialCards";
import { ChooseActionState } from "./ChooseActionState";
import { admiralCard } from "castleRisk/cards/admiral/admiralCard";

export const initialStateForChooseAction:ChooseActionState = {
   "hasAttacked": false,
   "discardPile": [],
    "players": [
       {
         "id": crypto.randomUUID(),
          "name": "Peepycakes",
          "color": [
             140.2535294117647,
             102.0035294117647,
             140.2535294117647,
             255.5
          ],
          "castles": [
             {
                "empireName": "British Empire",
                "territoryName": "Scotland"
             }
          ],
          "pieces": [
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Venice"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Venice"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Venice"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Venice"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Venice"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Venice"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Venice"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Venice"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Venice"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Venice"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Venice"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Venice"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Venice"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Venice"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Denmark"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Denmark"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Denmark"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Denmark"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Denmark"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Venice"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Venice"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Venice"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Venice"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Venice"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Venice"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Naples"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Switzerland"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Finland"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Denmark"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Norway"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Ireland"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Wales"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "London"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Yorkshire"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Scotland"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   140.2535294117647,
                   102.0035294117647,
                   140.2535294117647,
                   255.5
                ],
                "URI": "PlacedInReserves"
             }
          ],
          "hiddenArmyTerritoryNames": ["Trieste"],
          "cards": [generalCard, admiralCard],
       },
       {
         "id": crypto.randomUUID(),
          "name": "Mike",
          "color": [
             0,
             0,
             0,
             1
          ],
          "castles": [
             {
                "empireName": "Austrian Empire",
                "territoryName": "Vienna"
             }
          ],
          "pieces": [
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Vienna"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Vienna"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Vienna"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Vienna"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Vienna"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Vienna"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Vienna"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Vienna"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Vienna"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Vienna"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Vienna"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Vienna"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Vienna"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Vienna"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Prussia"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Prussia"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Prussia"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Prussia"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Prussia"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Rhine"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Rhine"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Rhine"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Rhine"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Rhine"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Macedonia"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Cypress"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Serbia"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Prussia"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Rhine"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Sweden"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Hungary"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Trieste"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Galecia"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Bohemia"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Vienna"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   0,
                   0,
                   0,
                   1
                ],
                "URI": "PlacedInReserves"
             }
          ],
          "hiddenArmyTerritoryNames": ["Hungary"],
          "cards": [],
       },
       {
         "id": crypto.randomUUID(),
          "name": "Tdizzlewizzle",
          "color": [
             255,
             255,
             0,
             255
          ],
          "castles": [
             {
                "empireName": "French Empire",
                "territoryName": "Burgundy"
             }
          ],
          "pieces": [
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Bavaria"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Bavaria"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Bavaria"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Bavaria"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Montenegro"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Montenegro"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Montenegro"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Montenegro"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Montenegro"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Berlin"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Berlin"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Berlin"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Berlin"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Berlin"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Berlin"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Berlin"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Berlin"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Berlin"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Berlin"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Bavaria"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Bavaria"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Bavaria"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Bavaria"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Bavaria"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Madrid"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Rome"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Montenegro"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Bavaria"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Berlin"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Brittany"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Gascony"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Marseille"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Netherlands"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Paris"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Burgundy"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   255,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             }
          ],
          "hiddenArmyTerritoryNames": ["Ukraine"],
          "cards": [],
       },
       {
         "id": crypto.randomUUID(),
          "name": "Tommy",
          "color": [
             255,
             0,
             0,
             255
          ],
          "castles": [
             {
                "empireName": "Russian Empire",
                "territoryName": "Poland"
             }
          ],
          "pieces": [
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Saxony"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Saxony"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Saxony"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Saxony"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Rumania"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Rumania"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Rumania"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Rumania"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Rumania"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Poland"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Poland"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Poland"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Poland"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Poland"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Saxony"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Saxony"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Saxony"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Saxony"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Saxony"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Poland"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Poland"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Poland"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Poland"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Poland"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Portugal"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Turkey"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Bulgaria"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Saxony"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Rumania"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Moscow"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "St. Petersburg"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Livonia"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Smolensk"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Ukraine"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInTerritory",
                "territoryName": "Poland"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             },
             {
                "color": [
                   255,
                   0,
                   0,
                   255
                ],
                "URI": "PlacedInReserves"
             }
          ],
          "hiddenArmyTerritoryNames": ["Galecia"],
          "cards": [],
       }
    ],
    "currentPlayerIndex": 0,
    "mousePoint": {
       "x": 168,
       "y": 2
    },
    reinforcementsCount: 3,
    cards: initialCards,
    "URI": "ChooseActionState"
 }