import { useRef } from "react";
import ReactDice, { ReactDiceRef } from "react-dice-complete";

const MyDiceApp = () => {
  const reactDice = useRef<ReactDiceRef>(null);
  const reactDice2 = useRef<ReactDiceRef>(null);

  const rollDone = (totalValue: number, values: number[]) => {
    console.log("individual die values array:", values);
    console.log("total dice value:", totalValue);
  };

  const rollAll = () => {
    reactDice.current?.rollAll([6, 2, 5]);
    reactDice2.current?.rollAll([3, 4]);
  };

  return (
    <>
      <button onClick={rollAll}>Roll</button>
      <ReactDice
        disableIndividual={true}
        defaultRoll={6}
        numDice={3}
        rollTime={1}
        ref={reactDice}
        rollDone={rollDone}
        dieCornerRadius={5}
        dotColor="black"
        faceColor="white"
      />
      <ReactDice
        disableIndividual={true}
        defaultRoll={6}
        numDice={2}
        ref={reactDice2}
        rollTime={1.5}
        rollDone={rollDone}
        dieCornerRadius={5}
        dotColor="white"
        faceColor="red"
      />
    </>
  );
};
