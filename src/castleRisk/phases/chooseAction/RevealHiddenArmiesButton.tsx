import { Button, Tooltip } from "@mui/material";
import { FC } from "react";
import { CastleRiskPlayState } from "../common/CastleRiskPlayState";
import { HasOnClick } from "../common/HasOnClick";
import { canRevealHiddenArmies } from "../hiddenArmies/reveal/canRevealHiddenArmies";

export const RevealHiddenArmiesButton: FC<CastleRiskPlayState & HasOnClick> = ({
  onClick,
  ...state
}) => {
  const disabled = !canRevealHiddenArmies(state);
  return (
    <Tooltip
      title="You may reveal your hidden armies as long as you occupy the territory
at the time you wish to reveal. The number of armies is equivalent to
the highest refinforcements card that was played, or 3 if no cards
have been played."
      placement="left"
    >
      <div
        style={{
          display: "inline-block",
          cursor: disabled ? "not-allowed" : "pointer",
        }}
      >
        <Button
          variant="outlined"
          onClick={() => onClick()}
          disabled={disabled}
          style={{width: "100%"}}
        >
          Reveal Hidden Armies
        </Button>
      </div>
    </Tooltip>
  );
};
