import { Alert } from "@mui/material";
import { FC, PropsWithChildren } from "react";

export const SideNote: FC<PropsWithChildren> = ({ children }) => (
  <div
    style={{
      position: "fixed",
      bottom: "1rem",
      fontSize: "small",
    }}
  >
    <Alert severity="info">
        {children}
    </Alert>
  </div>
);
