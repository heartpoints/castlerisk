import { CanvasEffect } from "../../../utils/canvas/CanvasEffect";
import { drawLayers } from "../common/drawLayers";
import { activeTerritories } from "./activeTerritories";
import { ChooseActionState } from "./ChooseActionState";
import { highlightedTerritoryGroupsControlledByPlayer } from "../common/highlightedTerritoryGroupsControlledByPlayer";

export const drawBoard = (state: ChooseActionState): CanvasEffect =>
  drawLayers({
    activeTerritories: activeTerritories(state),
    highlightedLineSegments: highlightedTerritoryGroupsControlledByPlayer(state),
    shouldDrawHighlight: false,
    state,
  });
