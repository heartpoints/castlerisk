import { FC } from "react";
import { SideNote } from "../chooseAction/SideNote";
import { ActionText } from "../common/ActionText";
import { HasCurrentPlayerIndex } from "../common/HasCurrentPlayerIndex";
import { HasPlayers } from "../common/HasPlayers";
import { SidePanelContainer } from "../common/SidePanelContainer";
import { PlayerHeading } from "../common/PlayerHeading";
import { ReserveArmies } from "../common/ReserveArmies";

export const SidePanel: FC<HasPlayers & HasCurrentPlayerIndex> = ({
  ...state
}) => (
  <SidePanelContainer>
    <PlayerHeading {...state} />
    <p>
      <ActionText>Claim Territories</ActionText> - In turn, players place 1 army
      piece on any unoccupied territory until all territories on the board are
      occupied.
    </p>
    <ReserveArmies {...state} />
    <SideNote>
      HINT: Remember, it is beneficial to possess all the territories of a given
      color!
    </SideNote>
  </SidePanelContainer>
);
