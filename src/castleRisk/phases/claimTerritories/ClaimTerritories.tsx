import { PropsWithSetState } from "../../../utils/react/PropsWithSetState";
import { GameCanvas } from "../../gameCanvas/gameCanvas";
import { GameGrid } from "../common/GameGrid";
import { onMouseMove } from "../common/onMouseMove";
import { ClaimTerritoriesState } from "./ClaimTerritoriesState";
import { SidePanel } from "./SidePanel";
import { drawBoard } from "./drawBoard";
import { onMouseUp } from "./onMouseUp";

export const ClaimTerritories = ({
  setState,
  ...state
}: PropsWithSetState<ClaimTerritoriesState>) => (
  <GameGrid
    gameCanvas={
      <GameCanvas
        drawGame={drawBoard(state)}
        onMouseMove={(e) => setState(onMouseMove(state)(e))}
        onMouseUp={onMouseUp(state)(setState)}
      />
    }
    sidePanel={<SidePanel {...state} />}
  />
);
