import { CastleRiskPlayState } from "../common/CastleRiskPlayState";

import { HasURI } from "@utils/zod/HasURI";
import { z } from "zod";

export const ClaimTerritoriesState = z.object({
  ...HasURI("ClaimTerritoriesState").shape,
  ...CastleRiskPlayState.shape,
});
export type ClaimTerritoriesState = z.infer<typeof ClaimTerritoriesState>;
