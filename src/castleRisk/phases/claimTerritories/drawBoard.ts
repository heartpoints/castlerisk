import { CanvasEffect } from "../../../utils/canvas/CanvasEffect";
import { drawLayers } from "../common/drawLayers";
import { highlightedTerritoryGroupsControlledByPlayer } from "../common/highlightedTerritoryGroupsControlledByPlayer";
import { activeTerritories } from "./activeTerritories";
import { ClaimTerritoriesState } from "./ClaimTerritoriesState";

export const drawBoard = (state: ClaimTerritoriesState): CanvasEffect =>
  drawLayers({
    activeTerritories: activeTerritories(state),
    highlightedLineSegments: highlightedTerritoryGroupsControlledByPlayer(state),
    shouldDrawHighlight: true,
    state,
  });
