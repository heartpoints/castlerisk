import { pipe } from 'fp-ts/lib/function'
import { movePieceFromCurrentPlayerHandToTerritory } from '../../pieces/movePieceFromCurrentPlayerHandToTerritory'
import { moveToNextPlayer } from '../../players/moveToNextPlayer'
import { Territory } from '../../territories/Territory'
import { stateWithUpToNPiecesInHand } from '../common/stateWithUpToNPiecesInHand'
import { PlaceHiddenArmiesState } from '../hiddenArmies/place/PlaceHiddenArmiesState'
import { toHiddenArmiesState } from '../hiddenArmies/place/toHiddenArmiesState'
import { ClaimTerritoriesState } from './ClaimTerritoriesState'
import { isPhaseComplete } from './isPhaseComplete'

export const stateWhenActiveTerritoryClicked = 
    (territory: Territory) => 
    (state: ClaimTerritoriesState): ClaimTerritoriesState | PlaceHiddenArmiesState => 
    pipe(
        state,
        movePieceFromCurrentPlayerHandToTerritory(territory),
        s => isPhaseComplete(s)
            ? toHiddenArmiesState(s)
            : pipe(
                s,
                moveToNextPlayer,
                stateWithUpToNPiecesInHand(1)
            )
    )
