import { new_ } from "@utils/zod/new_";
import { pipe } from "fp-ts/lib/function";
import { moveToNextPlayer } from "../../players/moveToNextPlayer";
import { stateWithUpToNPiecesInHand } from "../common/stateWithUpToNPiecesInHand";
import { PlaceCastlesState } from "../placeCastles/PlaceCastlesState";
import { ClaimTerritoriesState } from "./ClaimTerritoriesState";

export const toClaimTerritoriesState = (state: PlaceCastlesState) =>
  pipe(
    state,
    moveToNextPlayer,
    new_(ClaimTerritoriesState),
    stateWithUpToNPiecesInHand(1)
  );
