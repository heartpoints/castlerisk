import { activeTerritoryClickHandler } from '../common/activeTerritoryClickHandler'
import { activeTerritories } from './activeTerritories'
import { stateWhenActiveTerritoryClicked } from './stateWhenActiveTerritoryClicked'

export const onMouseUp = activeTerritoryClickHandler(activeTerritories)(stateWhenActiveTerritoryClicked)
