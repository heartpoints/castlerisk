import { CastleRiskPlayState } from '../common/CastleRiskPlayState'
import { activeTerritories } from './activeTerritories'

export const isPhaseComplete = 
    (state:CastleRiskPlayState) =>
    activeTerritories(state).length === 0
