import { currentPlayerFromState } from "../../../players/currentPlayerFromState"
import { territoryByName } from "../../../territories/territoryByName"
import { CastleRiskPlayState } from "../../common/CastleRiskPlayState"

export const activeTerritories = 
    (state: CastleRiskPlayState) => 
    currentPlayerFromState(state).hiddenArmyTerritoryNames.map(tn => territoryByName(tn)!)
