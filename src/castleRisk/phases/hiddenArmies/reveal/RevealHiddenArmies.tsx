import { new_ } from "@utils/zod/new_";
import { pipe } from "fp-ts/lib/function";
import { PropsWithSetState } from "../../../../utils/react/PropsWithSetState";
import { CardContainer } from "../../../cards/CardContainer";
import { GameCanvas } from "../../../gameCanvas/gameCanvas";
import { ChooseActionState } from "../../chooseAction/ChooseActionState";
import { currentlyHoveredActiveTerritory } from "../../common/currentlyHoveredActiveTerritory";
import { GameGrid } from "../../common/GameGrid";
import { onMouseMove } from "../../common/onMouseMove";
import { activeTerritories } from "./activeTerritories";
import { drawBoard } from "./drawBoard";
import { SidePanel } from "./SidePanel";
import { revealHiddenArmiesOnTerritory } from "./revealHiddenArmiesOnTerritory";
import { RevealHiddenArmiesState } from "./RevealHiddenArmiesState";
import { CurrentPlayerCards } from "castleRisk/cards/CurrentPlayerCards";

export const RevealHiddenArmies = (
  props: PropsWithSetState<RevealHiddenArmiesState>
) =>
  pipe(props, ({ setState, ...state }) => (
    <GameGrid
      gameCanvas={
        <GameCanvas
          drawGame={drawBoard(state)}
          onMouseMove={(e) => setState(onMouseMove(state)(e))}
          onMouseUp={() => {
            const territory = currentlyHoveredActiveTerritory(state.mousePoint)(
              activeTerritories(state)
            );
            territory.do((t) => {
              alert(
                `${state.reinforcementsCount} hidden armies revealed in ${t.name}!`
              );
              setState(
                new_(ChooseActionState)(revealHiddenArmiesOnTerritory(state, t))
              );
            });
          }}
        />
      }
      sidePanel={<SidePanel {...{ setState, ...state }} />}
      cards={<CurrentPlayerCards {...{setState, ...state}} />}
    />
  ));
