import { drawLayers } from "../../common/drawLayers";
import { RevealHiddenArmiesState } from "./RevealHiddenArmiesState";
import { activeTerritories } from "./activeTerritories";
import { CanvasEffect } from "../../../../utils/canvas/CanvasEffect";

export const drawBoard = (state: RevealHiddenArmiesState): CanvasEffect =>
  drawLayers({
    activeTerritories: activeTerritories(state),
    shouldDrawHighlight: true,
    state,
  });
