import { HasURI } from "@utils/zod/HasURI";
import { z } from "zod";
import { CastleRiskPlayState } from "../../common/CastleRiskPlayState";

export const RevealHiddenArmiesState = z.object({
  ...HasURI("RevealHiddenArmiesState").shape,
  ...CastleRiskPlayState.shape,
});
export type RevealHiddenArmiesState = z.infer<typeof RevealHiddenArmiesState>;
