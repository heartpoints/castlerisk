import { currentPlayerFromState } from "../../../players/currentPlayerFromState";
import { playerEquals } from "../../../players/playerEquals";
import { territoryByName } from "../../../territories/territoryByName";
import { playerForTerritory } from "../../../territories/territoryOwner";
import { CastleRiskPlayState } from "../../common/CastleRiskPlayState";

export const canRevealHiddenArmies = (state: CastleRiskPlayState) => {
  const currentPlayer = currentPlayerFromState(state);
  const { hiddenArmyTerritoryNames } = currentPlayer;
  const { players } = state;

  return (
    hiddenArmyTerritoryNames.length > 0 &&
    hiddenArmyTerritoryNames.some((hiddenArmyTerritoryName) => {
      const owningPlayer = playerForTerritory(players)(
        territoryByName(hiddenArmyTerritoryName)
      );
      return owningPlayer && playerEquals(owningPlayer)(currentPlayer);
    })
  );
};
