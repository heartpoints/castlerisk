import { Button } from "@mui/material";
import { new_ } from "@utils/zod/new_";
import { PropsWithSetState } from "../../../../utils/react/PropsWithSetState";
import { ChooseActionState } from "../../chooseAction/ChooseActionState";
import { SideNote } from "../../chooseAction/SideNote";
import { ActionText } from "../../common/ActionText";
import { PlayerActionButtons } from "../../common/PlayerActionButtons";
import { PlayerHeading } from "../../common/PlayerHeading";
import { ReserveArmies } from "../../common/ReserveArmies";
import { SidePanelContainer } from "../../common/SidePanelContainer";
import { RevealHiddenArmiesState } from "./RevealHiddenArmiesState";

export const SidePanel = ({
  setState,
  ...state
}: PropsWithSetState<RevealHiddenArmiesState>) => (
  <SidePanelContainer>
    <PlayerHeading {...state} />
    <p>
      <ActionText>Reveal Hidden Armies</ActionText> - Click a territory to
      reveal {state.reinforcementsCount} hidden armies!
    </p>
    <PlayerActionButtons>
      <Button
        variant="outlined"
        onClick={() => setState(new_(ChooseActionState)(state))}
      >
        Cancel
      </Button>
    </PlayerActionButtons>
    <ReserveArmies {...state} />
    <SideNote>
      NOTE: Players may have more than one hidden army location if they have
      defeated another player who had not yet used their hidden armies
    </SideNote>
  </SidePanelContainer>
);
