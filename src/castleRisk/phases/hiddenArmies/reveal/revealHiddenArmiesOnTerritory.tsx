import { isType } from "@utils/zod/isType";
import { new_ } from "@utils/zod/new_";
import { removeFirst } from "../../../../utils/arrays/removeFirst";
import { replaceN } from "../../../../utils/arrays/replaceN";
import { PlacedInReserves } from "../../../pieces/PlacedInReserves";
import { PlacedInTerritory } from "../../../pieces/PlacedInTerritory";
import { currentPlayerFromState } from "../../../players/currentPlayerFromState";
import { playerEquals } from "../../../players/playerEquals";
import { Territory } from "../../../territories/Territory";
import { CastleRiskPlayState } from "../../common/CastleRiskPlayState";

export const revealHiddenArmiesOnTerritory = (
  state: CastleRiskPlayState,
  territory: Territory
) => {
  const currentPlayer = currentPlayerFromState(state);
  return {
    ...state,
    players: state.players.map((player) =>
      playerEquals(currentPlayer)(player)
        ? {
            ...player,
            pieces: replaceN(
              player.pieces,
              isType(PlacedInReserves),
              (p) =>
                new_(PlacedInTerritory)({ ...p, territoryName: territory.name }),
              state.reinforcementsCount
            ),
            hiddenArmyTerritoryNames: removeFirst(player.hiddenArmyTerritoryNames, t => t === territory.name)
          }
        : player
    ),
  };
};
