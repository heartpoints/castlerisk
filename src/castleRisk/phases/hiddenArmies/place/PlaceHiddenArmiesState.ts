import { HasURI } from "@utils/zod/HasURI";
import { z } from "zod";
import { CastleRiskPlayState } from "../../common/CastleRiskPlayState";

export const PlaceHiddenArmiesState = z.object({
  ...HasURI("PlaceHiddenArmiesState").shape,
  ...CastleRiskPlayState.shape,
});
export type PlaceHiddenArmiesState = z.infer<typeof PlaceHiddenArmiesState>;
