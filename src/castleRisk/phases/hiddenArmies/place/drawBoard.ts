import { drawLayers } from "../../common/drawLayers";
import { PlaceHiddenArmiesState } from "./PlaceHiddenArmiesState";
import { activeTerritories } from "./activeTerritories";
import { CanvasEffect } from "../../../../utils/canvas/CanvasEffect";

export const drawBoard = (state: PlaceHiddenArmiesState): CanvasEffect =>
  drawLayers({
    activeTerritories: activeTerritories(state),
    shouldDrawHighlight: true,
    state,
  });
