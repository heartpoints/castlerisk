import { FC } from "react";
import { SideNote } from "../../chooseAction/SideNote";
import { ActionText } from "../../common/ActionText";
import { HasCurrentPlayerIndex } from "../../common/HasCurrentPlayerIndex";
import { HasPlayers } from "../../common/HasPlayers";
import { SidePanelContainer } from "../../common/SidePanelContainer";
import { PlayerHeading } from "../../common/PlayerHeading";
import { ReserveArmies } from "../../common/ReserveArmies";

export const SidePanel: FC<HasPlayers & HasCurrentPlayerIndex> = ({
  ...state
}) => (
  <SidePanelContainer>
    <PlayerHeading {...state} />
    <p>
      <ActionText>Place Hidden Armies</ActionText> - In turn, players choose one
      territory from among any on the board, so long as no castle occupies it.
    </p>
    <ReserveArmies {...state} />
    <SideNote>
      NOTE: Later in the game, a player may <i>reveal</i> their hidden armies,{" "}
      <i>but only if they occupy the territory at that time</i>
    </SideNote>
  </SidePanelContainer>
);
