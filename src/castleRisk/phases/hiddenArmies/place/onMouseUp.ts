import { activeTerritories } from "./activeTerritories";
import { stateWhenActiveTerritoryClicked } from "./stateWhenActiveTerritoryClicked";
import { activeTerritoryClickHandler } from "../../common/activeTerritoryClickHandler";

export const onMouseUp = activeTerritoryClickHandler(activeTerritories)(stateWhenActiveTerritoryClicked);
