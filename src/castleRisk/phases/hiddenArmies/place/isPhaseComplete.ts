import { isPlayReturningToFirstPlayer } from "../../common/isPlayReturningToFirstPlayer";

export const isPhaseComplete = isPlayReturningToFirstPlayer;
