import { FC } from "react";
import { PropsWithSetState } from "../../../../utils/react/PropsWithSetState";
import { GameCanvas } from "../../../gameCanvas/gameCanvas";
import { GameGrid } from "../../common/GameGrid";
import { onMouseMove } from "../../common/onMouseMove";
import { drawBoard } from "./drawBoard";
import { onMouseUp } from "./onMouseUp";
import { SidePanel } from "./SidePanel";
import { PlaceHiddenArmiesState } from "./PlaceHiddenArmiesState";

export const PlaceHiddenArmies: FC<
  PropsWithSetState<PlaceHiddenArmiesState>
> = ({ setState, ...state }) => (
  <GameGrid
    gameCanvas={
      <GameCanvas
        drawGame={drawBoard(state)}
        onMouseMove={(e) => setState(onMouseMove(state)(e))}
        onMouseUp={onMouseUp(state)(setState)}
      />
    }
    sidePanel={<SidePanel {...state} />}
  />
);
