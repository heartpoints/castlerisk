import { Territory } from "../../../territories/Territory";
import { PlaceHiddenArmiesState } from "./PlaceHiddenArmiesState";

//TODO: functional lenses from optics-ts can replace this
export const placeHiddenArmy =
  (territory: Territory) =>
  (state: PlaceHiddenArmiesState): PlaceHiddenArmiesState => ({
    ...state,
    players: state.players.map((player, index) =>
      index === state.currentPlayerIndex
        ? {
            ...player,
            hiddenArmyTerritoryNames: [territory.name],
          }
        : player
    ),
  });
