import { not } from "../../../../utils/predicates/not"
import { territoryHasCastle } from "../../../castles/territoryHasCastle"
import { territories } from "../../../map/territories"
import { CastleRiskPlayState } from "../../common/CastleRiskPlayState"

export const activeTerritories = 
    ({players}: CastleRiskPlayState) => 
    territories().filter(not(territoryHasCastle(players)))
