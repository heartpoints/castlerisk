import { flow } from 'fp-ts/lib/function'
import { moveToNextPlayer } from '../../../players/moveToNextPlayer'
import { PlaceHiddenArmiesState } from './PlaceHiddenArmiesState'
import { new_ } from '@utils/zod/new_'

export const toHiddenArmiesState =
    flow(moveToNextPlayer, new_(PlaceHiddenArmiesState))
