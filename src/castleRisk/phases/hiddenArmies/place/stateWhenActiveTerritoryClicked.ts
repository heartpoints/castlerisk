import { pipe } from "fp-ts/lib/function"
import { moveToNextPlayer } from "../../../players/moveToNextPlayer"
import { Territory } from "../../../territories/Territory"
import { FortifyTerritoriesState } from "../../fortifyTerritories/FortifyTerritoriesState"
import { toFortifyTerritoriesState } from "../../fortifyTerritories/toFortifyTerritoriesState"
import { isPhaseComplete } from "./isPhaseComplete"
import { PlaceHiddenArmiesState } from "./PlaceHiddenArmiesState"
import { placeHiddenArmy } from "./placeHiddenArmy"

export const stateWhenActiveTerritoryClicked = 
    (territory:Territory) =>
    (state:PlaceHiddenArmiesState):PlaceHiddenArmiesState | FortifyTerritoriesState =>
    pipe(
        state,
        placeHiddenArmy(territory),
        s => isPhaseComplete(s)
            ? toFortifyTerritoriesState(s)
            : moveToNextPlayer(s)
    )