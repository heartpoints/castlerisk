import { CanvasEffect } from "../../../utils/canvas/CanvasEffect";
import { clearCanvas } from "../../../utils/canvas/clearCanvas";
import { drawNothing } from "../../../utils/canvas/drawNothing";
import { drawSegment } from "../../../utils/canvas/drawSegment";
import { white } from "../../../utils/colors/white";
import { sequentially } from "../../../utils/composition/sequentially";
import { Segments } from "../../../utils/geometry/Segments";
import { not } from "../../../utils/predicates/not";
import { castlesOfPlayers } from "../../castles/castlesOfPlayers";
import { drawCastlePieces } from "../../castles/drawCastlePieces";
import { territories } from "../../map/territories";
import { drawArmyPiecesInHand } from "../../pieces/drawArmyPiecesInHand";
import { drawPlacedArmiesForPlayer } from "../../pieces/drawPlacedArmiesForPlayer";
import { drawPlayerNameByMouse } from "../../players/drawPlayerNameByMouse";
import { darkName } from "../../territories/darkName";
import { drawCurrentlyHoveredTerritoryWithoutDots } from "../../territories/drawCurrentlyHoveredTerritoryWithoutDots";
import { drawDarkTerritory } from "../../territories/drawDarkTerritory";
import { drawNormalTerritory } from "../../territories/drawNormalTerritory";
import { normalName } from "../../territories/normalName";
import { Territories } from "../../territories/Territories";
import { territoryIn } from "../../territories/territoryIn";
import { drawSea } from "../../water/drawSea";
import { CastleRiskPlayState } from "./CastleRiskPlayState";
import { currentlyHoveredActiveTerritory } from "./currentlyHoveredActiveTerritory";
import { drawDottedLines } from "./drawDottedLines";
import { drawTerritoryGroupNames } from "./drawTerritoryGroupNames";

type DrawLayers = (args: {
  activeTerritories: Territories;
  highlightedLineSegments?: Segments;
  shouldDrawHighlight: Boolean;
  state: CastleRiskPlayState;
}) => CanvasEffect;

export const drawLayers: DrawLayers = ({
  activeTerritories,
  highlightedLineSegments = [],
  shouldDrawHighlight,
  state,
}) => {
  const { players, mousePoint } = state;
  const inactiveTerritories = territories().filter(
    not(territoryIn(activeTerritories))
  );
  const currentTerritory =
    currentlyHoveredActiveTerritory(mousePoint)(activeTerritories);
  return sequentially([
    clearCanvas,
    drawSea,
    drawDottedLines,
    ...inactiveTerritories.map(drawDarkTerritory),
    ...activeTerritories.map(drawNormalTerritory),
    shouldDrawHighlight
      ? currentTerritory.mapOrDefault(
          drawCurrentlyHoveredTerritoryWithoutDots,
          drawNothing
        )
      : drawNothing,
    ...activeTerritories.map(normalName),
    ...inactiveTerritories.map(darkName),
    ...players.map(drawPlacedArmiesForPlayer(players)),
    ...highlightedLineSegments.map(drawSegment(white)),
    drawCastlePieces(castlesOfPlayers(players)),
    drawPlayerNameByMouse(state),
    drawArmyPiecesInHand(state),
    drawTerritoryGroupNames,
  ]);
};
