import { numPiecesInHand } from "../../pieces/numPiecesInHand";
import { currentPlayerFromState } from "../../players/currentPlayerFromState";
import { CastleRiskPlayState } from "./CastleRiskPlayState";
import { PlaceSpoilsState } from "../placeSpoils/PlaceSpoilsState";

export const isPlayerDonePlacingPieces = 
    (state:CastleRiskPlayState | PlaceSpoilsState) =>
    numPiecesInHand(currentPlayerFromState(state)) === 0
