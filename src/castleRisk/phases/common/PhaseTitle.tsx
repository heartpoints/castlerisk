
export const PhaseTitle =
    ({ children: phaseName }) => 
    <h2>{phaseName}</h2>
