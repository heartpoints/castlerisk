import { CanvasEffect } from "../../../../utils/canvas/CanvasEffect";
import { CastleRiskPlayState } from "../CastleRiskPlayState";
import { drawLayers } from "../drawLayers";
import { activeTerritories } from "./activeTerritories";

export const drawBoard = (
  state: CastleRiskPlayState
): CanvasEffect =>
  drawLayers({
    activeTerritories: activeTerritories(state),
    shouldDrawHighlight: true,
    state,
  });
