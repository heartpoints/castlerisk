import { currentPlayerFromState } from "../../../players/currentPlayerFromState"
import { territoriesForPlayer } from "../../../territories/territoriesForPlayer"
import { CastleRiskPlayState } from "../CastleRiskPlayState"

export const activeTerritories = 
    (state:CastleRiskPlayState) =>
    territoriesForPlayer(currentPlayerFromState(state))
    