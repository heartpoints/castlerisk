import { VoidCallback } from "../../../utils/axioms/VoidCallback";

export type HasOnClick = { onClick: VoidCallback; };
