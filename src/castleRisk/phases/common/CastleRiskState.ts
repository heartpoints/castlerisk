import { AddPlayersState } from "../addPlayers/AddPlayersState";
import { AdminState } from "../admin/AdminState";
import { AttackerWonState } from "../attack/attackerWon/AttackerWonState";
import { ChooseAttackOriginState } from "../attack/chooseAttackOrigin/ChooseAttackOriginState";
import { ChooseAttackTargetState } from "../attack/chooseAttackTarget/ChooseAttackTargetState";
import { ChooseDiceCountState } from "../attack/chooseDiceCount/ChooseDiceCountState";
import { AttackOutcomeState } from "../attack/general/AttackOutcomeState";
import { RollingState } from "../attack/rolling/RollingState";
import { BoardAdmiralShipState } from "../attack/boardAdmiralShip/BoardAdmiralShipState";
import { ChooseActionState } from "../chooseAction/ChooseActionState";
import { ClaimTerritoriesState } from "../claimTerritories/ClaimTerritoriesState";
import { FillDefeatedTerritoriesState } from "../fillDefeatedTerritories/FillDefeatedTerritoriesState";
import { FortifyTerritoriesState } from "../fortifyTerritories/FortifyTerritoriesState";
import { PlaceHiddenArmiesState } from "../hiddenArmies/place/PlaceHiddenArmiesState";
import { RevealHiddenArmiesState } from "../hiddenArmies/reveal/RevealHiddenArmiesState";
import { PlaceCastlesState } from "../placeCastles/PlaceCastlesState";
import { PlaceReinforcementsState } from "../placeReinforcments/PlaceReinforcementsState";
import { PlaceSpoilsState } from "../placeSpoils/PlaceSpoilsState";
import { PlayDiplomatState } from "../playDiplomat/PlayDiplomatState";
import { PlayerDefeatedState } from "../playerDefeated/PlayerDefeatedState";

export type CastleRiskState =
  | AddPlayersState
  | PlaceCastlesState
  | ClaimTerritoriesState
  | PlaceHiddenArmiesState
  | RevealHiddenArmiesState
  | FortifyTerritoriesState
  | ChooseActionState
  | PlaceSpoilsState
  | AdminState
  | ChooseAttackOriginState
  | ChooseAttackTargetState
  | ChooseDiceCountState
  | AttackerWonState
  | AttackOutcomeState
  | PlayerDefeatedState
  | FillDefeatedTerritoriesState
  | RollingState
  | PlaceReinforcementsState
  | BoardAdmiralShipState
  | PlayDiplomatState;
