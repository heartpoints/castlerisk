import { ReactElement } from "react";

type GameGridProps = {
  sidePanel: ReactElement;
  gameCanvas: ReactElement;
  cards?: ReactElement;
  dice?: ReactElement;
};
export const GameGrid = ({
  sidePanel,
  gameCanvas,
  cards,
  dice,
}: GameGridProps) => (
  <div
    style={{
      display: "flex",
      flexDirection: "row",
      justifyContent: "flex-start",
    }}
  >
    <div style={{ width: "70%", height: "100vh", position: "relative" }}>
      {gameCanvas}
      {cards}
      {dice}
    </div>
    <div style={{ width: "30%", height: "100vh", zIndex: 1 }}>
      {sidePanel}
    </div>
  </div>
);
