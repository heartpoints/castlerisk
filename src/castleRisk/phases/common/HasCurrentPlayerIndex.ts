import { z } from "zod"

export const HasCurrentPlayerIndex = z.object({currentPlayerIndex: z.number().int().nonnegative()}) 
export type HasCurrentPlayerIndex = z.infer<typeof HasCurrentPlayerIndex>
