import { z } from "zod"
import { Players } from "../../players/Players"

export const HasPlayers = z.object({players: Players})
export type HasPlayers = z.infer<typeof HasPlayers>
