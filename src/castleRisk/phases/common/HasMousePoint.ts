import { Point } from '../../../utils/geometry/Point';

import { z } from "zod";

export const HasMousePoint = z.object({ mousePoint: Point });
export type HasMousePoint = z.infer<typeof HasMousePoint>;
