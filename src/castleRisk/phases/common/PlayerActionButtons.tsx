import { FC, PropsWithChildren } from "react";

export const PlayerActionButtons: FC<PropsWithChildren> = ({ children }) => (
  <div
    style={{
      display: "flex",
      flexDirection: "column",
      gap: "0.5em",
      borderStyle: "dashed",
      borderWidth: "2px",
      padding: "1em",
      borderColor: "rgba(0,0,0,0.1)",
    }}
  >
    {children}
  </div>
);
