import { Consumer } from "../../../utils/axioms/Consumer"
import { Mapper } from "../../../utils/axioms/Mapper"
import { Point } from "../../../utils/geometry/Point"
import { Territories } from "../../territories/Territories"
import { CastleRiskPlayState } from "./CastleRiskPlayState"
import { currentlyHoveredActiveTerritory } from "./currentlyHoveredActiveTerritory"
import { StateWhenActiveTerritoryClicked } from "./StateWhenActiveTerritoryClicked"

//NOTE: This complex signature just generates a mouse click handler that, if the mouse is hovering
//over an active territory, delegates to your state transforming function `stateWhenActiveTerritoryClicked`
//and react setState's the result for you.
export const activeTerritoryClickHandler = 
    (activeTerritories:Mapper<CastleRiskPlayState, Territories>) =>
    <T extends CastleRiskPlayState, OutputStateType>
    (stateWhenActiveTerritoryClicked:StateWhenActiveTerritoryClicked<T, OutputStateType>) =>
    (state:T) =>
    (setState:Consumer<OutputStateType>) =>
    (ignoredClickEvent:Point) =>
    currentlyHoveredActiveTerritory(state.mousePoint)(activeTerritories(state)).map(
        territory => setState(stateWhenActiveTerritoryClicked(territory)(state))
    )
