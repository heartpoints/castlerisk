import { territoriesAttackableByWater } from "castleRisk/cards/admiral/territoriesAttackableByWater";
import { Player } from "../../players/Player";
import { Players } from "../../players/Players";
import { Territory } from "../../territories/Territory";
import { uniqueTerritories } from "./uniqueTerritories";
import { territoriesAttackableByLand } from "./territoriesAttackableByLand";
import { isAdmiralActive } from "castleRisk/cards/admiral/isAdmiralActive";

export const attackableTerritories =
  (players: Players) =>
  (currentPlayer: Player) =>
  (attackOriginTerritory: Territory) =>
    uniqueTerritories([
      ...territoriesAttackableByLand(players)(currentPlayer)(
        attackOriginTerritory
      ),
      ...(isAdmiralActive({ players })
        ? territoriesAttackableByWater(
            currentPlayer,
            attackOriginTerritory,
            players
          )
        : []),
    ]);
