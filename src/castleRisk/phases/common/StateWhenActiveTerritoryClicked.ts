import { Territory } from "../../territories/Territory"

export type StateWhenActiveTerritoryClicked<InputState, OutputState> = 
    (territory: Territory) => 
    (state: InputState) => 
    OutputState
