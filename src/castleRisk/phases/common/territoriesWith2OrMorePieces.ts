import { territoryPieceCountPairsForPlayer } from "../../pieces/territoryPieceCountPairs"
import { Player } from "../../players/Player"
import { Players } from "../../players/Players"

export const territoriesWith2OrMorePieces = 
    (players:Players) =>
    (player:Player) =>
    territoryPieceCountPairsForPlayer(players)(player)
        .filter(([_, numPieces]) => numPieces > 1)
        .map(([t]) => t)
