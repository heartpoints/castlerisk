import { isNoActiveTruce } from "castleRisk/cards/diplomat/isNoActiveTruce";
import { Player } from "castleRisk/players/Player";
import { playerEquals } from "castleRisk/players/playerEquals";
import { Players } from "castleRisk/players/Players";
import { Territory } from "castleRisk/territories/Territory";
import { playerForTerritory } from "castleRisk/territories/territoryOwner";

export const isOpponentOccupiedWithNoTruce =
  (attackingPlayer: Player, players: Players) => (territory: Territory) => {
    const possiblyAttackablePlayer = playerForTerritory(players)(territory);
    return (
      !!possiblyAttackablePlayer &&
      !playerEquals(possiblyAttackablePlayer)(attackingPlayer) &&
      isNoActiveTruce(attackingPlayer, possiblyAttackablePlayer)
    );
  };
