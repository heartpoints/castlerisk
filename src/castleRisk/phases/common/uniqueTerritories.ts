import { deduplicateItems } from "@utils/hashing/deduplicateItems";
import { Territory } from "castleRisk/territories/Territory";

export const uniqueTerritories = deduplicateItems((t: Territory) => t.name);
