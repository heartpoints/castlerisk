import { pipe } from "fp-ts/lib/function";
import { length } from "../../../utils/arrays/length";
import { isGreaterThan } from "../../../utils/math/isGreaterThan";
import { Player } from "../../players/Player";
import { Players } from "../../players/Players";
import { Territory } from "../../territories/Territory";
import { attackableTerritories } from "./attackableTerritories";

export const hasAttackableTerritory =
  (players: Players) =>
  (currentPlayer: Player) =>
  (attackOriginTerritory: Territory) =>
    pipe(
      attackableTerritories(players)(currentPlayer)(attackOriginTerritory),
      length,
      isGreaterThan(0)
    );
