import { playersWithPiecesMovedFromReserveToHand } from '../../players/playersWithPiecesMovedFromReserveToHand'
import { CastleRiskPlayState } from "./CastleRiskPlayState"

export const stateWithUpToNPiecesInHand = 
    (numPiecesMax: number) => 
    <T extends CastleRiskPlayState>(state: T) => 
    ({
        ...state,
        players: playersWithPiecesMovedFromReserveToHand(state.players)(state.currentPlayerIndex)(numPiecesMax),
    })
