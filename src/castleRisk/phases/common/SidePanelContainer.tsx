import { FC, PropsWithChildren } from "react";

export const SidePanelContainer: FC<PropsWithChildren> = ({ children }) => (
  <section
    style={{
      backgroundColor: "rgba(255,255,255,0.9)",
      padding: "1em",
      boxShadow: "-10px 10px 10px rgba(0, 0, 0, 0.3)",
      minHeight: "100vh",
      display: "flex",
      flexDirection: "column",
      gap: "1em",
    }}
  >
    {children}
  </section>
);
