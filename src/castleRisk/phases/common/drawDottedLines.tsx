import { centroidOfPolygon } from '../../../utils/geometry/centroidOfPolygon';
import { dottedLineConnections } from '../../territories/dottedLineConnections';
import { territoryByName } from '../../territories/territoryByName';

export const drawDottedLines = (context: CanvasRenderingContext2D) => {
    dottedLineConnections.forEach(
        ([startTerritoryName, endTerritoryName]) => {
            context.save();
            const start = centroidOfPolygon(territoryByName(startTerritoryName)!.boundary);
            const end = centroidOfPolygon(territoryByName(endTerritoryName)!.boundary);
            context.setLineDash([5, 4]);
            context.lineWidth = 2;
            context.strokeStyle = 'black';
            context.beginPath();
            context.moveTo(start.x, start.y);
            context.lineTo(end.x, end.y);
            context.stroke();
            context.restore();
        }
    );
};
