import { CanvasEffect } from "../../../utils/canvas/CanvasEffect";
import { drawText } from "../../../utils/canvas/drawText";
import { territoryGroups } from "../../map/territoryGroups";
import { centroidOfPolygon } from "../../../utils/geometry/centroidOfPolygon";
import { uniqueTerritoryGroupSegments } from "../../empires/uniqueTerritoryGroupSegments";
import { centeredTextPoint } from "../../../utils/canvas/centeredTextPoint";
import { shapesFromSegments } from "../../../utils/geometry/shapesFromSegments";

export const drawTerritoryGroupNames: CanvasEffect = (context) => {
  const territoryShapeListPairs = territoryGroups().map((tg) => ({
    tg,
    shapes: shapesFromSegments(uniqueTerritoryGroupSegments(tg)),
  }));
  territoryShapeListPairs.forEach(({ tg, shapes }) =>
    shapes.forEach((shape) =>
      //TODO: if the shape is exactly equal to one territory border, exclude it
      drawText(false)([255, 255, 255, 0.5])(
        centeredTextPoint(centroidOfPolygon(shape), tg.name, context)
      )(tg.name)(context)
    )
  );
};
