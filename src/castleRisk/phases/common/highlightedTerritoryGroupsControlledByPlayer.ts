import { territoryGroupsControlledByPlayer } from "../../empires/territoryGroupsControlledByPlayer";
import { uniqueTerritoryGroupSegments } from "../../empires/uniqueTerritoryGroupSegments";
import { currentPlayerFromState } from "../../players/currentPlayerFromState";
import { CastleRiskPlayState } from "./CastleRiskPlayState";


export const highlightedTerritoryGroupsControlledByPlayer = (state: CastleRiskPlayState) => territoryGroupsControlledByPlayer(
  state,
  currentPlayerFromState(state)
).flatMap(uniqueTerritoryGroupSegments);
