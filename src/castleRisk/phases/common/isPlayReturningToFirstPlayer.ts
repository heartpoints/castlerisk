import { nextPlayerIndex } from '../../players/nextPlayerIndex';
import { CastleRiskPlayState } from './CastleRiskPlayState';

export const isPlayReturningToFirstPlayer = (s: CastleRiskPlayState) => nextPlayerIndex(s) === 0;
