import { PlayerCards } from "castleRisk/cards/PlayerCards";
import { z } from "zod";

export const HasDiscardPile = z.object({ discardPile: PlayerCards });
export type HasDiscardPile = z.infer<typeof HasDiscardPile>;
