import { DiplomatAvatar } from "castleRisk/cards/diplomat/DiplomatAvatar";
import { FC, useEffect, useRef } from "react";
import { context2dForRef } from "../../../utils/canvas/context2dForRef";
import { drawReserveArmies } from "../../pieces/drawReserveArmies";
import { currentPlayerFromState } from "../../players/currentPlayerFromState";
import { playerBoxSize } from "../../players/playerBoxSize";
import { HasCurrentPlayerIndex } from "./HasCurrentPlayerIndex";
import { HasPlayers } from "./HasPlayers";
import { activeDiplomats } from "../../cards/diplomat/activeDiplomats";

export const ReserveArmies: FC<HasPlayers & HasCurrentPlayerIndex> = ({
  ...state
}) => {
  const { players } = state;
  const canvasRef = useRef(null);
  useEffect(() => {
    drawReserveArmies(players)(currentPlayerFromState(state))(
      context2dForRef(canvasRef)
    );
  });
  return (
    <section style={{ display: "flex", flexDirection: "column", gap: "1em" }}>
      <h3>Reserve Armies + Castle Pieces</h3>
      <canvas
        ref={canvasRef}
        width={playerBoxSize.x}
        height={playerBoxSize.y}
        style={{
          width: playerBoxSize.x,
          height: playerBoxSize.y,
          borderColor: "gray",
          borderWidth: "1px",
          borderStyle: "solid",
        }}
      />
      <div style={{ display: "flex", flexDirection: "row", gap: "0.5em" }}>
        {activeDiplomats(state).map((d, i) => (
          <DiplomatAvatar diplomat={d} {...state} key={i} />
        ))}
      </div>
    </section>
  );
};
