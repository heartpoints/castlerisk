import { z } from "zod";

export const HasHasAttacked = z.object({ hasAttacked: z.boolean() });
export type HasHasAttacked = z.infer<typeof HasHasAttacked>;
