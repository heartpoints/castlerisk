import { z } from "zod";
import { HasPlayerCards } from "../../cards/HasPlayerCards";
import { HasReinforcementsCount } from "../../cards/reinforcements/HasReinforcementsCount";
import { HasCurrentPlayerIndex } from "./HasCurrentPlayerIndex";
import { HasMousePoint } from "./HasMousePoint";
import { HasPlayers } from "./HasPlayers";
import { HasDiscardPile } from "./HasDiscardPile";
import { HasHasAttacked } from "./HasHasAttacked";

export const CastleRiskPlayState = z.object({
  ...HasCurrentPlayerIndex.shape,
  ...HasReinforcementsCount.shape,
  ...HasPlayerCards.shape,
  ...HasDiscardPile.shape,
  ...HasMousePoint.shape,
  ...HasPlayers.shape,
  ...HasHasAttacked.shape,
});

export type CastleRiskPlayState = z.infer<typeof CastleRiskPlayState>;
