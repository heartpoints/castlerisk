import { mergeProps } from '../../../utils/composition/mergeProps'
import { Point } from '../../../utils/geometry/Point'
import { HasMousePoint } from './HasMousePoint'

//TODO: refactor again to make a handler that accepts setState (make more like onMouseUp?)
export const onMouseMove = 
    <S extends HasMousePoint>(state:S) =>
    (e:Point) => 
    mergeProps({ mousePoint: e })(state)