import { Player } from "castleRisk/players/Player";
import { Players } from "castleRisk/players/Players";
import { adjacentOrDottedLineTerritories } from "castleRisk/territories/adjacentOrDottedLineTerritories";
import { Territory } from "castleRisk/territories/Territory";
import { isOpponentOccupiedWithNoTruce } from "./isOpponentOccupiedWithNoTruce";

export const territoriesAttackableByLand =
  (players: Players) =>
  (currentPlayer: Player) =>
  (attackOriginTerritory: Territory) =>
    adjacentOrDottedLineTerritories(attackOriginTerritory).filter(
      isOpponentOccupiedWithNoTruce(currentPlayer, players)
    );
