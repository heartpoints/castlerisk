import { Territory } from 'castleRisk/territories/Territory'
import { Point } from '../../../utils/geometry/Point'
import { Territories } from '../../territories/Territories'
import { currentlyHoveredTerritory } from '../../territories/currentlyHoveredTerritory'
import { territoryIn } from '../../territories/territoryIn'
import { Maybe } from '@utils/maybe/MaybeType'

export const currentlyHoveredActiveTerritory = 
    (mousePoint:Point) => 
    (activeTerritories:Territories):Maybe<Territory> =>
    currentlyHoveredTerritory(mousePoint).if(territoryIn(activeTerritories))
