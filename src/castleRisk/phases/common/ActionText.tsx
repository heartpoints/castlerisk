import { FC, PropsWithChildren } from "react";

export const ActionText: FC<PropsWithChildren> = ({ children }) => (
  <strong style={{ color: "green", fontWeight: "bolder" }}>{children}</strong>
);
