import { FC } from "react";
import { currentPlayerName } from "../../players/currentPlayerName";
import { HasCurrentPlayerIndex } from "./HasCurrentPlayerIndex";
import { HasPlayers } from "./HasPlayers";
import { PhaseTitle } from "./PhaseTitle";

export const PlayerHeading: FC<HasPlayers & HasCurrentPlayerIndex> = ({ ...state }) => (
  <PhaseTitle>
    {currentPlayerName(state)}'s Turn{" "}
  </PhaseTitle>
);
