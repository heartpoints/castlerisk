import { not } from "../../../utils/predicates/not"
import { pieceColors } from "../../pieces/pieceColors"
import { withinColor } from "../../../utils/colors/withinColor"
import { Players } from "../../players/Players"

export const unusedColorsForPlayers = (players: Players) => pieceColors.filter(not(withinColor(players.map(p => p.color))))
