import { Players } from '../../players/Players'
import { toPlaceCastlesState } from '../placeCastles/toPlaceCastlesState'

export const onGameStart = 
    (playersWithoutEmpires: Players) => 
    toPlaceCastlesState(playersWithoutEmpires)
