import { Colors } from "../../../utils/colors/Colors";
import { HasURI } from "../../../utils/zod/HasURI";
import { Player } from "../../players/Player";
import { HasPlayers } from "../common/HasPlayers";

import { z } from "zod";

export const AddPlayersState = z.object({
  ...HasURI("AddPlayersState").shape,
  ...HasPlayers.shape,
  newPlayer: Player,
  remainingColors: Colors,
});
export type AddPlayersState = z.infer<typeof AddPlayersState>;
