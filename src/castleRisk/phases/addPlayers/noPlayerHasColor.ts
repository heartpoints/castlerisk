import { colorsAreEqual } from '../../../utils/colors/colorsAreEqual'
import { Players } from '../../players/Players'
import { RGBAColor } from '../../../utils/colors/RGBAColor'

export const noPlayerHasColor = 
    (players: Players) => 
    (c: RGBAColor) => 
    !players.some(p => colorsAreEqual(c)(p.color))
