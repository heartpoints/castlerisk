import { Button } from "@mui/material";
import { PropsWithSetState } from "../../../utils/react/PropsWithSetState";
import { pieceColors } from "../../pieces/pieceColors";
import { PlayerSection } from "../../players/PlayerSection";
import { initialPlayer } from "../../players/initialPlayer";
import { ActionText } from "../common/ActionText";
import { PhaseTitle } from "../common/PhaseTitle";
import { SidePanelContainer } from "../common/SidePanelContainer";
import { AddPlayersState } from "./AddPlayersState";
import { NewPlayerForm } from "./newPlayerForm";
import { noPlayerHasColor } from "./noPlayerHasColor";
import { onGameStart } from "./onGameStart";

export const PlayerSetupForm = ({
  setState,
  ...state
}: PropsWithSetState<AddPlayersState>) => {
  const { players, newPlayer } = state;
  const remainingColors = pieceColors.filter(noPlayerHasColor(players));
  const allowNewPlayers = remainingColors.length > 0;
  const initialColor = remainingColors[0];
  const player = newPlayer || initialPlayer("", initialColor, []);
  return (
    <SidePanelContainer>
      <PhaseTitle>Who's Playing?</PhaseTitle>
      <p>
        <ActionText>Enter player names and colors</ActionText> and then click
        begin game
      </p>
      {allowNewPlayers && (
        <NewPlayerForm {...state} {...{ player, remainingColors, setState }} />
      )}
      <PlayerSection {...{ players, player }} />
      <Button
        variant="contained"
        onClick={() => setState(onGameStart(players))}
        disabled={players.length < 2}
      >
        Begin game
      </Button>
    </SidePanelContainer>
  );
};
