import { Provider } from "../../../utils/axioms/Provider"
import { black } from "../../../utils/colors/black"
import { AddPlayersState } from "./AddPlayersState"
import { unusedColorsForPlayers } from "./unusedColorsForPlayers"
import { newPlayer } from "../../players/newPlayer"
import { new_ } from "@utils/zod/new_"

export const initialCastleRiskStateValue:Provider<AddPlayersState> = 
    () => 
    new_(AddPlayersState)({
        players: [],
        newPlayer: newPlayer(black)([]),
        remainingColors: unusedColorsForPlayers([]),
    })
