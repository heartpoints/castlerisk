import { Player } from '../../players/Player'
import { Players } from '../../players/Players'
import { playerNameNotDuplicated } from '../../players/playerNameNotDuplicated'
import { playerNameRequirementsMet } from '../../players/playerNameRequirementsMet'

export const allowedToAdd = 
    (players: Players) => 
    (possibleNewPlayer: Player) => 
    playerNameRequirementsMet(possibleNewPlayer)
    && playerNameNotDuplicated(players)(possibleNewPlayer)
