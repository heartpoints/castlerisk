import { Button, TextField } from "@mui/material";
import { ColorPicker } from "../../../utils/colors/ColorPicker";
import { RGBAColor } from "../../../utils/colors/RGBAColor";
import { black } from "../../../utils/colors/black";
import { PropsWithSetState } from "../../../utils/react/PropsWithSetState";
import { Player } from "../../players/Player";
import { newPlayer } from "../../players/newPlayer";
import { PlayerActionButtons } from "../common/PlayerActionButtons";
import { AddPlayersState } from "./AddPlayersState";
import { allowedToAdd } from "./allowedToAdd";
import { unusedColorsForPlayers } from "./unusedColorsForPlayers";

type NewPlayerFormProps = PropsWithSetState<
  { player: Player } & AddPlayersState
>;
export const NewPlayerForm = ({ setState, ...state }: NewPlayerFormProps) => {
  const { player, players, remainingColors } = state;
  const onButtonClick = () => {
    const newPlayers = [...players, player];
    setState({
      ...state,
      players: newPlayers,
      newPlayer: newPlayer(unusedColorsForPlayers(newPlayers)[0] || black)(
        newPlayers
      ),
    });
  };

  const updatePlayerName = ({ target: { value: name } }) =>
    setState({
      ...state,
      newPlayer: { ...player, name },
    });

  const updateColor = (color: RGBAColor) =>
    setState({
      ...state,
      newPlayer: { ...player, color },
    });

  return (
    <PlayerActionButtons>
      <TextField
        id="outlined-basic"
        label="Player Name"
        variant="outlined"
        value={player.name}
        onChange={updatePlayerName}
      />
      <ColorPicker
        colors={remainingColors}
        onColorChange={updateColor}
        value={player.color}
      />
      <Button
        variant="outlined"
        onClick={onButtonClick}
        disabled={!allowedToAdd(players)(player)}
      >
        Add player
      </Button>
    </PlayerActionButtons>
  );
};
