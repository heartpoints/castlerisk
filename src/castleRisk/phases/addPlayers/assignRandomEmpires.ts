import { randomSampling } from '../../../utils/list/randomSampling';
import { zip } from '../../../utils/list/zip';
import { empires } from '../../map/empires/empires';
import { Players } from '../../players/Players';

export const assignRandomEmpires = 
    (players: Players): Players => 
    zip(players, randomSampling(players.length)(empires()))
    .value
    .map(([player, empire]) => ({
        ...player,
        castles: [{ empireName: empire.name }],
    }));
