import { PropsWithSetState } from "../../../utils/react/PropsWithSetState";
import { GameGrid } from "../common/GameGrid";
import { AddPlayersState } from "./AddPlayersState";
import { GameBoardWithAccessories } from "./GameBoardWithAccessories";
import { PlayerSetupForm } from "./playerSetupForm";

export const AddPlayers = ({
  setState,
  ...state
}: PropsWithSetState<AddPlayersState>) => (
  <GameGrid
    gameCanvas={<GameBoardWithAccessories />}
    sidePanel={<PlayerSetupForm {...{ setState, ...state }} />}
  />
);
