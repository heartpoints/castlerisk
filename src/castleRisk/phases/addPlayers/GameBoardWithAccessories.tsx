export const GameBoardWithAccessories = () => (
    <div style={{ width: "100%", height: "100vh", padding: "20px" }}>
        <img
            src="images/castleRiskGameBoardWithAccessories.png"
            style={{ width: "100%", height: "auto" }} 
            alt="castle risk game board with accessories shown"
        />
    </div>
);
