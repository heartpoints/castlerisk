import { FC } from "react";
import { SideNote } from "../chooseAction/SideNote";
import { ActionText } from "../common/ActionText";
import { HasCurrentPlayerIndex } from "../common/HasCurrentPlayerIndex";
import { HasPlayers } from "../common/HasPlayers";
import { SidePanelContainer } from "../common/SidePanelContainer";
import { PlayerHeading } from "../common/PlayerHeading";
import { ReserveArmies } from "../common/ReserveArmies";

export const SidePanel: FC<HasPlayers & HasCurrentPlayerIndex> = ({
  ...state
}) => (
  <SidePanelContainer>
    <PlayerHeading {...state} />
    <p>
      <ActionText>Fortify Territories</ActionText> - In turn, each player places
      5 armies (or as many as they have in reserve if less than 5) on any
      territory they already occupy.
    </p>
    <ReserveArmies {...state} />
    <SideNote>
      Once all players have run out of armies, player order will be randomized
      again and the setup phase of the game will be considered complete.
    </SideNote>
  </SidePanelContainer>
);
