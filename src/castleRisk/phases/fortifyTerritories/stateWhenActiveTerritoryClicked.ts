import { pipe } from "fp-ts/lib/function"
import { movePieceFromCurrentPlayerHandToTerritory } from "../../pieces/movePieceFromCurrentPlayerHandToTerritory"
import { moveToNextPlayer } from "../../players/moveToNextPlayer"
import { Territory } from "../../territories/Territory"
import { ChooseActionState } from "../chooseAction/ChooseActionState"
import { toChooseActionFromFortifyTerritories } from "../chooseAction/toChooseActionFromFortifyTerritories"
import { isPlayerDonePlacingPieces } from "../common/isPlayerDonePlacingPieces"
import { stateWithUpToNPiecesInHand } from "../common/stateWithUpToNPiecesInHand"
import { FortifyTerritoriesState } from "./FortifyTerritoriesState"
import { isPhaseComplete } from "./isPhaseComplete"

export const stateWhenActiveTerritoryClicked = 
    (territory:Territory) =>
    (state: FortifyTerritoriesState): FortifyTerritoriesState | ChooseActionState => 
    pipe(
        state,
        movePieceFromCurrentPlayerHandToTerritory(territory),
        s => isPlayerDonePlacingPieces(s)
            ? isPhaseComplete(s)
                ? toChooseActionFromFortifyTerritories(s)
                : pipe(
                    s,
                    moveToNextPlayer,
                    stateWithUpToNPiecesInHand(5)
                )
            : s
    )
