import { HasURI } from "@utils/zod/HasURI";
import { z } from "zod";
import { CastleRiskPlayState } from "../common/CastleRiskPlayState";

export const FortifyTerritoriesState = z.object({
  ...HasURI("FortifyTerritoriesState").shape,
  ...CastleRiskPlayState.shape,
});
export type FortifyTerritoriesState = z.infer<typeof FortifyTerritoriesState>;
