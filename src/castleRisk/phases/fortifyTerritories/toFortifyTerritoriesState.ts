import { pipe } from "fp-ts/lib/function"
import { moveToNextPlayer } from "../../players/moveToNextPlayer"
import { stateWithUpToNPiecesInHand } from "../common/stateWithUpToNPiecesInHand"
import { PlaceHiddenArmiesState } from "../hiddenArmies/place/PlaceHiddenArmiesState"
import { FortifyTerritoriesState } from "./FortifyTerritoriesState"
import { new_ } from "@utils/zod/new_"

export const toFortifyTerritoriesState = 
    (state:PlaceHiddenArmiesState):FortifyTerritoriesState =>
    pipe(
        state,
        moveToNextPlayer,
        new_(FortifyTerritoriesState),
        stateWithUpToNPiecesInHand(5),
    )