import { isType } from "@utils/zod/isType";
import { PlacedInTerritory } from "../../pieces/PlacedInTerritory";
import { FortifyTerritoriesState } from "./FortifyTerritoriesState";

export const isPhaseComplete = 
    (state:FortifyTerritoriesState) =>
    state.players.flatMap(p => p.pieces).every(isType(PlacedInTerritory))