import { isType } from "@utils/zod/isType";
import { new_ } from "@utils/zod/new_";
import { Diplomat } from "castleRisk/cards/diplomat/Diplomat";
import { currentPlayerFromState } from "castleRisk/players/currentPlayerFromState";
import { produce } from "immer";
import { CastleRiskPlayState } from "../common/CastleRiskPlayState";
import { PlayDiplomatState } from "./PlayDiplomatState";

export const toPlayDiplomatState = (state: CastleRiskPlayState, cardIndex: number) => {
    const newState = produce(state, (draft) => {
      const currentPlayer = currentPlayerFromState(draft);
      currentPlayer.cards = currentPlayer.cards.map((c, i) =>
        isType(Diplomat)(c) ? (i == cardIndex ? { ...c, isActive: true } : c) : c
      );
    });
    return new_(PlayDiplomatState)(newState);
  };