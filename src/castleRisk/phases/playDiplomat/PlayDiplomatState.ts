import { z } from "zod";
import { HasURI } from "@utils/zod/HasURI";
import { CastleRiskPlayState } from "../common/CastleRiskPlayState";

export const PlayDiplomatState = z.object({
     ...HasURI("PlayDiplomatState").shape,
     ...CastleRiskPlayState.shape,
});
export type PlayDiplomatState = z.infer<typeof PlayDiplomatState>;