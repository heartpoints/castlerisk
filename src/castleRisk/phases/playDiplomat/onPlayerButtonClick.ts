import { isType } from "@utils/zod/isType";
import { new_ } from "@utils/zod/new_";
import { Diplomat } from "castleRisk/cards/diplomat/Diplomat";
import { currentPlayerFromState } from "castleRisk/players/currentPlayerFromState";
import { ID } from "castleRisk/players/ID";
import { produce } from "immer";
import { ChooseActionState } from "../chooseAction/ChooseActionState";
import { CastleRiskPlayState } from "../common/CastleRiskPlayState";

export const onPlayerButtonClick = (playerID: ID, state: CastleRiskPlayState) => {
    return new_(ChooseActionState)(
      produce(state, (draft) => {
        const currentPlayer = currentPlayerFromState(draft);
        currentPlayer.cards = currentPlayer.cards.map((c) =>
          isType(Diplomat)(c) &&
          c.isActive &&
          c.activeAgainstPlayerId == undefined
            ? { ...c, activeAgainstPlayerId: playerID, issuingPlayer: currentPlayer.id }
            : c
        );
      })
    );
  };