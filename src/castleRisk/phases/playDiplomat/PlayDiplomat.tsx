import { Button } from "@mui/material";
import { doNothing } from "@utils/axioms/doNothing";
import { PropsWithSetState } from "@utils/react/PropsWithSetState";
import { CurrentPlayerCards } from "castleRisk/cards/CurrentPlayerCards";
import { diplomatTargetPlayers } from "castleRisk/cards/diplomat/diplomatTargetPlayers";
import { GameCanvas } from "castleRisk/gameCanvas/gameCanvas";
import { currentPlayerFromState } from "castleRisk/players/currentPlayerFromState";
import { SidePanelTemplate } from "../attack/general/SidePanelTemplate";
import { ActionText } from "../common/ActionText";
import { GameGrid } from "../common/GameGrid";
import { PlayerActionButtons } from "../common/PlayerActionButtons";
import { PlayerHeading } from "../common/PlayerHeading";
import { ReserveArmies } from "../common/ReserveArmies";
import { onMouseMove } from "../common/onMouseMove";
import { drawBoard } from "../common/placement/drawBoard";
import { PlayDiplomatState } from "./PlayDiplomatState";
import { onPlayerButtonClick } from "./onPlayerButtonClick";

export const PlayDiplomat = (props: PropsWithSetState<PlayDiplomatState>) => {
  const { setState, ...state } = props;
  const currentPlayer = currentPlayerFromState(state);
  return (
    <GameGrid
      gameCanvas={
        <GameCanvas
          drawGame={drawBoard(state)}
          onMouseMove={(e) => setState(onMouseMove(state)(e))}
          onMouseUp={doNothing}
        />
      }
      sidePanel={
        <SidePanelTemplate
          actionTextContent={
            <p>
              <ActionText>Choose Player for Truce</ActionText> - this player
              will not be able to attack you during their next turn, but be
              aware, you may not attack them during your current turn either.
            </p>
          }
          playerActionButtons={
            <PlayerActionButtons>
              {diplomatTargetPlayers(state).map((p) => (
                <Button
                  variant="outlined"
                  key={p.id}
                  onClick={() => setState(onPlayerButtonClick(p.id, state))}
                >
                  {p.name}
                </Button>
              ))}
            </PlayerActionButtons>
          }
          playerHeading={<PlayerHeading {...state} />}
          reserveArmies={<ReserveArmies {...state} />}
        />
      }
      cards={<CurrentPlayerCards {...{ setState, ...state }} />}
    />
  );
};
