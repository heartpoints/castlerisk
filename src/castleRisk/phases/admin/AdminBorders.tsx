import { useState } from "react";
import { loadBackground } from "../../../utils/canvas/loadBackground";
import { GameCanvas } from "../../gameCanvas/gameCanvas";
import { castleRiskBoardBackgroundPath } from "../../images/castleRiskBoardBackgroundPath";
import { onMouseMove } from "../common/onMouseMove";
import { adminHeader } from "./adminHeader";
import { onMouseUp } from "./onMouseUp";
import { rightPanel } from "./rightPanel";
import { updateAdminCanvas } from "./updateAdminCanvas";
import { useAsyncEffect } from "./useAsyncEffect";

export const AdminBorders = (state: any) => {
  const { setState, admin } = state;
  const rightSection = (
    <div>
      {adminHeader}
      {rightPanel(admin)(setState)}
    </div>
  );

  const [background, setBackground] = useState<HTMLImageElement>();

  useAsyncEffect(async () => {
    const background = await loadBackground(castleRiskBoardBackgroundPath);
    setBackground(background);
  });

  if (!background) return <h1>Loading</h1>;

  return (
    <div>
      <GameCanvas
        drawGame={(canvas) => updateAdminCanvas(canvas)(state)(background)}
        onMouseMove={(e) => setState(onMouseMove(state)(e))}
        onMouseUp={onMouseUp(admin)(setState)}
      />
      {rightSection}
    </div>
  );
};
