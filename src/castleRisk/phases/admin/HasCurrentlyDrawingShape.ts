import { Shape } from "@utils/geometry/Shape";

import { z } from "zod";

export const HasCurrentlyDrawingShape = z.object({ currentlyDrawingShape: Shape });
export type HasCurrentlyDrawingShape = z.infer<typeof HasCurrentlyDrawingShape>;
