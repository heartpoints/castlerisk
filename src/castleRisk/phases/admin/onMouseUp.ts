import { OnCanvasMouseEventHandler } from '../../../utils/canvas/OnCanvasMouseEventHandler'
import { Point } from '../../../utils/geometry/Point'
import { HasCurrentlyDrawingShape } from "./HasCurrentlyDrawingShape"
import { overlappingPointsWithinAdminShapes } from './overlappingPointsWithinAdminShapes'

export const onMouseUp = 
    (admin:HasCurrentlyDrawingShape) =>
    (setState: any): OnCanvasMouseEventHandler => 
    (e:Point) => {
        const overlappingPoints = overlappingPointsWithinAdminShapes(e)
        const newPoint = overlappingPoints.length > 0 ? overlappingPoints[0] : e
        return setState(state => ({
            ...state,
            admin: {
                ...admin,
                currentlyDrawingShape: [...admin.currentlyDrawingShape, newPoint],
            }
        }))
    }
    
