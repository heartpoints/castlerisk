import { HasMousePoint } from "../common/HasMousePoint";

import { HasURI } from "@utils/zod/HasURI";
import { z } from "zod";

import { HasAdmin } from "./HasAdmin";

export const AdminState = z.object({
  ...HasURI("AdminState").shape,
  ...HasMousePoint.shape,
  ...HasAdmin.shape,
});
export type AdminState = z.infer<typeof AdminState>;
