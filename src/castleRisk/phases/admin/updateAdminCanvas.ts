import { clearCanvas } from '../../../utils/canvas/clearCanvas';
import { drawImage } from '../../../utils/canvas/drawImage';
import { sequentially } from '../../../utils/composition/sequentially';
import { drawTerritoryNames } from '../../territories/drawTerritoryNames';
import { drawTerritoryShapes } from '../../territories/drawTerritoryShapes';
import { drawAdminStuff } from './drawAdminStuff';

export const updateAdminCanvas = 
    (canvas: CanvasRenderingContext2D) =>
    (state: any) =>
    (background: HTMLImageElement) =>
    {
        return sequentially([
            clearCanvas,
            drawImage(background)(0)(0),
            drawTerritoryShapes(state.mousePoint),
            drawTerritoryNames,
            drawAdminStuff(state),
        ])(canvas)
    }
    
