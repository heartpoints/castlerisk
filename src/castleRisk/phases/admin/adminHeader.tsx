import { PhaseTitle } from "../common/PhaseTitle";

export const adminHeader = 
    <>
        <PhaseTitle>Administer Borders</PhaseTitle>
        <div>
            Not for use in-game, but rather to identify the borders of a given map and generate the data structure
            to be used for the geometric portion of the game logic.
        </div>
    </>
