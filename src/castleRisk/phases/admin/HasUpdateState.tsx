import { UpdateStateOf } from "./UpdateStateOf";

export type HasUpdateState<T> = {
    updateState: UpdateStateOf<T>;
};
