import { activelyDrawingFillStyle } from '../../config/activelyDrawingFillStyle'
import { activelyDrawingPointStyle } from '../../config/activelyDrawingPointStyle'
import { drawPoint } from '../../../utils/canvas/drawPoint'
import { drawShape } from '../../../utils/canvas/drawShape'
import { overlappingPointsWithinAdminShapes } from './overlappingPointsWithinAdminShapes'
import { green } from '../../../utils/colors/green';
import { sequentially } from '../../../utils/composition/sequentially'
// import { CastleRiskState } from '../common/CastleRiskState'

export const drawAdminStuff = 
    (state: any) => 
    {
        const { admin, mousePoint } = state
        const { currentlyDrawingShape } = admin
        const overlappingPoints = overlappingPointsWithinAdminShapes(mousePoint)
        const drawAllGreenPoints = overlappingPoints.map(drawPoint(green))

        return sequentially([
            drawShape(currentlyDrawingShape)(activelyDrawingPointStyle)(activelyDrawingFillStyle),
            ...drawAllGreenPoints
        ])
    }
