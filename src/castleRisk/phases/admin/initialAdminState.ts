import { new_ } from "@utils/zod/new_";
import { originPoint } from "../../../utils/geometry/originPoint";
import { AdminState } from "./AdminState";

export const initialAdminState = () =>
  new_(AdminState)({
    admin: {
      currentlyDrawingShape: [],
    },
    mousePoint: originPoint,
  });
