import { HasCurrentlyDrawingShape } from "./HasCurrentlyDrawingShape";
import { instructions } from './instructions';

export const rightPanel = 
    (admin: HasCurrentlyDrawingShape) => 
    (updateState: any) => 
    <>
        {instructions}
        <textarea 
            style={{width: "100%", height: "30vh"}}
            onChange={e => updateState({ admin: { ...admin, currentlyDrawingShape: JSON.parse(e.target.value) } })}
            value={JSON.stringify(admin.currentlyDrawingShape, null, 3)}
        />
    </>
