import { z } from "zod";
import { HasCurrentlyDrawingShape } from "./HasCurrentlyDrawingShape";

export const HasAdmin = z.object({ admin: HasCurrentlyDrawingShape });
export type HasAdmin = z.infer<typeof HasAdmin>;
