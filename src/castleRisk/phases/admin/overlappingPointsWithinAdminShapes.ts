import { Point } from '../../../utils/geometry/Point';
import { pointsOverlap } from '../../../utils/geometry/pointsOverlap';
import { listFromArray } from '../../../utils/list/listFromArray';
import { territories } from '../../map/territories';

export const overlappingPointsWithinAdminShapes = 
    (mousePoint: Point) => {
        const allPoints = listFromArray(territories().map(t => t.boundary)).flatMap(listFromArray);
        return allPoints.where(pointsOverlap(mousePoint)).asArray;
    };
