import { currentPlayerFromState } from "../../players/currentPlayerFromState";
import { moveToNextPlayer } from "../../players/moveToNextPlayer";
import { CastleRiskPlayState } from "../common/CastleRiskPlayState";
import { canMovePieceToUnoccupiedTerritory } from "./canMovePieceToUnoccupiedTerritory";

export const moveToNextPlayerThatCanMovePieceToUnoccupiedTerritory = <T extends CastleRiskPlayState>(state: T) => {
  const nextState = moveToNextPlayer(state);
  const nextPlayer = currentPlayerFromState(nextState);
  return canMovePieceToUnoccupiedTerritory(state.players)(nextPlayer)
    ? nextState
    : moveToNextPlayerThatCanMovePieceToUnoccupiedTerritory(nextState);
};
