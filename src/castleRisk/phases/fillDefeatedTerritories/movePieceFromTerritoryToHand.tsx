import { new_ } from "@utils/zod/new_";
import { replaceFirst } from "../../../utils/arrays/replaceFirst";
import { isPlacedTerritory } from "../../pieces/isPlacedTerritory";
import { PlacedInHand } from "../../pieces/PlacedInHand";
import { currentPlayerFromState } from "../../players/currentPlayerFromState";
import { playerEquals } from "../../players/playerEquals";
import { Territory } from "../../territories/Territory";
import { FillDefeatedTerritoriesState } from "./FillDefeatedTerritoriesState";

export const movePieceFromTerritoryToHand = (
  state: FillDefeatedTerritoriesState,
  territory: Territory
) => ({
  ...state,
  players: state.players.map((player) =>
    playerEquals(player)(currentPlayerFromState(state))
      ? {
          ...player,
          pieces: replaceFirst(
            player.pieces,
            isPlacedTerritory(territory),
            new_(PlacedInHand)
          ),
        }
      : player
  ),
});
