import { new_ } from "@utils/zod/new_";
import { Player } from "../../players/Player";
import { ChooseActionState } from "../chooseAction/ChooseActionState";
import { PlayerDefeatedState } from "../playerDefeated/PlayerDefeatedState";
import { canMovePieceToUnoccupiedTerritory } from "./canMovePieceToUnoccupiedTerritory";
import { FillDefeatedTerritoriesState } from "./FillDefeatedTerritoriesState";
import { moveToNextPlayerAndPlaceReserveArmyInHand } from "./moveToNextPlayerAndPlaceReserveArmyInHand";
import { moveToNextPlayerThatCanMovePieceToUnoccupiedTerritory } from "./moveToNextPlayerThatCanMovePieceToUnoccupiedTerritory";
import { nobodyCanMovePiecesToUnoccupiedTerritory } from "./nobodyCanMovePiecesToUnoccupiedTerritory";

export const toFillDefeatedTerritories = (
  state: PlayerDefeatedState | FillDefeatedTerritoriesState,
  winningPlayer: Player
): FillDefeatedTerritoriesState | ChooseActionState => {
  const { players } = state;
  const nextState = canMovePieceToUnoccupiedTerritory(players)(winningPlayer)
    ? state
    : nobodyCanMovePiecesToUnoccupiedTerritory(players)
    ? moveToNextPlayerAndPlaceReserveArmyInHand(state)
    : moveToNextPlayerThatCanMovePieceToUnoccupiedTerritory(state);
  return new_(FillDefeatedTerritoriesState)({ ...nextState, winningPlayer });
};
