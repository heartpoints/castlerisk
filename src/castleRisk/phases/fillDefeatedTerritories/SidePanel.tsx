import { Alert } from "@mui/material";
import { PropsWithSetState } from "../../../utils/react/PropsWithSetState";
import { isPieceInSomePlayersHand } from "../../pieces/isPieceInSomePlayersHand";
import { currentPlayerFromState } from "../../players/currentPlayerFromState";
import { SideNote } from "../chooseAction/SideNote";
import { ActionText } from "../common/ActionText";
import { PlayerHeading } from "../common/PlayerHeading";
import { ReserveArmies } from "../common/ReserveArmies";
import { SidePanelContainer } from "../common/SidePanelContainer";
import { FillDefeatedTerritoriesState } from "./FillDefeatedTerritoriesState";

export const SidePanel = ({
  setState,
  ...state
}: PropsWithSetState<FillDefeatedTerritoriesState>) => {
  const currentPlayer = currentPlayerFromState(state);
  const { defeatedPlayer } = state;
  return (
    <SidePanelContainer>
      <PlayerHeading {...state} />
      <Alert severity="warning" variant="filled">
        {currentPlayer.name} has defeated {defeatedPlayer.name}!
      </Alert>
      <p>
        <ActionText>Fill Defeated Territories</ActionText> -{" "}
        {isPieceInSomePlayersHand(state)
          ? "Select an empty territory to place your piece"
          : "Select a territory you occupy, from which you will move one army"}
      </p>
      <ReserveArmies {...state} />
      <SideNote>
        NOTE: All empty territories (outlined in white) must be filled
      </SideNote>
    </SidePanelContainer>
  );
};
