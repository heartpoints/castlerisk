import { segmentsFromShape } from "../../../utils/geometry/segmentsFromShape";
import { unoccupiedTerritories } from "../../territories/unoccupiedTerritories";
import { drawLayers } from "../common/drawLayers";
import { FillDefeatedTerritoriesState } from "./FillDefeatedTerritoriesState";
import { activeTerritories } from "./activeTerritories";
import { CanvasEffect } from "../../../utils/canvas/CanvasEffect";

export const drawBoard = (state: FillDefeatedTerritoriesState): CanvasEffect =>
  drawLayers({
    activeTerritories: activeTerritories(state),
    highlightedLineSegments: unoccupiedTerritories(state).flatMap((t) =>
      segmentsFromShape(t.boundary)
    ),
    shouldDrawHighlight: true,
    state,
  });
