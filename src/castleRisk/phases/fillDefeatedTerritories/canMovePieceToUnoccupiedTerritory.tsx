import { Player } from "../../players/Player";
import { Players } from "../../players/Players";
import { territoriesWith2OrMorePieces } from "../common/territoriesWith2OrMorePieces";

export const canMovePieceToUnoccupiedTerritory = (players: Players) => (player: Player) => territoriesWith2OrMorePieces(players)(player).length > 0;
