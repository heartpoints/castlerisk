import { unoccupiedTerritories } from "../../territories/unoccupiedTerritories";
import { territoriesWith2OrMorePieces } from "../common/territoriesWith2OrMorePieces";
import { FillDefeatedTerritoriesState } from "./FillDefeatedTerritoriesState";
import { isPieceInSomePlayersHand } from "../../pieces/isPieceInSomePlayersHand";
import { currentPlayerFromState } from "../../players/currentPlayerFromState";

export const activeTerritories = (state: FillDefeatedTerritoriesState) =>
  isPieceInSomePlayersHand(state)
    ? unoccupiedTerritories(state)
    : territoriesWith2OrMorePieces(state.players)(currentPlayerFromState(state));
