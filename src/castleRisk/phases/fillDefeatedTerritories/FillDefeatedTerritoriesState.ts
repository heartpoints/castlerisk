import { HasURI } from "@utils/zod/HasURI";
import { z } from "zod";
import { CastleRiskPlayState } from "../common/CastleRiskPlayState";
import { HasDefeatedPlayer } from "../playerDefeated/HasDefeatedPlayer";
import { HasWinningPlayer } from "../playerDefeated/HasWinningPlayer";

export const FillDefeatedTerritoriesState = z.object({
  ...HasURI("FillDefeatedTerritoriesState").shape,
  ...CastleRiskPlayState.shape,
  ...HasWinningPlayer.shape,
  ...HasDefeatedPlayer.shape,
});

export type FillDefeatedTerritoriesState = z.infer<
  typeof FillDefeatedTerritoriesState
>;
