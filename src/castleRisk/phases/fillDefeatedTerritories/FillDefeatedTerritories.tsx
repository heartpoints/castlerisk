import { PropsWithSetState } from "../../../utils/react/PropsWithSetState";
import { CardContainer } from "../../cards/CardContainer";
import { GameCanvas } from "../../gameCanvas/gameCanvas";
import { isPieceInSomePlayersHand } from "../../pieces/isPieceInSomePlayersHand";
import { movePieceFromCurrentPlayerHandToTerritory } from "../../pieces/movePieceFromCurrentPlayerHandToTerritory";
import { CurrentPlayerCards } from "../../cards/CurrentPlayerCards";
import { currentlyHoveredActiveTerritory } from "../common/currentlyHoveredActiveTerritory";
import { GameGrid } from "../common/GameGrid";
import { onMouseMove } from "../common/onMouseMove";
import { activeTerritories } from "./activeTerritories";
import { drawBoard } from "./drawBoard";
import { FillDefeatedTerritoriesState } from "./FillDefeatedTerritoriesState";
import { movePieceFromTerritoryToHand } from "./movePieceFromTerritoryToHand";
import { SidePanel } from "./SidePanel";
import { toFillDefeatedOrChooseNextActionState } from "./toFillDefeatedOrChooseNextActionState";

export const FillDefeatedTerritories = ({
  setState,
  ...state
}: PropsWithSetState<FillDefeatedTerritoriesState>) => (
  <GameGrid
    gameCanvas={
      <GameCanvas
        drawGame={drawBoard(state)}
        onMouseMove={(e) => setState(onMouseMove(state)(e))}
        onMouseUp={() =>
          currentlyHoveredActiveTerritory(state.mousePoint)(
            activeTerritories(state)
          ).map((territory) =>
            setState(
              isPieceInSomePlayersHand(state)
                ? toFillDefeatedOrChooseNextActionState(
                    movePieceFromCurrentPlayerHandToTerritory(territory)(state),
                    state.winningPlayer
                  )
                : movePieceFromTerritoryToHand(state, territory)
            )
          )
        }
      />
    }
    sidePanel={<SidePanel {...{ setState, ...state }} />}
    cards={<CurrentPlayerCards {...{setState, ...state}} />}
  />
);
