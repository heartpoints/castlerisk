import { new_ } from "@utils/zod/new_";
import { indexOf } from "../../../utils/arrays/indexOf";
import { Player } from "../../players/Player";
import { playerEquals } from "../../players/playerEquals";
import { unoccupiedTerritories } from "../../territories/unoccupiedTerritories";
import { ChooseActionState } from "../chooseAction/ChooseActionState";
import { PlayerDefeatedState } from "../playerDefeated/PlayerDefeatedState";
import { FillDefeatedTerritoriesState } from "./FillDefeatedTerritoriesState";
import { toFillDefeatedTerritories } from "./toFillDefeatedTerritories";

export const toFillDefeatedOrChooseNextActionState = (
  state: PlayerDefeatedState | FillDefeatedTerritoriesState,
  winningPlayer: Player
) => {
  return unoccupiedTerritories(state).length === 0
    ? new_(ChooseActionState)({
        ...state,
        currentPlayerIndex: indexOf(state.players, playerEquals(winningPlayer)),
      })
    : toFillDefeatedTerritories(state, winningPlayer);
};
