import { Players } from "../../players/Players";
import { canMovePieceToUnoccupiedTerritory } from "./canMovePieceToUnoccupiedTerritory";

export const nobodyCanMovePiecesToUnoccupiedTerritory = (players: Players) => !players.some(canMovePieceToUnoccupiedTerritory(players));
