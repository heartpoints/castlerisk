import { isType } from "@utils/zod/isType";
import { moveNReservePiecesToHand } from "../../pieces/moveNReservePiecesToHand";
import { PlacedInReserves } from "../../pieces/PlacedInReserves";
import { currentPlayerFromState } from "../../players/currentPlayerFromState";
import { moveToNextPlayer } from "../../players/moveToNextPlayer";
import { playerEquals } from "../../players/playerEquals";
import { CastleRiskPlayState } from "../common/CastleRiskPlayState";

export const moveToNextPlayerAndPlaceReserveArmyInHand = <T extends CastleRiskPlayState>(
  state: T
) => {
  const nextState = moveToNextPlayer(state);
  const nextPlayer = currentPlayerFromState(nextState);
  return {
    ...nextState,
    players: nextState.players.map((player) => playerEquals(nextPlayer)(player)
      ? {
        ...player,
        pieces: moveNReservePiecesToHand(
          player.pieces.filter(isType(PlacedInReserves))
        )(1),
      }
      : player
    ),
  };
};
