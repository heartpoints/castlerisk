import { dottedLineConnections } from "./dottedLineConnections";
import { Territory } from "./Territory";
import { territoryByName } from "./territoryByName";

export const territoriesDottedLinedTo = (territory: Territory) =>
  dottedLineConnections
    .filter(([start, end]) => start === territory.name || end === territory.name)
    .map(([start, end]) =>
      territoryByName(start === territory.name ? end : start)!
    );
