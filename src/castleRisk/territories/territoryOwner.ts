import { Player } from "../players/Player"
import { Players } from "../players/Players"
import { playerIndexForTerritory } from "./playerIndexForTerritory"
import { Territory } from "./Territory"

export const playerForTerritory = 
    (players:Players) =>
    (territory:Territory):Player | undefined =>
    players[playerIndexForTerritory(players)(territory)]
