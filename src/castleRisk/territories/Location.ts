import { Boundary } from "../../utils/geometry/Boundary";

import { z } from "zod";

export const Location = z.object({ boundary: Boundary });
export type Location = z.infer<typeof Location>;
