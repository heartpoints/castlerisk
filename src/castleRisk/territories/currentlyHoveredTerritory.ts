import { pointInShape } from '../../utils/geometry/pointInShape';
import { Point } from '../../utils/geometry/Point';
import { listOfTerritories } from './listOfTerritories';

export const currentlyHoveredTerritory = 
    (mousePoint: Point) => 
    listOfTerritories.first(t => pointInShape(mousePoint)(t.boundary));
