import { placedPiecesForPlayer } from "../pieces/placedPieces";
import { Players } from "../players/Players";
import { Territory } from "./Territory";

export const playerIndexForTerritory =
  (players: Players) => (territory: Territory) =>
    players.findIndex((p) =>
      placedPiecesForPlayer(p).some((p) => p.territoryName === territory.name)
    )!;
