import { Territory } from "./Territory";

import { z } from "zod";

export const Territories = z.array(Territory);
export type Territories = z.infer<typeof Territories>;
