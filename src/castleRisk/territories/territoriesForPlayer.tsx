import { territoryNameEquals } from "./territoryNameEquals"
import { territories } from "../map/territories"
import { Player } from "../players/Player"
import { placedPiecesForPlayer } from "../pieces/placedPieces"
import { withValue } from "../../utils/axioms/withValue"

export const territoriesForPlayer = 
    (currentPlayer:Player) => 
    withValue
        (placedPiecesForPlayer(currentPlayer))
        (
            placedPieces =>
            territories().filter(t => placedPieces.some(territoryNameEquals(t.name)))
        )
    