import { not } from 'fp-ts/lib/Predicate';
import { territoryNameExists } from './territoryNameExists';

export const noTerritoryName = not(territoryNameExists);
