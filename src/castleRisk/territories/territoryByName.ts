import { flow } from "fp-ts/lib/function";
import { memoize } from "../../utils/memoize";
import { territories } from "../map/territories";
import { getName } from "../../utils/axioms/getName";
import { equals } from "../../utils/axioms/equals";

export const territoryByName = 
    memoize(
        (territoryName?: string) => 
        territories().find(flow(getName, equals(territoryName)))!
    )
