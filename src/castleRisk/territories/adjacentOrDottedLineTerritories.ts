import { Territory } from "./Territory";
import { territories } from "../map/territories";
import { shapesHaveOneOrMoreCommonPoints } from "../../utils/geometry/shapesHaveOneOrMoreCommonPoints";
import { territoriesDottedLinedTo } from "./territoriesDottedLinedTo";

export const adjacentOrDottedLineTerritories = (territory: Territory) => [
  ...territories().filter((t) =>
    shapesHaveOneOrMoreCommonPoints(t.boundary)(territory.boundary)
  ),
  ...territoriesDottedLinedTo(territory),
];
