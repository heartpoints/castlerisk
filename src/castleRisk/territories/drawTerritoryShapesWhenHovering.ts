import { sequentially } from '../../utils/composition/sequentially';
import { Territory } from './Territory';
import { adjacentOrDottedLineTerritories } from "./adjacentOrDottedLineTerritories";
import { drawAdjacentTerritories } from './drawAdjacentTerritories';
import { drawCurrentlyHoveredTerritory } from './drawCurrentlyHoveredTerritory';
import { drawNonNearbyTerritories } from './drawNonNearbyTerritories';

export const drawTerritoryShapesWhenHovering = 
    (currentTerritory: Territory) => 
    {
        const adjacents = adjacentOrDottedLineTerritories(currentTerritory)
        return sequentially([
            drawNonNearbyTerritories(adjacents)(currentTerritory),
            drawAdjacentTerritories(adjacents),
            drawCurrentlyHoveredTerritory(currentTerritory)
        ])
    };
