import { Territory } from './Territory'
import { territoryNameEquals } from './territoryNameEquals'
import { allPlacedPiecesForPlayers } from '../pieces/allPlacedPiecesForPlayers'
import { Players } from '../players/Players'

export const isTerritoryOccupiedByPlayers = 
    (players: Players) => 
    (t: Territory) => 
    allPlacedPiecesForPlayers(players).some(territoryNameEquals(t.name))
