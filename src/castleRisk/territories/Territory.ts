import { z } from "zod";
import { HasName } from "../../utils/axioms/HasName";
import { Location } from "./Location";

export const Territory = z.object({
  ...HasName.shape,
  ...Location.shape,
});
export type Territory = z.infer<typeof Territory>;
