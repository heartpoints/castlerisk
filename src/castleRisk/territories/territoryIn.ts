import { Territories } from './Territories';
import { Territory } from './Territory';
import { territoryEquals } from './territoryEquals';

export const territoryIn = 
    (territories: Territories) => 
    (territory: Territory) => 
    territories.some(territoryEquals(territory));
