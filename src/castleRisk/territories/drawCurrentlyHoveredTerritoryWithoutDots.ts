import { drawPolygon } from '../../utils/canvas/drawPolygon';
import { white } from '../../utils/colors/white';
import { Territory } from './Territory';
import { territoryHoverColor } from './territoryHoverColor';


export const drawCurrentlyHoveredTerritoryWithoutDots = (t: Territory) => drawPolygon(white)(territoryHoverColor(t))(t.boundary);
