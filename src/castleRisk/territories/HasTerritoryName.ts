import { Name } from "@utils/axioms/Name";
import { z } from "zod";

export const HasTerritoryName = z.object({ territoryName: Name });
export type HasTerritoryName = z.infer<typeof HasTerritoryName>;
