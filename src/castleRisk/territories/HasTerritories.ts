import { Territories } from "./Territories";

import { z } from "zod";

export const HasTerritories = z.object({ territories: Territories });
export type HasTerritories = z.infer<typeof HasTerritories>;