import { empireContaining } from '../empires/empireContaining';
import { Territory } from './Territory';
import { drawTerritoryWithoutDots } from './drawTerritoryWithoutDots';

export const drawNormalTerritory = 
    (t: Territory) => 
    drawTerritoryWithoutDots(empireContaining(t).color)(t.boundary);

    