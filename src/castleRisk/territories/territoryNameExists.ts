import { isNotUndefined } from '../../utils/axioms/isNotUndefined';
import { getTerritoryName } from './getTerritoryName';
import { flow } from 'fp-ts/lib/function';

export const territoryNameExists = flow(getTerritoryName, isNotUndefined);
