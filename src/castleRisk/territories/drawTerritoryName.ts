import { drawText } from "../../utils/canvas/drawText";
import { centroidOfPolygon } from "../../utils/geometry/centroidOfPolygon";
import { RGBAColor } from "../../utils/colors/RGBAColor";
import { Territory } from "./Territory";
import { centeredTextPoint } from "../../utils/canvas/centeredTextPoint";

export const drawTerritoryName =
  (color: RGBAColor) => (t: Territory) => (context: CanvasRenderingContext2D) =>
    drawText(true)(color)(
      centeredTextPoint(centroidOfPolygon(t.boundary), t.name, context)
    )(t.name)(context);
