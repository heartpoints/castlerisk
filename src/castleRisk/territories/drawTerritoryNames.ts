import { white } from "../../utils/colors/white";
import { sequentially } from "../../utils/composition/sequentially";
import { territories } from '../map/territories';
import { drawTerritoryName } from "./drawTerritoryName";

export const drawTerritoryNames = 
    sequentially(territories().map(drawTerritoryName(white)))