import { drawPolygon } from "../../utils/canvas/drawPolygon";
import { black } from "../../utils/colors/black";

export const drawTerritoryWithoutDots = drawPolygon(black);
