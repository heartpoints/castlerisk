import { HasTerritoryName } from './HasTerritoryName';

export const getTerritoryName = (t: Partial<HasTerritoryName>) => t.territoryName;
