import { listFromArray } from '../../utils/list/listFromArray';
import { territories } from '../map/territories';

export const listOfTerritories = listFromArray(territories());
