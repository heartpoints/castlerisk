import { sequentially } from "../../utils/composition/sequentially";
import { Territories } from "./Territories";
import { drawAdjacentTerritory } from "./drawAdjacentTerritory";

export const drawAdjacentTerritories = 
    (adjacentTerritories: Territories) => 
    sequentially(adjacentTerritories.map(drawAdjacentTerritory))
