import { Player } from "../players/Player";
import { playerEquals } from "../players/playerEquals";
import { Players } from "../players/Players";
import { Territory } from "./Territory";
import { playerForTerritory } from "./territoryOwner";

export const territoryOwnedByPlayer =
  (players: Players) => (player: Player) => (territory: Territory) => {
    const owningPlayer = playerForTerritory(players)(territory);
    return owningPlayer && playerEquals(owningPlayer)(player);
  };
