import { lightenColor } from '../../utils/colors/lightenColor';
import { empireContaining } from '../empires/empireContaining';
import { Territory } from './Territory';

export const territoryHoverColor = (t: Territory) => lightenColor(0.75)(empireContaining(t).color);
