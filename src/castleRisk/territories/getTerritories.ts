import { prop } from "../../utils/axioms/prop";
import { HasTerritories } from "./HasTerritories";

export const getTerritories = prop<HasTerritories>()('territories')