import { not } from "../../utils/predicates/not";
import { territories } from "../map/territories";
import { isTerritoryOccupiedByPlayers } from "./isTerritoryOccupiedByPlayers";
import { CastleRiskPlayState } from "../phases/common/CastleRiskPlayState";

export const unoccupiedTerritories = ({ players }: CastleRiskPlayState) =>
  territories().filter(not(isTerritoryOccupiedByPlayers(players)));
