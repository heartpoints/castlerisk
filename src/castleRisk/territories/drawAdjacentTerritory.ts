import { drawShapeWithPoints } from '../../utils/canvas/drawShapeWithPoints';
import { gray } from '../../utils/colors/gray';
import { lightenColor } from '../../utils/colors/lightenColor';
import { Territory } from './Territory';
import { empireContaining } from '../empires/empireContaining';

export const drawAdjacentTerritory = 
    (territory: Territory) => 
    (context: CanvasRenderingContext2D) => 
    drawShapeWithPoints
        (gray)
        (gray)
        (lightenColor(0.5)
        (empireContaining(territory).color))
        (territory.boundary)(context);
