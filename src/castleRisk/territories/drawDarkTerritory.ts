import { darkenColor } from '../../utils/colors/darkenColor';
import { empireContaining } from '../empires/empireContaining';
import { Territory } from './Territory';
import { drawTerritoryWithoutDots } from './drawTerritoryWithoutDots';

export const drawDarkTerritory = 
    (t: Territory) => 
    drawTerritoryWithoutDots(darkenColor(0.4)(empireContaining(t).color))(t.boundary)