import { drawShapeWithPoints } from '../../utils/canvas/drawShapeWithPoints';
import { white } from '../../utils/colors/white';
import { Territory } from './Territory';
import { territoryHoverColor } from './territoryHoverColor';

export const drawCurrentlyHoveredTerritory = 
    (t: Territory) => 
    drawShapeWithPoints
        (white)
        (white)
        (territoryHoverColor(t))
        (t.boundary)
