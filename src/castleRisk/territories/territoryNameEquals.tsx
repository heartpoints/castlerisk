import { flow } from "fp-ts/lib/function";
import { getTerritoryName } from "./getTerritoryName";
import { equals } from "../../utils/axioms/equals";

export const territoryNameEquals = (name: string) => flow(getTerritoryName, equals(name));
