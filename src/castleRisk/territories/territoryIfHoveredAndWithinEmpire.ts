import { currentlyHoveredTerritory } from "./currentlyHoveredTerritory"
import { Empire } from "../empires/Empire"
import { Point } from "../../utils/geometry/Point"
import { territoryIn } from "./territoryIn"

export const territoryIfHoveredAndWithinEmpire = 
    (mousePoint: Point) =>
    ({ territories }: Empire) => 
    currentlyHoveredTerritory(mousePoint).if(territoryIn(territories))
