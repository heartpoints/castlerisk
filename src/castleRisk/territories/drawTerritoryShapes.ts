import { sequentially } from '../../utils/composition/sequentially';
import { Point } from '../../utils/geometry/Point';
import { territories } from '../map/territories';
import { currentlyHoveredTerritory } from './currentlyHoveredTerritory';
import { drawNormalTerritory } from './drawNormalTerritory';
import { drawTerritoryShapesWhenHovering } from './drawTerritoryShapesWhenHovering';

export const drawTerritoryShapes = 
    (mousePoint: Point) => 
    currentlyHoveredTerritory(mousePoint).mapOrDo(
        drawTerritoryShapesWhenHovering, 
        () => sequentially(territories().map(drawNormalTerritory))
    )
