
export const dottedLineConnections = [
    ["Burgundy", "London"],
    ["Brittany", "London"],
    ["Wales", "Ireland"],
    ["Ireland", "Scotland"],
    ["Scotland", "Norway"],
    ["Norway", "Denmark"],
    ["Denmark", "Sweden"],
    ["Sweden", "Finland"],
    ["Finland", "Livonia"],
    ["Naples", "Montenegro"],
];
