import { sequentially } from '../../utils/composition/sequentially';
import { territories } from '../map/territories';
import { Territories } from './Territories';
import { Territory } from './Territory';
import { drawNormalTerritory } from './drawNormalTerritory';
import { notInTerritories } from './notInTerritories';

export const drawNonNearbyTerritories = 
    (adjacentTerritories: Territories) => 
    (currentTerritory: Territory) => 
    {
        const territoriesOfInterest = [...adjacentTerritories, currentTerritory];
        const otherTerritories = territories().filter(notInTerritories(territoriesOfInterest));
        return sequentially(otherTerritories.map(drawNormalTerritory))
    };
