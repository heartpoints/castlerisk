import { Empire } from '../empires/Empire';
import { territories } from '../map/territories';
import { notInTerritories } from './notInTerritories';

export const outsideTerritories = (empire: Empire) => territories().filter(notInTerritories(empire.territories))
