import { None } from "./None";
import { Some } from "./Some";
import { Maybe } from "./MaybeType";
export const maybeIf = <T>(predicate: boolean, valueIfTrue: T): Maybe<T> => predicate ? Some(valueIfTrue) : None;
