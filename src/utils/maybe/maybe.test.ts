import { add3ToEvens } from "../testing/add3ToEvens";
import { doubleOdds } from "../testing/doubleOdds";
import { Consumer } from "../axioms/Consumer";
import { Provider } from "../axioms/Provider";
import { Some } from "./Some";
import { reduceMaybe } from "./reduceMaybe";

const describeSubject = <T>(subjectProvider:Provider<T>, callback:Consumer<T>) => describe(`when subject is ${subjectProvider.toString().substring(6)}`, () => callback(subjectProvider()))

const itIsEqualTo = (expectedValue:any) => (actualValue:any) => it(`itis equal to ${expectedValue}`, () => expect(actualValue).toEqual(expectedValue))

describe("Maybe", () => {
    describeSubject(
        () => Some(5).flatMap(doubleOdds).flatMap(add3ToEvens).valueOrDefault(2),
        itIsEqualTo(13)
    )
    describeSubject(
        () => reduceMaybe(5, doubleOdds, add3ToEvens, doubleOdds, add3ToEvens).valueOrDefault(2),
        itIsEqualTo(29)
    )
})
