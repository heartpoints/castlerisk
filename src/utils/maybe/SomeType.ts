import { Maybe } from "./MaybeType";

export type SomeType<T> = Maybe<T> & {
    value:T
}