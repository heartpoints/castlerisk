import { None } from "./None";
import { Some } from "./Some";
import { Maybe } from "./MaybeType";
export const If = (predicate: Boolean): Maybe<boolean> => predicate ? Some(true) : None;
