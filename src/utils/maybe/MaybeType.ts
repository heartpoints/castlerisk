import { Consumer } from "../axioms/Consumer";
import { Mapper } from "../axioms/Mapper";
import { Provider } from "../axioms/Provider";
import { Predicate } from "../predicates/Predicate";
import { MaybeFlatmapper } from "./MaybeFlatmapper";

export interface Maybe<T = any> {
  type: "Some" | "None";
  if(predicate: Predicate<T>): Maybe<T>;
  do(f: Consumer<T>): Maybe<T>;
  map<S>(f: Mapper<T, S>): Maybe<S>;
  mapOrDefault<S, R>(f: Mapper<T, S>, r: R): S | R;
  mapOrDo<S, R>(f: Mapper<T, S>, defaultProvider: Provider<R>): S | R;
  flatMap<S>(f: MaybeFlatmapper<T, S>): Maybe<S>;
  valueOrDefault<S>(someDefault: S): T | S;
  valueOr<S>(defaultProducer: Provider<S>): T | S;
  value: T;
  ifElse<S, R>(valueIfSomeObject: S, valueIfNone: R): S | R;
}
