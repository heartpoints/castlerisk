import { Provider } from "../axioms/Provider";
import { NoneType } from "./NoneType";
import { produceNone } from "./produceNone";

export const None: NoneType = {
    type: "None",
    do: produceNone,
    map: produceNone,
    flatMap: produceNone,
    mapOrDefault: (_, d) => d,
    mapOrDo: (f, d) => d(),
    get value(): never { throw new Error("Cannot get value for type none"); }, //todo: remove when ready to handle all the breaking stuff in restguru!
    valueOrDefault: <S>(someDefault: S): S => someDefault,
    ifElse: (_, valueIfNone) => valueIfNone,
    valueOr: <T>(defaultProducer:Provider<T>) => defaultProducer(),
    if: produceNone,
}