import { maybe } from "./maybe";
import { Maybe } from "./MaybeType";
import { Dictionary } from "../hashing/Dictionary";

export const maybeValueForKey = 
    <T>(obj: Dictionary<T>) => 
    (key: string): Maybe<T> => 
    maybe(obj[key]);
