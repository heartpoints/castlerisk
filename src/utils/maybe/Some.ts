import { Predicate } from "../predicates/Predicate";
import { MaybeFlatmapper } from "./MaybeFlatmapper";
import { Maybe } from "./MaybeType";
import { None } from "./None";
import { SomeType } from "./SomeType";

export const Some = <T>(value: T): SomeType<T> => ({
    type: "Some",
    do: (f) => { f(value); return Some(value) },
    map: <S>(f: (t: T) => S) => Some(f(value)),
    mapOrDefault: (f, d) => f(value),
    mapOrDo: (f, d) => f(value),
    flatMap: <S>(f: MaybeFlatmapper<T, S>): Maybe<S> => {
        return f(value)
    },
    value,
    valueOrDefault: () => value,
    ifElse: valueIfSomeObject => valueIfSomeObject,
    valueOr: () => value,
    if(predicate:Predicate<T>) { return predicate(value) ? this : None },
})
