import { Dictionary } from './Dictionary'
import { Mapper } from '../axioms/Mapper'
import { withValue } from '../axioms/withValue'

export const hashTable = 
    <T>(hashFunction: Mapper<T, string>) => 
    (items: T[]) => 
    items.reduce(
        (acc, curr) => withValue(hashFunction(curr))(hash => ({
            ...acc,
            [hash]: [...acc[hash] || [], curr]
        })),
        {} as Dictionary<T[]>
    )
