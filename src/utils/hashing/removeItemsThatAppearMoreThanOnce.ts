import { Mapper } from "@utils/axioms/Mapper";
import { hashTable } from "./hashTable";


export const removeItemsThatAppearMoreThanOnce = <T>(hashFunction: Mapper<T, string>) => (items: T[]) => Object.values(hashTable(hashFunction)(items))
    .filter((v) => v.length === 1)
    .map((i) => i[0]);
