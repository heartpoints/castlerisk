import { Dictionary } from "./Dictionary";

export const entries = <T>(items:Dictionary<T>) => Object.entries(items);
