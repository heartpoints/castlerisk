import { deduplicateItems } from "./deduplicateItems"

describe("deduplicateItems", () => {
    it("works", () => {
        const items = [{name: "Tommy"}, {name: "Mike"}, {name: "Tommy"}, {name: "Joe"}]
        const hashFunction = (a:{name:string}) => a.name
        const uniqued = deduplicateItems(hashFunction)(items)
        const objFromEntries = Object.fromEntries(items.map((item) => [hashFunction(item), item]))
        const valuesFromObj = Object.values(objFromEntries);
        expect(uniqued).toStrictEqual([{name: "Tommy"}, {name: "Mike"}, {name: "Joe"}])
    })
})