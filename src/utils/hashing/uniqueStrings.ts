import { deduplicateItems } from "./deduplicateItems";

export const uniqueStrings = deduplicateItems((o: string) => o.toString());
