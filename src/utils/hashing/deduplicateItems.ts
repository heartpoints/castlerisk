import { Mapper } from "@utils/axioms/Mapper";


export const deduplicateItems = <T>(hashFunction: Mapper<T, string>) => (items: T[]) => Object.values(
    Object.fromEntries(items.map((item) => [hashFunction(item), item]))
);
