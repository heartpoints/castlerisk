import { FC, PropsWithChildren } from "react";

export const TooltippedButtonContainer: FC<
  PropsWithChildren & { disabled?: boolean; }
> = ({ children, disabled = false }) => (
  <div
    style={{
      display: "inline-block",
      cursor: disabled ? "not-allowed" : "pointer",
    }}
  >
    {children}
  </div>
);
