import { CastleRiskState } from "castleRisk/phases/common/CastleRiskState";
import { HasSetState } from "./HasSetState";


export type PropsWithSetState<InputStateType> = InputStateType & HasSetState<CastleRiskState>;
