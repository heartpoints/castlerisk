export type SetState<T> = (state: T) => void;
