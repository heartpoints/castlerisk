import { SetState } from "./SetState"

export type HasSetState<StateType> = { setState: SetState<StateType> }
