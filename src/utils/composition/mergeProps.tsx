
export type MergeProps = <T>(overrides: T) => <S>(base: S) => T & S;
export const mergeProps: MergeProps = overrides => base => ({ ...base, ...overrides });
