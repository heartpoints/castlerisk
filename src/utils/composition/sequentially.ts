import { Consumer } from "../axioms/Consumer";
import { callEachWith } from "./callEachWith";

export type Consumers<T> = Array<Consumer<T>>

export type Sequentially = <T>(funcs:Consumers<T>) => Consumer<T>
export const sequentially:Sequentially = (funcs) => t => callEachWith(funcs, t);
