import { stringToMaybeBoolean } from "./stringToMaybeBoolean";
import { Maybe } from "../maybe/MaybeType";

export type MaybeStringToMaybeBoolean = (s:Maybe<string>) => Maybe<boolean>
export const maybeStringToMaybeBoolean: MaybeStringToMaybeBoolean = s => s.flatMap(stringToMaybeBoolean);
