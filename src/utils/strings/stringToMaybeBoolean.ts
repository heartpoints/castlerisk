import { Switch } from "../switch/Switch";
import { Maybe } from "../maybe/MaybeType";

export type StringToMaybeBoolean = (s:string) => Maybe<boolean>
export const stringToMaybeBoolean: StringToMaybeBoolean = (s: string) => Switch.when(s.toLowerCase().trim())
    .matches(s => s === "true", true)
    .matches(s => s === "false", false)
    .result;
