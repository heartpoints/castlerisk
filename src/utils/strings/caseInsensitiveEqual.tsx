import { equalsVia } from '../axioms/equalsVia';
import { toLowerCase } from './toLowerCase';

export const caseInsensitiveEqual = equalsVia(toLowerCase);
