import { Integer } from '../math/Integer';

export const angleForRegularPolygon = 
    (numSides: Integer) => 
    Math.PI * 2 / numSides;
