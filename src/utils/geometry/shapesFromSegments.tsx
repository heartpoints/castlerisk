import { Segments } from "./Segments";
import { not } from "../predicates/not";
import { Shapes } from "./Shapes";
import { Points } from "./Points";
import { last } from "../arrays/last";
import { pointsEqual } from "./pointsEqual";
import { segmentsEqual } from "./segmentsEqual";

export const shapesFromSegments = (segments: Segments): Shapes => {
    const shapesFromSegmentsR = (
        remainingSegments: Segments,
        shapesFound: Shapes,
        currentLine: Points
    ): Shapes => {
        if (remainingSegments.length === 0) {
            return shapesFound;
        } else {
            if (currentLine.length === 0) {
                const [currentSegment, ...nextSegments] = remainingSegments;
                return shapesFromSegmentsR(nextSegments, shapesFound, currentSegment);
            } else {
                const lastPoint = last(currentLine);
                const firstPoint = currentLine[0];
                const nextSegment = remainingSegments.find(
                    (s) => pointsEqual(lastPoint)(s[0]) || pointsEqual(lastPoint)(s[1])
                )!;
                const nextPoint = pointsEqual(lastPoint)(nextSegment[0])
                    ? nextSegment[1]
                    : nextSegment[0];
                const isLineComplete = pointsEqual(nextPoint)(firstPoint);
                const isntNextSegment = not(segmentsEqual(nextSegment));
                return shapesFromSegmentsR(
                    remainingSegments.filter(isntNextSegment),
                    isLineComplete ? [...shapesFound, currentLine] : shapesFound,
                    isLineComplete ? [] : [...currentLine, nextPoint]
                );
            }
        }
    };
    return shapesFromSegmentsR(segments, [], []);
};
