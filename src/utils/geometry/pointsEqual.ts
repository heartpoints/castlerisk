import { Point } from './Point';
export const pointsEqual = 
    ({ x, y }: Point) => 
    ({ x: x2, y: y2 }: Point) => 
    x === x2 && y === y2;
