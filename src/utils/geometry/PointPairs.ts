import { PointPair } from './PointPair';

export type PointPairs = PointPair[];
