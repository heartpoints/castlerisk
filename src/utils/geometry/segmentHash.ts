import { withValue } from '../axioms/withValue'
import { max } from '../math/max'
import { min } from '../math/min'
import { Segment } from './Segment'

export const segmentHash = ([{ x: x1, y: y1 }, { x: x2, y: y2 }]: Segment) => withValue([[x1, x2], [y1, y2]])(
    ([xs, ys]) => [
        min(xs),
        max(xs),
        min(ys),
        max(ys),
    ].join('_')
)
