import { Point } from "./Point";
import { Mapper } from '../axioms/Mapper';
export type CoordToNumber = Mapper<Point, number>;
