import { z } from "zod";

export const Point = z.object({x: z.number(), y: z.number()})
export type Point = z.infer<typeof Point>
