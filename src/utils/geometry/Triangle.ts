import { Point } from "./Point";
export type Triangle = [Point, Point, Point];
