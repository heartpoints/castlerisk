import { Triangle } from './Triangle'
import { lengthsOfSegments } from './lengthsOfSegments'
import { triangleSegments } from "./triangleSegments"

export const triangleSegmentLenghts = 
    (triangle: Triangle) => 
    lengthsOfSegments(triangleSegments(triangle))
