import { pointDivide } from './pointDivide';
import { pointSum } from './pointSum';
import { averager } from '../math/averager';

export const averagePoint = averager(pointSum)(pointDivide)
    