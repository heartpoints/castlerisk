import { Shape } from './Shape';
import { PointPairs } from './PointPairs';
import { segmentReducer } from './segmentReducer';

export const segmentsFromShape = (s: Shape): PointPairs => [
    ...s.reduce(segmentReducer, { segments: [], previousPoint: null }).segments,
    [s[0], s[s.length - 1]],
];
