import { removeItemsThatAppearMoreThanOnce } from "@utils/hashing/removeItemsThatAppearMoreThanOnce";
import { segmentHash } from "./segmentHash";

export const segmentsThatDoNotAppearMoreThanOnce = removeItemsThatAppearMoreThanOnce(segmentHash);
