import { Shape } from './Shape';
import { translatePoint } from './translatePoint';
export const translateShapeY = (y: number) => (shape: Shape) => shape.map(translatePoint({ x: 0, y }));
