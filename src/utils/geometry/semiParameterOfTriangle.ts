import { numericSum } from '../math/numericSum'
import { Triangle } from './Triangle'
import { triangleSegmentLenghts } from './triangleSegmentLenghts'

export const semiParameterOfTriangle = 
    (triangle: Triangle) => 
    numericSum(triangleSegmentLenghts(triangle))
