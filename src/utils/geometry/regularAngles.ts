import { range } from '../list/range';
import { Integer } from '../math/Integer';
import { angleForRegularPolygon } from './angleForRegularPolygon';

export const regularAngles = 
    (numAngles: Integer) => 
    range(numAngles).map(i => angleForRegularPolygon(numAngles) * i);
