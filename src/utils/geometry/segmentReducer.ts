import { Point } from './Point';
import { LineSegments } from './LineSegments';

type SegmentReduction = { segments: LineSegments, previousPoint: Point | null }

export const segmentReducer = 
    ({segments, previousPoint}:SegmentReduction, currentPoint:Point):SegmentReduction =>
    previousPoint 
        ? ({ segments: [...segments, [previousPoint, currentPoint]], previousPoint: currentPoint })
        : ({ segments, previousPoint: currentPoint })
