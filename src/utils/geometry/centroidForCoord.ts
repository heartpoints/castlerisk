import { Shape } from './Shape';
import { numericSum } from '../math/numericSum';
import { cyclicPermutations } from '../arrays/cyclicPermutations';
import { CentroidOfAdjacentPoints } from "./CentroidOfAdjacentPoints";
import { centroidCooefficient } from './centroidCooefficient';

export const centroidForCoord = 
    (centroidOfAdjacentPoints:CentroidOfAdjacentPoints) => 
    (points: Shape) => 
    centroidCooefficient(points) *
    numericSum(cyclicPermutations(points).map(centroidOfAdjacentPoints))
