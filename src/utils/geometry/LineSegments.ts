import { LineSegment } from './LineSegment';

export type LineSegments = LineSegment[];
