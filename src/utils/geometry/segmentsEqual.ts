import { equalsVia } from "../axioms/equalsVia";
import { segmentHash } from "./segmentHash";

export const segmentsEqual = equalsVia(segmentHash);
