import { Point } from './Point';

export const translatePointX = 
    (xDiff: number) => 
    ({ x, y }: Point) => 
    ({
        x: x + xDiff,
        y
    });
