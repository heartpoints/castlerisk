import { Point } from "./Point";

import { z } from "zod";

export const Points = z.array(Point);
export type Points = z.infer<typeof Points>;