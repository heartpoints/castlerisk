import { Shape } from './Shape'
import { numericSum } from '../math/numericSum'
import { quasiCommuteAdjacentPoints } from './quasiCommuteAdjacentPoints'
import { cyclicPermutations } from '../arrays/cyclicPermutations'

export const polygonSignedArea = 
    (points: Shape) => 
    0.5 * 
    numericSum(
        cyclicPermutations(points).map(quasiCommuteAdjacentPoints)
    )
