import { Segment } from "./Segment";
import { pointsEqual } from "./pointsEqual";

export const segmentsSharePoint = (s1: Segment) => (s2: Segment) => s1.some(s1point => s2.some(pointsEqual(s1point)));
