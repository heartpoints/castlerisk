import { Shape } from "./Shape"
import { Point } from "./Point"
import classifyPoint from "robust-point-in-polygon"
import { pointToArrayPoint } from "./pointToArrayPoint"

export const pointInShape = 
    (point: Point) => 
    (shape: Shape) => 
    classifyPoint(
        shape.map(pointToArrayPoint), 
        pointToArrayPoint(point)
    ) < 1
