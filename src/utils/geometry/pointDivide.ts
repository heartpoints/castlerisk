import { Point } from './Point';
import { Divide } from '../math/DivideOfT';

export const pointDivide: Divide<Point, number> = 
    ({ x, y }) => 
    (divisor) => 
    ({
        x: x / divisor,
        y: y / divisor
    });
