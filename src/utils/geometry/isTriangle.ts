import { Shape } from './Shape';
import { Triangle } from './Triangle';
export const isTriangle = (shape: Shape): shape is Triangle => shape.length === 3;
