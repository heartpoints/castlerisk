import { distanceBetweenPoints } from './distanceBetweenPoints'
import { Segments } from './Segments'

export const lengthsOfSegments = 
    (segments: Segments) => 
    segments.map(
        ([p1, p2]) => distanceBetweenPoints(p1)(p2)
    )
