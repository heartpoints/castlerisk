import { originPoint } from './originPoint';
import { pointPlus } from './pointPlus';
import { summer } from '../math/summer';

export const pointSum = summer(pointPlus)(originPoint);
