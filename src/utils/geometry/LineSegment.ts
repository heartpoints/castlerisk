import { Point } from "./Point";

export type LineSegment = [Point, Point];
