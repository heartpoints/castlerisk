import { Point } from './Point';
import { Plus } from '../math/PlusOfT';

export const pointPlus: Plus<Point> = 
    ({ x, y }) => 
    ({ x: x2, y: y2 }) => 
    ({ x: x + x2, y: y + y2 });
