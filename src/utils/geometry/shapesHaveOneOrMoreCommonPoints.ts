import { Shape } from "./Shape";
import { pointsEqual } from "./pointsEqual";

export type ShapesHaveOneOrMoreCommonPoints = (shape1:Shape) => (shape2:Shape) => boolean
export const shapesHaveOneOrMoreCommonPoints: ShapesHaveOneOrMoreCommonPoints = 
    (shape1: Shape) => 
    (shape2: Shape) => 
    shape1.some(
        (pointFromShape1) => 
        shape2.some(pointsEqual(pointFromShape1))
    )