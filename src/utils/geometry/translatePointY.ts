import { Point } from './Point';

export const translatePointY = 
    (yDiff: number) => 
    ({ x, y }: Point) => 
    ({
        x,
        y: y + yDiff
    });
