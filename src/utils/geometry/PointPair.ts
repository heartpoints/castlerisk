import { Point } from "./Point";
import { LikePair } from '../arrays/LikePair';

export type PointPair = LikePair<Point>;
