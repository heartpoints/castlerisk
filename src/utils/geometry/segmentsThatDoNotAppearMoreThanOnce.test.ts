import { Segment } from "./Segment"
import { Segments } from "./Segments"
import { segmentsThatDoNotAppearMoreThanOnce } from "./segmentsThatDoNotAppearMoreThanOnce"

describe("uniqueSegments", () => {
    it("works", () => {
        const segment1:Segment = [{x: 1, y: 1}, {x:2, y:2}]
        const segment2:Segment = [{x: 2, y: 2}, {x:4, y:4}]
        const segment3:Segment = [{x: 2, y: 2}, {x:4, y:4}]
        const items:Segments = [segment1, segment2,segment3]
        const uniqued = segmentsThatDoNotAppearMoreThanOnce(items)
        expect(uniqued).toStrictEqual([segment1])
    })
})