import { Point } from "./Point";
export const pointToArrayPoint = ({ x, y }: Point): [number, number] => [x, y];
