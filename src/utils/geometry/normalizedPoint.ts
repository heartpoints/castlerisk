import { Point } from './Point';
import { lengthFromOrigin } from './lengthFromOrigin';
export const normalizedPoint = (p: Point) => {
    const { x, y } = p;
    const h = lengthFromOrigin(p);
    return { x: x / h, y: y / h };
};
