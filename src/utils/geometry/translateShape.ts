import { Shape } from './Shape';
import { Point } from './Point';
import { translatePoint } from './translatePoint';

export const translateShape = 
    (displacement: Point) => 
    (shape: Shape) => 
    shape.map(translatePoint(displacement));
