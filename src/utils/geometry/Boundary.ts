import { Shape } from "./Shape";

export const Boundary = Shape;
export type Boundary = Shape;
