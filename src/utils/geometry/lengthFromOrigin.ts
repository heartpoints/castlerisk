import { Point } from './Point';
export const lengthFromOrigin = ({ x, y }: Point) => Math.sqrt(x * x + y * y);
