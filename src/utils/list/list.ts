import { EmptyList } from "./EmptyList";
import { IList } from "./IList";

export const list = 
    <T>(...items: Array<T>): IList<T> => 
    items.reduce(
        (listSoFar, item) => listSoFar.push(item), 
        EmptyList as IList<T>
    );
