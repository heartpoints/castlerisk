import { list } from "./list";
import { IList } from "./IList";
export const ListOfLists = <T>(arrayOfArrays: Array<Array<T>>): IList<IList<T>> => list(...arrayOfArrays).map(nestedArray => list(...nestedArray));
