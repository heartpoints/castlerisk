import { maybe } from "../maybe/maybe";
import { Maybe } from "../maybe/MaybeType";
import { Pair } from "../arrays/Pair";

const shittyZip = <T, S>(ts: Array<T>, ss: Array<S>):Array<Pair<T,S>> =>
    ts.map((t,i) => [t, ss[i]])

//todo: unneeded maybe, artifact of lodash's possible undefined return for zip
export const zip = <T, S>(ts: Array<T>, ss: Array<S>): Maybe<Array<Pair<T, S>>> => {
    return maybe(shittyZip(ts, ss));
};
