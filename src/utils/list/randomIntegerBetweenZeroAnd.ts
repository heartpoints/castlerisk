import { randomNumberBetweenZeroAnd } from "./randomNumberBetweenZeroAnd";

export const randomIntegerBetweenZeroAnd = (maxExclusive: number) =>
  Math.floor(randomNumberBetweenZeroAnd(maxExclusive));
