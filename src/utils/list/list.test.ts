// const when = (description, ...rest) => describe(`when ${description}`, ...rest)

import { listFromArray } from "./listFromArray"

describe('List', () => {
    describe('replaceFirst', () => {
        describe(`array contains [1,2,3,4]`, () => {
            const array = [1,2,3,4]
            describe(`predicate isOdd`, () => {
                const isEven = n => n % 2 === 0
                const result = listFromArray(array).replaceFirst(isEven, n => n * 7).asArray
                it(`is [1,14,3,4]`, () => {
                    expect(result).toEqual([1,14,3,4])
                })
            })
        })
    })
})

// import { list } from "./list";
// import { theExpression } from "../../testing/theExpression";
// import { flatten } from "./flatten";
// import { ListOfLists } from "./ListOfLists";
// import { EmptyList } from "./EmptyList";
// // import { scramble } from "./scramble";

// describe("List", () => {
//     const plus = (a,b) => a + b
//     const not = f => x => !f(x)
//     const isEven = a => a % 2 === 0
//     const isOdd = not(isEven)

//     theExpression(() => list(1,2,3).map(x => x * 2).asArray).shouldDeepEqual([2,4,6])
//     theExpression(() => [1,2,3].map(x => x * 2)).shouldDeepEqual([2,4,6])
//     theExpression(() => list().reduce(plus, 0)).shouldEqual(0)
//     theExpression(() => list(1,2,3).reduce(plus, 0)).shouldEqual(6)
//     theExpression(() => list(1,2,3).asArray).shouldDeepEqual([1,2,3])
//     theExpression(() => list(1,2).push(3).asArray).shouldDeepEqual([1,2,3])
//     theExpression(() => list(1,2,3).head.value).shouldEqual(1)
//     theExpression(() => list(1,2,3).first(isEven).value).shouldEqual(2)
//     theExpression(() => list(1,2,3).first(isOdd).value).shouldEqual(1)
//     theExpression(() => list(1,5,3).first(isEven).isNone()).shouldEqual(true)
//     theExpression(() => list(1,5,3).reduce((a, b) => a - b, 0)).shouldEqual(-9)
//     theExpression(() => [...list(1,2,3), 4, 5]).shouldDeepEqual([1,2,3,4,5])

//     describe("append", () => {
//         const firstList = list(1,2,3)
//         const secondList = list(4,5,6)
//         theExpression(() => firstList.append(secondList).asArray).shouldDeepEqual([1,2,3,4,5,6])
//     });

//     describe("join", () => {
//         const firstList = list(1,2,3)
//         theExpression(() => firstList.join("-")).shouldEqual("1-2-3")
//         theExpression(() => EmptyList.join("-")).shouldEqual("")
//     })

//     describe("ListOfLists", () => {
//         const listOfLists = ListOfLists([
//             [1,2,3],
//             [4,5,6]
//         ])
//         theExpression(() => listOfLists.toString()).shouldEqual("[[1, 2, 3], [4, 5, 6]]")
//     });

//     describe("flatten", () => {
//         const listOfLists = ListOfLists([
//             [1,2,3],
//             [4,5,6]
//         ])
//         theExpression(() => flatten(listOfLists).asArray).shouldDeepEqual([1,2,3,4,5,6])
//     });

//     describe("flatMap", () => {
//         const multiples = list(10,100,1000)
//         const nums = list(1,2,3)
//         const allProducts = [10,100,1000,20,200,2000,30,300,3000]
//         theExpression(() => nums.flatMap(n => multiples.map(m => n * m)).asArray).shouldDeepEqual(allProducts)
//     })

//     // TODO: inherantly random, test for randomness?
//     // describe("scramble", () => {
//     //     theExpression(() => scramble([1,2,3,4,5])).shouldDeepEqual([1,2,3,4,5])
//     // });

// }) 