import { Predicate } from "../predicates/Predicate";
import { maybe } from "../maybe/maybe";
import { Maybe } from "../maybe/MaybeType";

export const first = <T>(array: T[], predicate: Predicate<T>): Maybe<T> => maybe(array.find(predicate));
