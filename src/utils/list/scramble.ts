import { emptyArray } from "../arrays/emptyArray";
import { everythingButIndex } from "./everythingButIndex";
import { randomIntegerBetweenZeroAnd } from "./randomIntegerBetweenZeroAnd";

export const scramble = <T>(a:T[]):T[] => {
    return a.length === 0
        ? emptyArray<T>()
        : (() => {
            const randomIndex = randomIntegerBetweenZeroAnd(a.length);
            const scrambledElement = a[randomIndex];
            const stillNeedsScrambling = everythingButIndex(randomIndex)(a);
            return [scrambledElement, ...scramble(stillNeedsScrambling)];
        })();
};
