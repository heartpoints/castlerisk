import { list } from "./list";
import { IList } from "./IList";

export const listFromArray = <T>(array: Array<T>): IList<T> => list(...array);
