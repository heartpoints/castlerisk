import { discriminatedUnion, z } from "zod";
import { caseClass } from "./caseClass";
import { zodUnionOfLiterals } from "./zodUnionOfLiterals"
import { caseClassUnion } from "./caseClassUnion";

describe("zod built in descriminated union", () => {
    it("compiles", () => {
        const Mike = caseClass("Mike")({ mike: z.boolean() });
        const Tommy = caseClass("Tommy")({ tommy: z.boolean() });

        const mike = Mike.new({mike: false})
        if(mike.URI === Mike.URI) {
            mike.mike
        }
        const TommyOrMike2 = discriminatedUnion("URI", [Mike.schema, Tommy.schema]);
        type TommyOrMike2 = z.infer<typeof TommyOrMike2>;
        
        const TommyOrMike = caseClassUnion([Tommy.schema, Mike.schema])
        const uris = TommyOrMike.URIs
        const urisSchema = zodUnionOfLiterals(uris)
        type TommyOrMike = z.infer<typeof TommyOrMike.schema>

        const v = { notTommyOrMike: true }
        if (TommyOrMike.isType(v)) { v.URI === "Tommy" } //compile error if URI not Tommy or Mike
    })
})