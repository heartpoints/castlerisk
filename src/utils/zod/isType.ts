import { ZodLiteral, ZodObject, ZodType as ZodSchema } from "zod";

export const isType = <URI extends string, T extends { URI: URI }>(
  schema: ZodSchema<T>
) => (value: unknown): value is T => {
  if (!(schema instanceof ZodObject)) {
    console.error({ schema });
    throw new Error(`new_() only works with ZodObject`);
  }
  const shape = schema.shape;
  const uriSchema = shape["URI"];
  if (!(uriSchema instanceof ZodLiteral)) {
    throw new Error("The URI property must be a ZodLiteral");
  }
  const URI = uriSchema.value as URI;
  return (value as any).URI == URI
};
