//TODO: Would be nice to be able to just get nice optics off these types by lazily constructing them via a getter whose name
//matches the properties, with the exception of one method, optic, which returns whatever optic makes sense based on the chain of prior calls

// eg: Tommy.optics.b.c[2].d.lens

// this would produce the same as optic<Tommy>prop("b").prop("c").at(2) but without me needing to type those strings or the word prop.

// then, there would be wrappers for set / get / remove / preview that would call .lens or .____ for the appropriate type of optic they need:
// preview(Tommy.optics.b.c[2].d)
// remove(Tommy.optics.b.c[2].d)
// replace(Tommy.optics.b.c[2].d)(d => d + 1)

// potentially, we could even remove the .optics and just reserve optic-specific methods from being used in state (seems reasonable and could have optics as a fallback for edge-cases)
