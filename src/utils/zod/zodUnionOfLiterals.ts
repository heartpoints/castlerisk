import { z } from "zod";


export function zodUnionOfLiterals<T extends string | number> ( zodDiscriminators: readonly T[] ) {
    const literals = zodDiscriminators.map(
        x => z.literal( x )
    ) as unknown as readonly [ z.ZodLiteral<T>, z.ZodLiteral<T>, ...z.ZodLiteral<T>[] ]
    return z.union( literals )
}
