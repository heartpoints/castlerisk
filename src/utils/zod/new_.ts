import { ZodLiteral, ZodObject, ZodType as ZodSchema } from "zod";

export const new_ =
  <URI extends string, T extends { URI: URI }>(schema: ZodSchema<T>) =>
  (argsBesidesURI: Omit<T, "URI">): T => {
    if (!(schema instanceof ZodObject)) {
      console.error({ schema });
      throw new Error(`new_() only works with ZodObject`);
    }
    const shape = schema.shape;
    const uriSchema = shape["URI"];
    if (!(uriSchema instanceof ZodLiteral)) {
      throw new Error("The URI property must be a ZodLiteral");
    }
    const URI = uriSchema.value as URI;
    return {
      ...argsBesidesURI,
      URI,
    } as T;
  };
