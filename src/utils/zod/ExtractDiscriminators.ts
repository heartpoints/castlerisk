export type ExtractDiscriminators<T, D extends string> = T extends {
  [d in D]: infer S;
} ? S : never;
