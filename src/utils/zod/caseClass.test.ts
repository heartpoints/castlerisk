import { z } from "zod";
import { caseClass } from "./caseClass";

describe("caseClass", () => {
  it("compiles", () => {
    const Tommy = caseClass("Tommy")({
      a: z.number(),
      b: z.object({ c: z.boolean() }),
    });

    type Tommy = typeof Tommy.type;

    const ExtendedTommy = Tommy.extend("ExtendedTommy")({
      extended: z.boolean(),
    });
    type ExtendedTommy = typeof ExtendedTommy.type;

    const couldBeTommy: Tommy | { URI: "Mike"; mike: string } = Tommy.new({a: 5, b: { c: false }});
    const couldBeTommyOrExtendedTommy: Tommy | ExtendedTommy = ExtendedTommy.new({a: 7, b: { c: true }, extended: false })

    if (Tommy.isType(couldBeTommy)) {
      couldBeTommy.b.c;
    }

    if (ExtendedTommy.isType(couldBeTommyOrExtendedTommy)) {
        couldBeTommyOrExtendedTommy.extended;
    }

    const tommyParseResult = Tommy.safeParse({ m: 5 });
    const tommy2 = tommyParseResult.success && tommyParseResult.data;
  });
});
