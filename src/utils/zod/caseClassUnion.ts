/*
Incomplete Concept

- allow inputs to be CaseClass instances (need an abstraction for that, as well as ensure typesafety for their zod schemas)
- enable URIs to be a zod union and type for whatever unique range of URIs are present in the options
- the mapTuple example may help in preserving typesafety via variadic tuple types concept
*/

import {
  discriminatedUnion,
  RawCreateParams,
  z,
  ZodDiscriminatedUnionOption,
} from "zod";
import { zodDiscriminators } from "./zodDiscriminators";

export const caseClassUnion = <
  Types extends readonly [
    ZodDiscriminatedUnionOption<"URI">,
    ...ZodDiscriminatedUnionOption<"URI">[]
  ]
>(
  types: Types,
  params?: RawCreateParams
) => {
  const schema = discriminatedUnion("URI", types, params);
  const URIs = zodDiscriminators(schema); //zodUnionOfLiterals()
  type FullType = z.infer<typeof schema>;
  const { parse, safeParse } = schema;
  const isType = (val: unknown): val is FullType => safeParse(val).success;
  return {
    URIs,
    schema,
    isType,
    get type(): FullType {
      throw new Error(
        `caseClassUnion.type is convenience property for use with typeof at compile time only. URIs: ${URIs.join(
          ", "
        )}`
      );
    },
    safeParse,
    parse,
  };
};

function mapTuple<T extends unknown[]>(
  ...args: T
): { [K in keyof T]: [T[K], T[K]] } {
  return args.map((x) => [x, x]) as any;
}
