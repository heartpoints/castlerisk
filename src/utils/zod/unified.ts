// import { z, ZodDiscriminatedUnion, ZodDiscriminatedUnionOption } from "zod";
// import { PlayerCard } from "../../castleRisk/cards/PlayerCard";
// import { zodDiscriminators } from "./zodDiscriminators";
// import { zodUnionOfLiterals } from "./zodUnionOfLiterals";

// const unified = <
//   Discriminator extends string,
//   Options extends readonly ZodDiscriminatedUnionOption<Discriminator>[],
//   T extends ZodDiscriminatedUnion<Discriminator, Options>
// >(
//   schema: T
// ) => {
//   const discriminators = zodDiscriminators(schema);

//   //compile error on discriminators: Type 'unknown' is not assignable to type 'string | number'
//   return zodUnionOfLiterals(discriminators);
// };

// const CardType = unified(PlayerCard);
// type CardType = z.infer<typeof CardType>;
