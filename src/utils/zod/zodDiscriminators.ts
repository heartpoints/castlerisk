import { z, ZodDiscriminatedUnion } from "zod";

type ExtractedDiscriminators<T, D extends string> = T extends { [d in D]: infer S } ? S : never;

export const zodDiscriminators = <T extends ZodDiscriminatedUnion<any, any>>(schema:T):readonly ExtractedDiscriminators<z.infer<T>, T["discriminator"]>[] => schema.options.map(
    (x) => x.shape[schema.discriminator]
) as any as readonly ExtractedDiscriminators<z.infer<T>, T["discriminator"]>[]
