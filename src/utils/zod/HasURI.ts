import { z } from "zod";

export const HasURI = <URI extends string>(URI: URI) => z.object({ URI: z.literal(URI) });
export type HasURI<URI extends string> = { URI: URI }
