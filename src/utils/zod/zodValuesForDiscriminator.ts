import { z, ZodDiscriminatedUnion, ZodDiscriminatedUnionOption, ZodLiteral } from "zod";
import { PlayerCard } from "../../castleRisk/cards/PlayerCard";
import { ExtractDiscriminators } from "./ExtractDiscriminators";

const zodValuesForDiscriminator = <Discriminator extends string>(
  schema: z.ZodDiscriminatedUnion<
    Discriminator,
    readonly ZodDiscriminatedUnionOption<Discriminator>[]
  >
) => {
  return z.union([z.never(), z.never(), ...schema.options.map(
    (x) => x.shape[schema.discriminator]
  )]);

  //what i get:
  //z.ZodUnion<[z.ZodNever, z.ZodNever, ...z.ZodTypeAny[]]>

  //what i want:
  //z.ZodUnion<[z.ZodNever, z.ZodNever, ...(z.ZodLiteral<"Diplomat"> | z.ZodLiteral<"Spy"> | ...)[]]>
};
