import { RawCreateParams, z, ZodRawShape } from "zod";

export const caseClass =
  <URI extends string>(URI: URI) =>
  <T extends ZodRawShape>(shape: T, params?: RawCreateParams) => {
    const schema = z
      .object(shape, params)
      .extend({ URI: z.literal(URI) })
      .strict();
    type FullType = z.infer<typeof schema>;
    const { parse, safeParse } = schema;
    const isType = (val: unknown): val is FullType => safeParse(val).success;
    return {
      URI,
      schema,
      isType,
      get type(): FullType {
        throw new Error(
          `caseClass.type is convenience property for use with typeof at compile time only. URI: ${URI}`
        );
      },
      safeParse,
      parse,
      new: (args: Omit<FullType, "URI">) => ({ ...args, URI }),
      extend:
        <URIExtended extends string>(URI: URIExtended) =>
        <Augmentation extends ZodRawShape>(augmentation: Augmentation) =>
          caseClass(URI)(schema.extend(augmentation).shape),
    };
  };

  //the question is more, given a generic zod schema of any type, can i extend it? including if it is is an intersection of objects?