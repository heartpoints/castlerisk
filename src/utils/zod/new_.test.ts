import { when } from "@utils/testing/when"
import { HasURI } from "./HasURI"
import { z } from "zod"
import { new_ } from "./new_"

describe("new_", () => {
    when("zod schema has URI and string property hello", () => {
        it("creates a new object with both URI and specified value for hello", () => {
            const Example = HasURI("Example").merge(z.object({hello: z.string()}));
            const example = new_(Example)({hello: "tommy"})
            expect(example).toHaveProperty("hello", "tommy");
        })
    })
})