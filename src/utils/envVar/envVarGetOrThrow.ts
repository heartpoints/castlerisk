export const envVarGetOrThrow = (envKey: string) => {
    const retVal = process.env[envKey];
    if (retVal === undefined) {
        throw new Error(`Env var ${envKey} must be defined`);
    }
    return retVal;
};
