import { RGBAColor } from "../colors/RGBAColor";
import { rgbaToCSSString } from "../colors/rgbaToCSSString";
import { CanvasEffect } from "./CanvasEffect";

export const setStrokeStyle = 
    (color: RGBAColor): CanvasEffect => 
    (context) => context.strokeStyle = rgbaToCSSString(color);
