import { CanvasEffect } from "./CanvasEffect";

export const fill: CanvasEffect = (context) => context.fill();
