import { CanvasEffect } from "./CanvasEffect";

export const moveTo = (x: number, y: number): CanvasEffect => (context) => context.moveTo(x, y);
