export const setStrokeWidth = (width: number) => (context: CanvasRenderingContext2D) => context.lineWidth = width;
