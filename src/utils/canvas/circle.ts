import { Point } from '../geometry/Point';

export const circle = ({ x, y }: Point) => (radius: number) => (context: CanvasRenderingContext2D) => context.ellipse(x, y, radius, radius, 0, 0, Math.PI * 2);
