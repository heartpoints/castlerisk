import { CanvasEffect } from "./CanvasEffect";

export const lineTo =
  (x: number, y: number): CanvasEffect =>
  (context) =>
    context.lineTo(x, y);
