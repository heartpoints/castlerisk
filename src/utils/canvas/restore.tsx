import { CanvasEffect } from './CanvasEffect';


export const restore: CanvasEffect = canvas => canvas.restore();
