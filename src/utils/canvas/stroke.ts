import { CanvasEffect } from "./CanvasEffect";

export const stroke: CanvasEffect = (context) => context.stroke();
