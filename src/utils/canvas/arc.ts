export const arc = (x: number, y: number, pointRadius: number) => (context: CanvasRenderingContext2D) => context.arc(x, y, pointRadius, 0, 2 * Math.PI);
