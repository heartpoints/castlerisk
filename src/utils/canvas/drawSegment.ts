import { sequentially } from "../composition/sequentially";
import { Segment } from "../geometry/Segment";
import { RGBAColor } from "../colors/RGBAColor";
import { beginPath } from "./beginPath";
import { closePath } from "./closePath";
import { lineTo } from "./lineTo";
import { moveTo } from "./moveTo";
import { setStrokeStyle } from "./setStrokeStyle";
import { stroke } from "./stroke";
import { setStrokeWidth } from "./setStrokeWidth";

export const drawSegment = 
    (strokeColor: RGBAColor) => 
    ([{ x: x1, y: y1 }, { x: x2, y: y2 }]: Segment) => 
    sequentially([
        setStrokeStyle(strokeColor),
        setStrokeWidth(2),
        beginPath,
        moveTo(x1, y1),
        lineTo(x2, y2),
        closePath,
        stroke,
        setStrokeWidth(1),
    ])
