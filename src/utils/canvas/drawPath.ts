import { sequentially } from "../composition/sequentially";
import { Shape } from "../geometry/Shape";
import { beginPath } from "./beginPath";
import { CanvasEffect } from "./CanvasEffect";
import { closePath } from "./closePath";
import { fill } from "./fill";
import { lineTo } from "./lineTo";
import { moveTo } from "./moveTo";
import { stroke } from "./stroke";

export const drawPath = 
    ([{x, y}, ...remainingPoints]: Shape): CanvasEffect => 
    sequentially([
        beginPath,
        moveTo(x, y),
        ...remainingPoints.map(({x, y}) => lineTo(x, y)),
        lineTo(x, y),
        closePath,
        stroke,
        fill
    ])
