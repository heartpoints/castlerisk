import { sequentially } from '../composition/sequentially';
import { pointRadius } from '../../castleRisk/config/pointRadius';
import { Point } from "../geometry/Point";
import { RGBAColor } from "../colors/RGBAColor";
import { arc } from './arc';
import { beginPath } from './beginPath';
import { closePath } from './closePath';
import { fill } from './fill';
import { setFillStyle } from "./setFillStyle";

export const drawPoint = 
    (fillColor: RGBAColor) =>
    ({ x, y }: Point) => 
    sequentially([
        setFillStyle(fillColor),
        beginPath,
        arc(x,y,pointRadius),
        closePath,
        fill
    ])
