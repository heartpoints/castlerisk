export const setFont = (fontDef: string) => (context: CanvasRenderingContext2D) => context.font = fontDef;
