import { Consumer } from "../axioms/Consumer";
import { Point } from "../geometry/Point";

export type OnCanvasMouseEventHandler = Consumer<Point>;
