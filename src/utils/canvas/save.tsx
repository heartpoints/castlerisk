import { CanvasEffect } from './CanvasEffect';


export const save: CanvasEffect = canvas => canvas.save();
