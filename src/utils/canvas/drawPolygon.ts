import { doNothing } from "../axioms/doNothing";
import { sequentially } from "../composition/sequentially";
import { RGBAColor } from "../colors/RGBAColor";
import { Shape } from "../geometry/Shape";
import { drawPath } from "./drawPath";
import { setFillStyle } from "./setFillStyle";
import { setStrokeStyle } from "./setStrokeStyle";
import { CanvasEffect } from "./CanvasEffect";

export const drawPolygon = 
    (strokeColor: RGBAColor) =>
    (fillColor: RGBAColor) => 
    (shape: Shape): CanvasEffect => 
    shape.length > 2
        ? sequentially([
            setStrokeStyle(strokeColor),
            setFillStyle(fillColor),
            drawPath(shape),
        ])
        : doNothing
