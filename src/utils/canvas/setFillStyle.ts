import { RGBAColor } from "../colors/RGBAColor";
import { rgbaToCSSString } from "../colors/rgbaToCSSString";
import { CanvasEffect } from "./CanvasEffect";

export const setFillStyle =
  (color: RGBAColor): CanvasEffect =>
  (context) =>
    (context.fillStyle = rgbaToCSSString(color));
