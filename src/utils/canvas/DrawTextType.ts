import { Point } from "../geometry/Point";
export type DrawText = (textLocation: Point) => (text: string) => void;
