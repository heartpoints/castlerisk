import { CanvasEffect } from "./CanvasEffect";

export const closePath: CanvasEffect = (context) => context.closePath();
