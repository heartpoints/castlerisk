import { CanvasEffect } from "./CanvasEffect";

export const beginPath: CanvasEffect = (context) => context.beginPath();
