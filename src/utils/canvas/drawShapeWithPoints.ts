import { drawPoint } from './drawPoint';
import { drawPolygon } from './drawPolygon';
import { RGBAColor } from '../colors/RGBAColor';
import { Shape } from '../geometry/Shape';
import { sequentially } from '../composition/sequentially';

export const drawShapeWithPoints = 
    (strokeColor: RGBAColor) => 
    (pointColor: RGBAColor) => 
    (fillColor: RGBAColor) => 
    (shape: Shape) => 
    sequentially([
        drawPolygon(strokeColor)(fillColor)(shape),
        ...shape.map(drawPoint(pointColor))
    ])
