import { canvasWidth } from "../../castleRisk/config/canvasWidth";
import { canvasHeight } from "../../castleRisk/config/canvasHeight";
import { CanvasEffect } from "./CanvasEffect";

export const clearCanvas: CanvasEffect = (context) =>
  context.clearRect(0, 0, canvasWidth, canvasHeight);
