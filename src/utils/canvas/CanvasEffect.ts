export type CanvasEffect = (context: CanvasRenderingContext2D) => void;
