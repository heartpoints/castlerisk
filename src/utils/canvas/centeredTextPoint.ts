import { getTextWidth } from "./getTextWidth";
import { translatePointX } from "../geometry/translatePointX";
import { Point } from "../geometry/Point";

export const centeredTextPoint = (
    centerpoint: Point,
    text: string,
    context: CanvasRenderingContext2D
): Point => translatePointX(getTextWidth(text)(context) / -2)(centerpoint);
