import { RGBAColor } from "../colors/RGBAColor";
import { Shape } from "../geometry/Shape";
import { drawPoint } from './drawPoint';
import { drawPolygon } from "./drawPolygon";
import { black } from "../colors/black";
import { sequentially } from "../composition/sequentially";

export const drawShape = 
    (shape: Shape) => 
    (pointFillStyle: RGBAColor ) =>
    (shapeFillStyle: RGBAColor) => 
    sequentially([
        drawPolygon(black)(shapeFillStyle)(shape),
        ...shape.map(drawPoint(pointFillStyle))
    ])
