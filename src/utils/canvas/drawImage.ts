export const drawImage = 
    (background: HTMLOrSVGImageElement) => 
    (x: number) => 
    (y: number) => 
    (context: CanvasRenderingContext2D) => 
    context.drawImage(
        background, 
        x, 
        y, 
        background.width as number, 
        background.height as number
    )
