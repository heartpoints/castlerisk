import { sequentially } from "../composition/sequentially";
import { Point } from "../geometry/Point";
import { RGBAColor } from "../colors/RGBAColor";
import { defaultFontFace } from "./defaultFontFace";
import { defaultFontPixels } from "./defaultFontPixels";
import { defaultFontSize } from "./defaultFontSize";
import { setShadow } from "./setShadow";
import { setStrokeStyle } from "./setStrokeStyle";
import { strokeText } from "./strokeText";
import { setFont } from "./setFont";
import { save } from "./save";
import { restore } from "./restore";

//need an optional size, perhaps we should convert to named params
export const drawText =
    (useShadow: boolean) => 
    (color: RGBAColor) => 
    ({ x, y }: Point) => 
    (text: string) => 
    sequentially([
        save,
        setShadow(useShadow),
        setStrokeStyle(color),
        setFont(`${defaultFontSize} ${defaultFontFace}`),
        strokeText(text)(x)(y + defaultFontPixels / 2),
        restore,
    ])
