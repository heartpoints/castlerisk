import { averager } from '../math/averager';
import { colorDivideBy } from './colorDivideBy';
import { sumColors } from './sumColors';


export const averageColor = averager(sumColors)(colorDivideBy);