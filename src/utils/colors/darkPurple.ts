import { RGBAColor } from "./RGBAColor";
import { darkenColor } from "./darkenColor";
import { purple } from "./purple";
export const darkPurple: RGBAColor = darkenColor(0.1)(purple);
