import { RGBAColor } from './RGBAColor';
export const rgbaColorToPlainString = (color: RGBAColor) => color.join(",");
