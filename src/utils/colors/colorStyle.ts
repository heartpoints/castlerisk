import { RGBAColor } from './RGBAColor';
import { contrastColor } from './contrastColor';
import { rgbaToCSSString } from './rgbaToCSSString';

export const colorStyle = (c: RGBAColor) => ({
    backgroundColor: rgbaToCSSString(c), color: rgbaToCSSString(contrastColor(c))
});
