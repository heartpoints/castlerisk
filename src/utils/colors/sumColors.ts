import { summer } from '../math/summer';
import { black } from './black';
import { plusColors } from './plusColors';
export const sumColors = summer(plusColors)(black);
