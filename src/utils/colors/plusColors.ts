import { RGBAColor } from './RGBAColor';
import { arraySum } from '../arrays/arraySum';
export const plusColors = (c: RGBAColor) => (c2: RGBAColor) => arraySum(c)(c2) as RGBAColor;
