import { RGBAColor } from './RGBAColor';
export const plainStringToRGBAColor = (rgbaString: string) => rgbaString.split(",").map(parseFloat) as RGBAColor;
