import { max } from '../math/max'
import { min } from '../math/min'
import { RGBAColor } from './RGBAColor'

export const colorSaturation = ([r, g, b]: RGBAColor): number => {
    const rgbComponents = [r, g, b]
    const maxComponent = max(rgbComponents)
    const minComponent = min(rgbComponents)
    const denominator = maxComponent + minComponent
    return denominator === 0
        ? 0
        : (maxComponent - minComponent) / denominator
}
