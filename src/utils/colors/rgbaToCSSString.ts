import { RGBAColor } from "./RGBAColor";
export const rgbaToCSSString = (color: RGBAColor) => `rgba(${color.join(",")})`;
