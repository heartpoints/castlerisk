import { RGBAColor } from './RGBAColor';
import { colorStyle } from './colorStyle';
import { rgbaColorToPlainString } from './rgbaColorToPlainString';


//todo: may break form / value is not used
export const ColorMenuItem = (color: RGBAColor) => {
    const colorString = rgbaColorToPlainString(color);
    return <p style={colorStyle(color)} key={colorString}>&nbsp;</p>
};
