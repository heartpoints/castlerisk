import { RGBAColor } from './RGBAColor';
import { averageNumber } from '../math/averageNumber';
export const colorIntensity = ([r, g, b]: RGBAColor): number => averageNumber([r, g, b]);
