import { lightenColor } from './lightenColor';
import { black } from './black';
export const gray = lightenColor(0.5)(black);
