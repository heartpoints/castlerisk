import { RGBAColor } from "./RGBAColor";

import { z } from "zod";

export const HasColor = z.object({ color: RGBAColor });
export type HasColor = z.infer<typeof HasColor>;
