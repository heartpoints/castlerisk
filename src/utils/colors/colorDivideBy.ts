import { arrayDivideByNumber } from '../arrays/arrayDivideByNumber';
import { Divide } from '../math/DivideOfT';
import { RGBAColor } from './RGBAColor';
export const colorDivideBy = arrayDivideByNumber as any as Divide<RGBAColor, number>;