import { Consumer } from '../axioms/Consumer';
import { RGBAColor } from './RGBAColor';
import { Colors } from './Colors';
export type ColorPickerProps = {
    colors: Colors;
    onColorChange: Consumer<RGBAColor>;
    value: RGBAColor;
};
