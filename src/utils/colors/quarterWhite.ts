import { white } from './white';
import { opacity } from './opacity';
export const quarterWhite = opacity(0.25)(white);
