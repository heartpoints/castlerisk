import { inverseColor } from "./inverseColor";
import { RGBAColor } from "./RGBAColor";
import { rgbaToCSSString } from "./rgbaToCSSString";

export const buttonStyle = (color: RGBAColor) => ({
    backgroundColor: rgbaToCSSString(color),
    color: rgbaToCSSString(inverseColor(color)),
    width: "10em",
    height: "5em",
    cursor: "pointer",
});
