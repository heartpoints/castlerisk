import { zip } from '../list/zip';
import { RGBAColor } from './RGBAColor';
export const colorsAreEqual = (color1: RGBAColor) => (color2: RGBAColor) => zip(color1, color2).value.every(([c1, c2]) => c1 === c2);
