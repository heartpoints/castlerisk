import { z } from "zod";
import { RGBAColorComponent } from "./RGBAColorComponent";

export const RGBAColor = z.tuple([RGBAColorComponent, RGBAColorComponent, RGBAColorComponent, RGBAColorComponent]);
export type RGBAColor = z.infer<typeof RGBAColor>;