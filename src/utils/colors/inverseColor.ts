import { RGBAColor } from './RGBAColor';
import { inverseColorComponent } from "./inverseColorComponent";
export const inverseColor = ([r, g, b, a]: RGBAColor): RGBAColor => [...[r, g, b].map(inverseColorComponent), a] as RGBAColor;
