import { white } from './white';
import { opacity } from './opacity';
export const halfWhite = opacity(0.5)(white);
