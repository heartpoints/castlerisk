import { buttonStyle } from "./buttonStyle";
import { ColorPickerProps } from "./ColorPickerProps";
import { colorsAreEqual } from "./colorsAreEqual";

export const ColorPicker = ({
  colors,
  onColorChange,
  value,
}: ColorPickerProps) => {
  return (
    <div style={{ marginTop: "5px" }}>
      <span>Army Piece Color</span>
      <div>
        {colors.map((color, i) => (
          <button
            style={buttonStyle(color)}
            onClick={() => onColorChange(color)}
            key={i}
          >
            {colorsAreEqual(color)(value) ? "Selected" : "-"}
          </button>
        ))}
      </div>
    </div>
  );
};
