import { RGBAColor } from "./RGBAColor"
import { diffsWithNum } from "../math/diffsWithNum"
import { scale } from "../math/scale"
import { addNums } from "../math/addNums"

export const lightenColor = 
    (percentage: number) => 
    (color: RGBAColor) => {
        const [r, g, b, a] = color
        const rgb = [r, g, b]
        const amountToPureWhite = diffsWithNum(255)(rgb)
        const amountsToIncrease = scale(percentage)(amountToPureWhite)
        return [...addNums(amountsToIncrease)(rgb), a] as RGBAColor
    }
