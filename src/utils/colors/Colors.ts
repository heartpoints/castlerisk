import { RGBAColor } from "./RGBAColor";

import { z } from "zod";

export const Colors = z.array(RGBAColor);
export type Colors = z.infer<typeof Colors>;
