import { colorsAreEqual } from "./colorsAreEqual";
import { within } from "../arrays/within";

export const withinColor = within(colorsAreEqual);
