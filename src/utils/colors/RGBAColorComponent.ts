import { z } from "zod";


export const RGBAColorComponent = z.number().min(0).max(256);
export type RGBAColorComponent = z.infer<typeof RGBAColorComponent>;
