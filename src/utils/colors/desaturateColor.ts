import { RGBAColor } from "./RGBAColor";
import { diffsWithNum } from "../math/diffsWithNum";
import { scale } from "../math/scale";
import { addNums } from "../math/addNums";
import { averageNumber } from "../math/averageNumber";

export const desaturateColor = (saturationPercent: number) => ([r, g, b, a]: RGBAColor) => {
    const rgb = [r, g, b];
    const grayLevel = averageNumber(rgb);
    const diffs = diffsWithNum(grayLevel)(rgb);
    const scaledDiffs = scale(saturationPercent)(diffs);
    return [...addNums(rgb)(scaledDiffs), a] as RGBAColor;
};
