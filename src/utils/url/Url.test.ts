import { urlFromString } from "./urlFromString";

 describe("urlFromString", () => {
    const result = urlFromString("http://www.cnn.com/hey")
    it("has protocol http", () => {
        expect(result.protocol === 'http')
    })
    it("has path /hey", () => {
        expect(result.path === '/hey')
    })
    it("has fullyQualifiedDomainName cnn.com", () => {
        expect(result.fullyQualifiedDomainName === 'cnn.com')
    })
 })
    