import { Predicate } from "../predicates/Predicate";

export const not = 
    <T>(predicate:Predicate<T>):Predicate<T> =>
    (t:T) => !predicate(t)