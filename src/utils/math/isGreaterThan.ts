export const isGreaterThan = (threshold: number) => (testNumber: number) => testNumber > threshold;
