import { makeArray } from "../arrays/makeArray"

export const min = (n: number[] | number, ...additionalNums: number[]) => Math.min(...makeArray(n), ...additionalNums)
