export type Sum<T> = (summables: T[]) => T;
