export const maxCurried = (n: number) => (n2: number) => Math.max(n, n2);
