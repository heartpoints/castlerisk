
export const divideBy = 
    <T extends number>
    (a:T) => 
    (b:number) => 
    b / a as T