export const numericMultiply = (a: number) => (b: number) => a * b;
