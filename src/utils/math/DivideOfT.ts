export type Divide<T, S> = (itemToDivide: T) => (divisor: S) => T;
