import { makeArray } from "../arrays/makeArray";

export const max = (n: number[] | number, ...additionalNums:number[]) => Math.max(...makeArray(n), ...additionalNums);
