import { max } from "./max";
import { min } from "./min";

export const minMax = (minimum:number, maximum:number, actual:number) => max(minimum, min(actual, maximum))