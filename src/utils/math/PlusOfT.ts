export type Plus<T> = (a: T) => (b: T) => T;
