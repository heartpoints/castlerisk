export const minCurried = (n: number) => (n2: number) => Math.min(n, n2);

