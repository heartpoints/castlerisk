import { Divide } from './DivideOfT';
import { Sum } from './SumOfT';

export const averager = 
    <T>(sum: Sum<T>) => 
    (divide: Divide<T, number>) => 
    (averagables: T[]) => 
    divide(sum(averagables))(averagables.length)
