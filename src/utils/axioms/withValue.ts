import { Mapper } from "./Mapper";

export const withValue = <T>(value: T) => <R>(handler: Mapper<T, R>) => handler(value);
