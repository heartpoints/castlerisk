export type EqualityCheck<T> = (t1: T) => (t2: T) => boolean;
