//TODO: figure out how to infer the types from the passed string's value
export const prop = <T>() => <P extends keyof T>(prop: P) => (t: T) => t[prop];
