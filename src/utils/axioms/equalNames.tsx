import { equalsVia } from './equalsVia';
import { getName } from './getName';

export const equalNames = equalsVia(getName)
