import { Mapper } from '../../utils/axioms/Mapper';

export const equalsVia = <T, S>(mapper: Mapper<T, S>) => (t1: T) => (t2: T) => mapper(t1) === mapper(t2);
