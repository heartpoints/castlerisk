import { z } from "zod";


export const Name = z.string().min(1).max(20);
export type Name = z.infer<typeof Name>;
