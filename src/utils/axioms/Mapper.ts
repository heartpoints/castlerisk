export type Mapper<T, S> = (t:T) => S
export type SelfMapper<T> = Mapper<T, T>