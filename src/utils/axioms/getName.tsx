import { prop } from './prop';
import { HasName } from './HasName';


export const getName = prop<HasName>()("name");
