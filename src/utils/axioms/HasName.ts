import { z } from "zod";
import { Name } from "./Name";

export const HasName = z.object({ name: Name });
export type HasName = z.infer<typeof HasName>;
