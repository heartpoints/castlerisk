import { Predicate } from "../predicates/Predicate";

export const either = <T>(p1: Predicate<T>) => (p2: Predicate<T>) => (t: T) => p1(t) || p2(t);
