
export const isUndefined = <T>(n:T | undefined):n is undefined => n === undefined;
