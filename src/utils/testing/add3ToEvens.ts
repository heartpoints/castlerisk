import { Some } from "../maybe/Some";
import { None } from "../maybe/None";
export const add3ToEvens = (x: number) => x % 2 === 0 ? Some(x + 3) : None;
