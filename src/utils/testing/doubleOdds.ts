import { Some } from "../maybe/Some";
import { None } from "../maybe/None";
export const doubleOdds = (x: number) => x % 2 === 1 ? Some(x * 2) : None;
