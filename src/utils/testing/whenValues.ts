import { Consumer } from "../axioms/Consumer";
import { whenTextFromObject } from "./whenTextFromObject";

export const whenValues =
    <T extends Object>(obj: T, block: Consumer<T>) => 
    context(whenTextFromObject(obj), () => block(obj));
