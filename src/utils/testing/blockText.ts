import { Consumer } from "../axioms/Consumer";
export const blockText = <T>(block: Consumer<T>) => block.toString().replace("\n", "");
