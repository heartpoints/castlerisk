import { EqualityComparer } from "../list/EqualityComparer";

export const within = <T>(equalityOperator: EqualityComparer<T>) => (elements: T[]) => (candidateElement: T) => elements.some(equalityOperator(candidateElement));
