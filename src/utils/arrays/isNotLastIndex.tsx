import { not } from '../../utils/predicates/not';
import { isLastIndex } from './isLastIndex';

export const isNotLastIndex = (items: any[]) => not(isLastIndex(items));
