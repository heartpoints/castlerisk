import { divideBy } from '../math/numericDivide';

export const arrayDivideByNumber = 
    (a: Array<number>) => 
    (divisor: number) => 
    a.map(divideBy(divisor));
