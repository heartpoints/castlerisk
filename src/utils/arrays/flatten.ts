
export const flatten = <T>(items: T[][]):T[] => items.flat();
