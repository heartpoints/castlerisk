import { Predicate } from '../predicates/Predicate'


export const filter = 
    <T>(predicate: Predicate<T>) => 
    (items: T[]) => 
    items.filter(predicate)
