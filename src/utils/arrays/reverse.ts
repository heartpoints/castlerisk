
export const reverse = <T>(items: T[]) => [...items].reverse();
