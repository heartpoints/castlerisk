import { Predicate } from "../predicates/Predicate";


export const removeFirst = <T>(items: T[], predicate: Predicate<T>): T[] => items.reduce(
  (acc, cur) => acc.hasReplaced
    ? { hasReplaced: true, items: [...acc.items, cur] }
    : predicate(cur)
      ? { hasReplaced: true, items: acc.items }
      : { hasReplaced: false, items: [...acc.items, cur] },
  { hasReplaced: false, items: [] as T[] }
).items;
