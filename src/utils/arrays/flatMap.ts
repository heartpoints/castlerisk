import { Mapper } from '../axioms/Mapper'
import { map } from './map'
import { flatten } from './flatten'
import { flow, pipe } from 'fp-ts/lib/function'

export const flatMap = 
    <T, S>(mapper: Mapper<T, S[]>) => 
    flow(map(mapper), flatten)
