import { Predicate } from "../predicates/Predicate";

export const some = <T>(p: Predicate<T>) => (items: T[]) => items.some(p);
