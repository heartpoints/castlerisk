import { Predicate } from "../predicates/Predicate";

export const indexOf = <T,>(items: T[], predicate: Predicate<T>): number =>
  items.reduce(
    (foundIndex, cur, i) =>
      foundIndex === -1 ? (predicate(cur) ? i : -1) : foundIndex,
    -1
  );
