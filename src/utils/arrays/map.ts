import { Mapper } from '../axioms/Mapper';

export const map = 
    <T,S> (mapper:Mapper<T,S>) =>
    (items:T[]) => items.map(mapper)
