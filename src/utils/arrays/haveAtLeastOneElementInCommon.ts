export const haveAtLeastOneElementInCommon = (arr1:any[], arr2:any[]) => {
    return arr1.some(r=> arr2.indexOf(r) >= 0)
}