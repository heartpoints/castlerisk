import { Pair } from './Pair';
export type LikePair<T> = Pair<T, T>;
