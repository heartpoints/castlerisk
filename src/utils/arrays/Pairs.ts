import { Pair } from "./Pair";

export type Pairs<T, S> = Pair<T, S>[];
