
export const sort = <T>(items: T[]): T[] => [...items].sort();
