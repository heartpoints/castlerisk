import { Mapper } from '../axioms/Mapper'
import { entries } from '../hashing/entries'
import { hashTable } from '../hashing/hashTable'

export const groupBy = 
    <T>(hashFunction: Mapper<T, string>) =>
    (items:T[]) => 
    entries(hashTable(hashFunction)(items))

