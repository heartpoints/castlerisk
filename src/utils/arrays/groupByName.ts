import { HasName } from '../axioms/HasName';
import { getName } from '../axioms/getName';
import { groupBy } from './groupBy';


export const groupByName = <T extends HasName>(namedItems: T[]) => groupBy<T>(getName)(namedItems);
