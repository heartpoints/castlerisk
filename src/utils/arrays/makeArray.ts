import { isArray } from "./isArray";


export const makeArray = <T>(t: T | T[]) => isArray(t) ? t : [t];
