export const isLastIndex = (items: any[]) => (index: number) => index === items.length - 1;
