import { Pair } from './Pair';
import { groupBy } from './groupBy';

export const groupStrings = 
    (items: string[]): Pair<string, number>[] => 
    groupBy((s: string) => s)(items).map(([key, keys]) => [key, keys.length]);
