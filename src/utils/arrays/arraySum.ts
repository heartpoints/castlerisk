export const arraySum = (a1: number[]) => (a2: number[]) => a1.map((v, i) => v + a2[i]);
