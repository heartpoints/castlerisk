import { list } from "../list/list";
import { TypePredicate } from "../predicates/TypePredicate";

export const replaceFirst = <T, R extends T, S>(
  items: T[],
  predicate: TypePredicate<T, R>,
  replacer: (t: R) => S
): (T | S)[] => list(...items).replaceFirstT(predicate, replacer).asArray;
