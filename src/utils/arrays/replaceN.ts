import { TypePredicate } from "../predicates/TypePredicate";
import { replaceFirst } from "./replaceFirst";

export const replaceN = <T, R extends T, S extends T>(
  items: T[],
  predicate: TypePredicate<T, R>,
  replacer: (t: R) => S,
  num: number
): (T | S)[] => num === 0
    ? items
    : replaceN(
      replaceFirst(items, predicate, replacer),
      predicate,
      replacer,
      num - 1
    );
