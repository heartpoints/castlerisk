import { throwError } from "../debugging/throwError";

export const last = <T,>(items: T[]): T => items.length === 0
    ? throwError(new Error(`Attempted to get last of empty array`))
    : items[items.length - 1];
