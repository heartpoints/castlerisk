TODO
====

This should be empty / unchanged before an MR is merged! Use it only temporarily to track work for the branch you're on.

# Action Items Below

- Avatars of General / Marshall / Reinforcements / Admiral for cursor and sometimes dice rolls
- Dice
    - Show each die as a default not clickable die
    - Dice Roll Summaries should include little avatars and the total score (including history as well as snack)
- General
    - Disabled
    - In Battle
    - Ready for Battle
    - discard to deck