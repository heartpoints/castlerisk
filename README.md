castlerisk
----------

This is a mostly functional typescipt react implementation of castlerisk. Most of what happens uses the canvas so react is not used much.

# Developer Workflow

## [The Board](https://gitlab.com/heartpoints/castlerisk/-/boards/1504659)

The board is where the team organizes and prioritizes work, as well as tracks status of issues as they go from `To Do` to `doing` to `done`, contributing to completion of longer-term milestones.

### Assign yourself an `Issue` to work on

- Find an `issue` in the `To Do` column that is a match for your skills / motivation
- the most urgent items should be at the top of the list
    - nothing in `To Do`?
        - Find the highest-ranked `issue` in the `Open` colunn
        - consult the `issue author` to understand the intent / purpose
        - write down the `definition of done` agreement in the description
        - move the ticket to `To Do`
- mark yourself as `assignee` on the `issue`
- move it to the "Doing" column

_IMPORTANT_: proceed to `Open a Draft Merge Request` right away, before you've made any branches or changes!

## Merge Requests

Your changes will reside in a `git branch` that will be associated with a `gitlab merge request`. It begins as a draft and eventually, when you're ready, you `mark as ready` and invite reviewers to review your changes. The CICD pipeline (as defined in [gitlab-ci.yml](./.gitlab-ci.yml)) is also automatically run whenever you push commits to your branch, and its results show up within the `pipeline tab`. Those pipelines must succeed in order for you to be able to merge your changes to the `master` branch.

### Open a `Draft Merge Request`

Open a `Draft Merge Request` right away when beginning your work and allow the automation to do the tedious branch naming and creation for you.

- click the `issue title` to open it full screen
- use the blue dropdown button to `Create merge request and branch`
- please use the existing naming convention / suggestion for your `branch name`
- choose `Draft` mode for your `merge request`
- mark yourself as `assignee` on the `merge request`

NOTE: notice the bidirectional linkage between the `issue` and the related `merge request`

## Open your `branch` in `VS Code for Web` and begin coding!

The development enviornment is completely `dockerized`, and runs remotely, with all the dependencies and tooling needed automatically available to you from day one! We do this via `gitpod`. A browser-based `VS Code for Web` is a fully featured IDE that is typically just as performant as a native IDE, sometimes faster as much of the hard work happens remotely. The IDE is just the browser-based skin you use to interact with the codebase, compiler, webpack, etc.

1. From your `merge request` page, click the link showing your `branch name` at the top of the page.
2. Click either the _blue gitpod button_, or if you don't see that, click the `open in web ide` dropdown and choose `gitpod`, then click that.
3. Login / Sign Up to gitpod if necessary. 
    - use your gitlab account to log in / sign up
    - (do not create a separate gitpod account)
    - confirm to gitlab that its ok to allow gitpod the requested permissions
4. Confirm Pod Details + Create your Pod
    - The context url is the merge request branch of interest (this should be pre-filled)
    - VS Code (Web) should be your chosen IDE
    - Click button to continue, and a docker image will be built / found for your remote developer environment
    - The repo will be cloned and the branch will be opened
5. Your pod should initialize
    - yarn install will occur
    - vs code extensions will be initialized
6. Gitpod will automatically run `yarn start` for you
    - this runs `webpack` in dev mode
    - it automatically compiles and recompiles as files change
    - output goes to the "terminal" tab of your web-based VS Code
    - the "ports" tab should have a link to the running version of the site
    - it may prompt you if you would like to open the running site in a new tab
    - NOTE: React hot reload does not yet work, so manually refresh the castlerisk dev site to see your changes!

You may begin work! Feel free to use the terminal(s) in VS code or the plugins in the left navigation to run commands including `git` for proper version control. Happy coding!

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More about Create React App

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

# Gitpod Environment Configuration Testing

If you change the dockerfile or gitpod yaml file, run `gp validate` to confirm that the environment is buildable before pushing changes; that way you won't break anyone who attempts to create a new gitpod workspace from your current commit / branch.

# Previous Saved Game

We started a game in 2012 in mentor, here is information about it:

- Scott's turn, followed by esa matti, then tommy, then pat
- The game state (other than the hidden armies and who is which color) is pictured in ./public/images/castle-risk-game.jpg